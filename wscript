# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

from waflib.Tools import waf_unit_test

import os.path

APPNAME = 'brownsugar'
VERSION = '0.6.0'

out = 'build'

taf.PACKAGE_NAME = 'brownsugar'

taf.LOAD_TOOLS = [
    'compiler_cxx',
    'taf.tools.cpp',
]

cpp.INCLUDES = [
    os.path.join(
        '..',
        'fg',
        'inc',
    ),
    os.path.join(
        '..',
        'sucrose',
        'inc',
    ),
]

cpp.TEST_LIBPATH = [
    os.path.join(
        '..',
        'sucrose',
        'build',
        'sucrose',
    ),
]

taf.POST_FUNCTIONS = [
    waf_unit_test.summary,
]
