# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-creatingtest'

module.SOURCE = {
    'core' : {
        'state' : [
            'creatingtest.cpp',
            'creating.cpp',
            'state.cpp',
            'eventimpl.cpp',
            'reenterevent.cpp',
            'threadimpl.cpp',
            'joiner.cpp',
        ],
        'thread' : [
            'thread.cpp',
            'singleton.cpp',
            'joiner.cpp',
            'cache.cpp',
        ],
    },
}
