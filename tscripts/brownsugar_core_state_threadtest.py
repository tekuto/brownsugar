# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-threadtest'

module.SOURCE = {
    'core' : {
        'state' : [
            'threadtest.cpp',
            'thread.cpp',
            'threadimpl.cpp',
        ],
    },
}
