# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-eventtest'

module.SOURCE = {
    'core' : {
        'state' : [
            'eventtest.cpp',
            'event.cpp',
            'eventimpl.cpp',
        ],
    },
}
