# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-threadbackgroundsingletontest'

module.SOURCE = {
    'core' : {
        'state' : [
            'threadbackgroundsingletontest.cpp',
            'threadbackgroundsingleton.cpp',
            'threadimpl.cpp',
        ],
    },
}
