# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-gametest'

module.SOURCE = {
    'core' : {
        'state' : [
            'gametest.cpp',
            'game.cpp',
            'state.cpp',
            'joiner.cpp',
            'eventimpl.cpp',
            'threadimpl.cpp',
            'reenterevent.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
    },
}
