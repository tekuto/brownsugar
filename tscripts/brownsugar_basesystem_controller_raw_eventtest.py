# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_gamestatemanager',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-basesystem-controller-raw-eventtest'

module.SOURCE = {
    'basesystem' : [
        {
            'controller' : {
                'raw' : [
                    'eventtest.cpp'
                ],
            },
        },
        'basesystem.cpp',
    ],
}

module.LIB = [
    'X11',
    'sucrose-window',
    'sucrose-gl',
    'sucrose-controller',
]

module.USE = [
    'brownsugar-gamestatemanager',
]
