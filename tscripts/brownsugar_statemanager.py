# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_statemanagertest',
    'brownsugar_core_thread_threadtest',
    'brownsugar_core_thread_singletontest',
    'brownsugar_core_thread_joinertest',
    'brownsugar_core_thread_cachetest',
    'brownsugar_core_state_statetest',
    'brownsugar_core_state_creatingtest',
    'brownsugar_core_state_basesystemtest',
    'brownsugar_core_state_gametest',
    'brownsugar_core_state_threadimpltest',
    'brownsugar_core_state_threadtest',
    'brownsugar_core_state_threadsingletontest',
    'brownsugar_core_state_threadbackgroundtest',
    'brownsugar_core_state_threadbackgroundsingletontest',
    'brownsugar_core_state_eventimpltest',
    'brownsugar_core_state_eventtest',
    'brownsugar_core_state_eventbackgroundtest',
    'brownsugar_core_state_reentereventtest',
    'brownsugar_core_state_joinertest',
    'brownsugar_core_state_eventmanagertest',
    'brownsugar_core_state_eventregistermanagertest',
    'brownsugar_core_module_contexttest',
    'brownsugar_core_package_resourcetest',
    'brownsugar_core_common_filedatatest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'brownsugar-statemanager'

module.SOURCE = [
    'statemanager.cpp',
    {
        'core' : {
            'state' : [
                'state.cpp',
                'creating.cpp',
                'basesystem.cpp',
                'game.cpp',
                'eventimpl.cpp',
                'event.cpp',
                'eventbackground.cpp',
                'reenterevent.cpp',
                'threadimpl.cpp',
                'thread.cpp',
                'threadsingleton.cpp',
                'threadbackground.cpp',
                'threadbackgroundsingleton.cpp',
                'joiner.cpp',
                'eventmanager.cpp',
                'eventregistermanager.cpp',
            ],
            'thread' : [
                'cache.cpp',
                'joiner.cpp',
                'singleton.cpp',
                'thread.cpp',
            ],
            'module' : [
                'context.cpp',
            ],
            'package' : [
                'resource.cpp',
            ],
            'common' : [
                'filedata.cpp',
            ],
        },
    },
]
