# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'filedatatest'

module.SOURCE = {
    'testdata' : {
        'filedatatest' : [
            'readdatatest.txt',
        ]
    }
}
