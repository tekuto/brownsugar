# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-statemanagertest'

module.SOURCE = [
    'statemanagertest.cpp',
    'statemanager.cpp',
    {
        'core' : {
            'state' : [
                'state.cpp',
                'creating.cpp',
                'eventimpl.cpp',
                'event.cpp',
                'reenterevent.cpp',
                'threadimpl.cpp',
                'joiner.cpp',
            ],
            'thread' : [
                'thread.cpp',
                'singleton.cpp',
                'joiner.cpp',
                'cache.cpp',
            ],
        },
    },
]
