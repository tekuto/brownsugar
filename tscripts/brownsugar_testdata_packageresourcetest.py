# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

import os.path

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'packageresourcetest'

module.SOURCE = {
    'testdata' : {
        'packageresourcetest' : [
            'packageresourcetest.txt',
        ]
    }
}
