# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_basesystemstatemanager',
    'brownsugar_gamestatemanagertest',
    'brownsugar_basesystem_basesystemtest',
    'brownsugar_basesystem_window_mainwindowtest',
    'brownsugar_basesystem_window_mainwindowdraweventtest',
    'brownsugar_basesystem_controller_raw_eventtest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'brownsugar-gamestatemanager'

module.SOURCE = [
    'gamestatemanager.cpp',
    {
        'basesystem' : [
            'basesystem.cpp',
            {
                'window' : [
                    'mainwindow.cpp',
                    'mainwindowdrawevent.cpp',
                ],
                'controller' : {
                    'raw' : [
                        'event.cpp',
                    ],
                },
            },
        ],
    },
]

module.USE = [
    'brownsugar-basesystemstatemanager',
]
