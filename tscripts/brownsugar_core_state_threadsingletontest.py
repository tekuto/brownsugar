# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-threadsingletontest'

module.SOURCE = {
    'core' : {
        'state' : [
            'threadsingletontest.cpp',
            'threadsingleton.cpp',
            'threadimpl.cpp',
        ],
    },
}
