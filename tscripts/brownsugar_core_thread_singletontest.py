# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-thread-singletontest'

module.SOURCE = {
    'core' : {
        'thread' : [
            'singletontest.cpp',
            'singleton.cpp',
            'joiner.cpp',
        ],
    },
}
