# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_basesystemstatemanager',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-basesystemstatemanagertest'

module.SOURCE = [
    'basesystemstatemanagertest.cpp',
]

module.LIB = [
    'sucrose-window',
    'sucrose-gl',
    'sucrose-controller',
]

module.USE = [
    'brownsugar-basesystemstatemanager',
]
