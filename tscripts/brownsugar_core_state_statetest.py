# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-statetest'

module.SOURCE = {
    'core' : {
        'state' : [
            'statetest.cpp',
            'state.cpp',
            'creating.cpp',
            'eventimpl.cpp',
            'event.cpp',
            'eventbackground.cpp',
            'reenterevent.cpp',
            'threadimpl.cpp',
            'thread.cpp',
            'threadsingleton.cpp',
            'threadbackground.cpp',
            'threadbackgroundsingleton.cpp',
            'joiner.cpp',
            'eventmanager.cpp',
        ],
        'thread' : [
            'thread.cpp',
            'singleton.cpp',
            'joiner.cpp',
            'cache.cpp',
        ],
    },
}
