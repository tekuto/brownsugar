# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-basesystemtest'

module.SOURCE = {
    'core' : {
        'state' : [
            'basesystemtest.cpp',
            'basesystem.cpp',
            'state.cpp',
            'joiner.cpp',
            'eventimpl.cpp',
            'reenterevent.cpp',
            'threadimpl.cpp',
        ],
        'thread' : [
            'cache.cpp',
            'joiner.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
    },
}
