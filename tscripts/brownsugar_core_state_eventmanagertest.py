# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-eventmanagertest'

module.SOURCE = {
    'core' : {
        'state' : [
            'eventmanagertest.cpp',
            'eventmanager.cpp',
            'state.cpp',
            'joiner.cpp',
            'threadimpl.cpp',
            'eventimpl.cpp',
            'event.cpp',
            'eventbackground.cpp',
            'reenterevent.cpp',
            'creating.cpp',
        ],
        'thread' : [
            'joiner.cpp',
            'cache.cpp',
            'thread.cpp',
            'singleton.cpp',
        ],
    },
}
