# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_statemanager',
    'brownsugar_basesystemstatemanagertest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'brownsugar-basesystemstatemanager'

module.SOURCE = [
    'basesystemstatemanager.cpp',
]

module.USE = [
    'brownsugar-statemanager',
]
