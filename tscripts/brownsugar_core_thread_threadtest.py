# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-thread-threadtest'

module.SOURCE = {
    'core' : {
        'thread' : [
            'threadtest.cpp',
            'thread.cpp',
            'joiner.cpp',
        ],
    },
}
