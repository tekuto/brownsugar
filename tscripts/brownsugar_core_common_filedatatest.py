# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_testdata_filedatatest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-common-filedatatest'

module.SOURCE = {
    'core' : {
        'common' : [
            'filedatatest.cpp',
            'filedata.cpp',
        ],
    },
}
