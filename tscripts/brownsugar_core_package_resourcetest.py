# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_testdata_packageresourcetest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-package-resourcetest'

module.SOURCE = {
    'core' : {
        'package' : [
            'resourcetest.cpp',
            'resource.cpp',
        ],
        'module' : [
            'context.cpp',
        ],
        'common' : [
            'filedata.cpp',
        ],
    },
}
