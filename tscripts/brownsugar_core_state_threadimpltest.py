# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-threadimpltest'

module.SOURCE = {
    'core' : {
        'state' : [
            'threadimpltest.cpp',
            'threadimpl.cpp',
        ],
    },
}
