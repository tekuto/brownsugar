# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-state-eventimpltest'

module.SOURCE = {
    'core' : {
        'state' : [
            'eventimpltest.cpp',
            'eventimpl.cpp',
        ],
    },
}
