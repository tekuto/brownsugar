# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-core-thread-cachetest'

module.SOURCE = {
    'core' : {
        'thread' : [
            'cachetest.cpp',
            'cache.cpp',
            'thread.cpp',
            'singleton.cpp',
            'joiner.cpp',
        ],
    },
}
