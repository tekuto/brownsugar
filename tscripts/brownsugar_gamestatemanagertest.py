# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'brownsugar_gamestatemanager',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'brownsugar-gamestatemanagertest'

module.SOURCE = [
    'gamestatemanagertest.cpp',
]

module.LIB = [
    'X11',
    'sucrose-window',
    'sucrose-gl',
    'sucrose-controller',
]

module.USE = [
    'brownsugar-gamestatemanager',
]
