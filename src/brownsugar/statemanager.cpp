﻿#include "fg/util/export.h"
#include "brownsugar/statemanager.h"

namespace {
    void dummyDestroy(
        void *
    )
    {
    }
}

namespace brownsugar {
    StateManager::StateManager(
    )
        : threadCacheUnique( ThreadCache::create() )
        , stateUnique(
            FgState::create(
                *( this->threadCacheUnique )
                , nullptr
                , false
                , nullptr
                , dummyDestroy
            )
        )
    {
    }

    StateManager * StateManager::create_(
    )
    {
        return new StateManager;
    }

    void StateManager::destroy(
        StateManager *  _this
    )
    {
        delete _this;
    }

    fg::State<> & StateManager::getState(
    )
    {
        return reinterpret_cast< fg::State<> & >( *( this->stateUnique ) );
    }
}
