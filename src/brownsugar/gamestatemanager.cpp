﻿#include "fg/util/export.h"
#include "brownsugar/gamestatemanager.h"
#include "brownsugar/basesystemstatemanager.h"
#include "brownsugar/basesystem/basesystem.h"
#include "brownsugar/core/module/context.h"

#include <string>
#include <utility>

namespace {
    const auto  PACKAGE_PATH = std::string( "." );

    auto createBasesystemStateManager(
    )
    {
        auto    thisUnique = brownsugar::BasesystemStateManager::create();

        auto &  basesystemState = thisUnique->getBasesystemState< brownsugar::Basesystem >();
        auto &  state = basesystemState.getState();

        auto    basesystemUnique = brownsugar::Basesystem::create();
        auto &  basesystem = *basesystemUnique;

        basesystemState.setData(
            basesystemUnique.release()
            , brownsugar::Basesystem::destroy
        );

        basesystem.initialize( state );

        return std::move( thisUnique );
    }
}

namespace brownsugar {
    GameStateManager::GameStateManager(
    )
        : basesystemStateManagerUnique( createBasesystemStateManager() )
        , moduleContextUnique( FgModuleContext::create( PACKAGE_PATH ) )
        , gameStateUnique(
            FgGameState::create(
                *( this->basesystemStateManagerUnique->getTopState() )
                , **( this->moduleContextUnique )
            )
        )
    {
    }

    GameStateManager * GameStateManager::create_(
    )
    {
        return new GameStateManager;
    }

    void GameStateManager::destroy(
        GameStateManager *  _this
    )
    {
        delete _this;
    }

    FgGameState & GameStateManager::getGameState_(
    )
    {
        return *( this->gameStateUnique );
    }

    FgBasesystemState & GameStateManager::getBasesystemState_(
    )
    {
        return this->basesystemStateManagerUnique->getBasesystemState_();
    }

    void GameStateManager::start(
    )
    {
        this->basesystemStateManagerUnique->start();
    }
}
