﻿#include "fg/util/test.h"
#include "brownsugar/basesystemstatemanager.h"
#include "brownsugar/core/state/state.h"
#include "fg/core/state/enterevent.h"
#include "fg/core/state/creating.h"
#include "fg/common/unique.h"

struct BasesystemStateManagerTestData;

TEST(
    BasesystemStateManagerTest
    , GetBasesystemState
)
{
    auto    managerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, managerUnique.get() );

    auto &  basesystemState = managerUnique->getBasesystemState< BasesystemStateManagerTestData >();

    auto &  state = basesystemState.getState();

    ASSERT_NE( nullptr, state->prevStatePtr );
    ASSERT_TRUE( state->disabled );
    ASSERT_NE( nullptr, state->nextStateUnique.get() );
    ASSERT_EQ( nullptr, state->dataPtr );
}

TEST(
    BasesystemStateManagerTest
    , GetTopState
)
{
    auto    managerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, managerUnique.get() );

    auto &  state = managerUnique->getTopState();

    ASSERT_NE( nullptr, state->prevStatePtr );
    ASSERT_TRUE( state->disabled );
    ASSERT_EQ( nullptr, state->nextStateUnique.get() );
    ASSERT_EQ( nullptr, state->dataPtr );
}

struct StartTestData : public fg::UniqueWrapper< StartTestData >
{
    int &   i;

    StartTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new StartTestData( _i );
    }
};

void startTestEventProc(
    fg::StateEnterEvent< StartTestData > &  _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    BasesystemStateManagerTest
    , Start
)
{
    auto    managerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, managerUnique.get() );

    auto &  state = managerUnique->getTopState();

    auto &  creatingState = reinterpret_cast< fg::CreatingState< StartTestData > & >( state );

    auto    i = 10;

    auto    dataUnique = StartTestData::create( i );
    ASSERT_NE( nullptr, dataUnique.get() );

    fgCreatingStateSetData(
        &*creatingState
        , dataUnique.release()
        , reinterpret_cast< FgCreatingStateDataDestroyProc >( StartTestData::destroy )
    );

    creatingState.setEventProcs( startTestEventProc );

    managerUnique->start();

    ASSERT_FALSE( state->disabled );
    ASSERT_EQ( 20, i );
}
