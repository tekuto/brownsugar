﻿#include "fg/util/test.h"
#include "brownsugar/gamestatemanager.h"
#include "brownsugar/core/state/state.h"
#include "fg/core/state/enterevent.h"
#include "fg/common/unique.h"

#include <X11/Xlib.h>

struct GameStateManagerTestData;

TEST(
    GameStateManagerTest
    , GetGameState
)
{
    XInitThreads();

    auto    managerUnique = brownsugar::GameStateManager::create();
    ASSERT_NE( nullptr, managerUnique.get() );

    auto &  gameState = managerUnique->getGameState< GameStateManagerTestData >();

    auto &  state = reinterpret_cast< fg::State< GameStateManagerTestData > & >( gameState.getCreatingState() );

    ASSERT_NE( nullptr, state->prevStatePtr );
    ASSERT_TRUE( state->disabled );
    ASSERT_EQ( nullptr, state->nextStateUnique.get() );
    ASSERT_EQ( nullptr, state->dataPtr );
}

TEST(
    GameStateManagerTest
    , GetBasesystemState
)
{
    XInitThreads();

    auto    managerUnique = brownsugar::GameStateManager::create();
    ASSERT_NE( nullptr, managerUnique.get() );

    auto &  basesystemState = managerUnique->getBasesystemState();

    auto &  state = basesystemState.getState();

    ASSERT_NE( nullptr, state->prevStatePtr );
    ASSERT_TRUE( state->disabled );
    ASSERT_NE( nullptr, state->nextStateUnique.get() );
    ASSERT_NE( nullptr, state->dataPtr );
}

struct StartTestData : public fg::UniqueWrapper< StartTestData >
{
    int &   i;

    StartTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new StartTestData( _i );
    }
};

void startTestEventProc(
    fg::StateEnterEvent< StartTestData > &  _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    GameStateManagerTest
    , Start
)
{
    XInitThreads();

    auto    managerUnique = brownsugar::GameStateManager::create();
    ASSERT_NE( nullptr, managerUnique.get() );

    auto &  gameState = managerUnique->getGameState< StartTestData >();
    auto &  creatingState = gameState.getCreatingState();
    auto &  state = reinterpret_cast< fg::State< StartTestData > & >( creatingState );

    auto    i = 10;

    auto    dataUnique = StartTestData::create( i );
    ASSERT_NE( nullptr, dataUnique.get() );

    gameState.setData(
        dataUnique.release()
        , StartTestData::destroy
    );

    creatingState.setEventProcs( startTestEventProc );

    managerUnique->start();

    ASSERT_FALSE( state->disabled );
    ASSERT_EQ( 20, i );
}
