﻿#include "fg/util/test.h"
#include "fg/core/module/context.h"
#include "brownsugar/core/module/context.h"

TEST(
    ModuleContextTest
    , Create
)
{
    const auto  MODULE_CONTEXT_UNIQUE = FgModuleContext::create( "PACKAGE_PATH" );
    ASSERT_NE( nullptr, MODULE_CONTEXT_UNIQUE.get() );
    const auto &    MODULE_CONTEXT = *MODULE_CONTEXT_UNIQUE;

    ASSERT_STREQ( "PACKAGE_PATH", MODULE_CONTEXT->PACKAGE_PATH.c_str() );
}

TEST(
    ModuleContextTest
    , GetPackagePath
)
{
    const auto  MODULE_CONTEXT_UNIQUE = FgModuleContext::create( "PACKAGE_PATH" );
    ASSERT_NE( nullptr, MODULE_CONTEXT_UNIQUE.get() );
    const auto &    MODULE_CONTEXT = *MODULE_CONTEXT_UNIQUE;

    ASSERT_STREQ( "PACKAGE_PATH", MODULE_CONTEXT->getPackagePath().c_str() );
}
