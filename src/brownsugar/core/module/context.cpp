﻿#include "fg/util/export.h"
#include "fg/core/module/context.h"
#include "brownsugar/core/module/context.h"

FgModuleContext::FgModuleContext(
    const std::string & _PACKAGE_PATH
)
    : PACKAGE_PATH( _PACKAGE_PATH )
{
}

FgModuleContext * FgModuleContext::create_(
    const std::string & _PACKAGE_PATH
)
{
    return new FgModuleContext( _PACKAGE_PATH );
}

const std::string & FgModuleContext::getPackagePath(
) const
{
    return this->PACKAGE_PATH;
}
