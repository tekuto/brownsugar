﻿#include "fg/util/test.h"
#include "fg/core/package/resource.h"
#include "brownsugar/core/package/resource.h"
#include "brownsugar/core/module/context.h"

TEST(
    PackageResourceTest
    , Create
)
{
    const auto  MODULE_CONTEXT = FgModuleContext( "./packageresourcetest" );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        reinterpret_cast< const fg::ModuleContext & >( MODULE_CONTEXT )
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );
    const auto &    PACKAGE_RESOURCE = *PACKAGE_RESOURCE_UNIQUE;

    const auto &    FILE_DATA = PACKAGE_RESOURCE->fileData;
    const auto      SIZE = FILE_DATA.size();

    ASSERT_EQ( 16, SIZE );

    auto    packageResourceString = std::string(
        FILE_DATA.data()
        , SIZE
    );

    ASSERT_STREQ( "PACKAGERESOURCE\n", packageResourceString.c_str() );
}

TEST(
    PackageResourceTest
    , Create_failed
)
{
    const auto  MODULE_CONTEXT = FgModuleContext( "./packageresourcetest" );

    try {
        fg::PackageResource::create(
            reinterpret_cast< const fg::ModuleContext & >( MODULE_CONTEXT )
            , "notfound"
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    PackageResourceTest
    , GetSize
)
{
    const auto  MODULE_CONTEXT = FgModuleContext( "./packageresourcetest" );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        reinterpret_cast< const fg::ModuleContext & >( MODULE_CONTEXT )
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );

    ASSERT_EQ( 16, PACKAGE_RESOURCE_UNIQUE->getSize() );
}

TEST(
    PackageResourceTest
    , GetData
)
{
    const auto  MODULE_CONTEXT = FgModuleContext( "./packageresourcetest" );

    const auto  PACKAGE_RESOURCE_UNIQUE = fg::PackageResource::create(
        reinterpret_cast< const fg::ModuleContext & >( MODULE_CONTEXT )
        , "packageresourcetest.txt"
    );
    ASSERT_NE( nullptr, PACKAGE_RESOURCE_UNIQUE.get() );

    const auto  DATA_PTR = PACKAGE_RESOURCE_UNIQUE->getData();
    ASSERT_NE( nullptr, DATA_PTR );

    auto    packageResourceString = std::string(
        static_cast< const char * >( DATA_PTR )
        , PACKAGE_RESOURCE_UNIQUE->getSize()
    );

    ASSERT_STREQ( "PACKAGERESOURCE\n", packageResourceString.c_str() );
}
