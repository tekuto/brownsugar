﻿#include "fg/util/export.h"
#include "fg/core/package/resource.h"
#include "brownsugar/core/package/resource.h"
#include "brownsugar/core/module/context.h"
#include "brownsugar/core/common/filedata.h"

#include <string>
#include <utility>

namespace {
    std::string getPackageResourcePath(
        const FgModuleContext & _MODULE_CONTEXT
        , const char *          _FILE_NAME
    )
    {
        auto    path = _MODULE_CONTEXT.getPackagePath();
        path.push_back( '/' );
        path.append( _FILE_NAME );

        return std::move( path );
    }

    brownsugar::FileData readFile(
        const FgModuleContext & _MODULE_CONTEXT
        , const char *          _FILE_NAME
    )
    {
        const auto  PACKAGE_RESOURCE_PATH = getPackageResourcePath(
            _MODULE_CONTEXT
            , _FILE_NAME
        );

        return brownsugar::readFile( PACKAGE_RESOURCE_PATH );
    }
}

FgPackageResource * fgPackageResourceCreate(
    const FgModuleContext * _MODULE_CONTEXT_PTR
    , const char *          _FILE_NAME
)
{
    auto    fileData = ::readFile(
        *_MODULE_CONTEXT_PTR
        , _FILE_NAME
    );

    return new FgPackageResource{
        std::move( fileData ),
    };
}

void fgPackageResourceDestroy(
    FgPackageResource * _implPtr
)
{
    delete _implPtr;
}

size_t fgPackageResourceGetSize(
    const FgPackageResource *   _IMPL_PTR
)
{
    return _IMPL_PTR->fileData.size();
}

const void * fgPackageResourceGetData(
    const FgPackageResource *   _IMPL_PTR
)
{
    return _IMPL_PTR->fileData.data();
}
