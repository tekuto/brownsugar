﻿#include "fg/util/export.h"
#include "brownsugar/core/thread/joiner.h"

#include <mutex>

namespace brownsugar {
    void ThreadJoiner::End::operator()(
        ThreadJoiner *  _joinerPtr
    ) const
    {
        auto &  runningThreadCount = _joinerPtr->runningThreadCount;

        auto    lock = std::unique_lock< std::mutex >( _joinerPtr->mutex );

        runningThreadCount--;

        if( runningThreadCount <= 0 ) {
            _joinerPtr->cond.notify_all();
        }
    }

    void ThreadJoiner::Join::operator()(
        ThreadJoiner *  _implPtr
    ) const
    {
        _implPtr->join();
    }

    ThreadJoiner::ThreadJoiner(
    )
        : runningThreadCount( 0 )
        , joiner( this )
    {
    }

    ThreadJoiner::Unique ThreadJoiner::create(
    )
    {
        return new ThreadJoiner;
    }

    void ThreadJoiner::destroy(
        ThreadJoiner *  _this
    )
    {
        delete _this;
    }

    ThreadJoiner::Ender ThreadJoiner::startThread(
    )
    {
        auto    lock = std::unique_lock< std::mutex >( this->mutex );

        this->runningThreadCount++;

        return ThreadJoiner::Ender( this );
    }

    void ThreadJoiner::join(
    )
    {
        auto    lock = std::unique_lock< std::mutex >( this->mutex );

        while( this->runningThreadCount > 0 ) {
            this->cond.wait( lock );
        }
    }
}
