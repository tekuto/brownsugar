﻿#include "brownsugar/core/thread/cache.h"
#include "brownsugar/core/thread/thread.h"
#include "brownsugar/core/thread/singleton.h"
#include "brownsugar/core/thread/joiner.h"

#include <mutex>

namespace {
    template<
        typename THREAD_UNIQUES_T
        , typename THREAD_PROC_T
    >
    bool setReexecuteThread(
        THREAD_UNIQUES_T &                          _threadUniques
        , THREAD_PROC_T                             _procPtr
        , void *                                    _userDataPtr
        , brownsugar::ThreadJoiner &                _joiner
        , const std::unique_lock< std::mutex > &
    )
    {
        for( auto & threadUnique : _threadUniques ) {
            if( threadUnique->setReexecute(
                _procPtr
                , _userDataPtr
                , _joiner
            ) == false ) {
                continue;
            }

            return true;
        }

        return false;
    }

    template<
        typename THREAD_UNIQUES_T
        , typename THREAD_PROC_T
    >
    bool resetThread(
        THREAD_UNIQUES_T &                          _threadUniques
        , THREAD_PROC_T                             _procPtr
        , void *                                    _userDataPtr
        , brownsugar::ThreadJoiner &                _joiner
        , const std::unique_lock< std::mutex > &
    )
    {
        for( auto & threadUnique : _threadUniques ) {
            if( threadUnique->reset(
                _procPtr
                , _userDataPtr
                , _joiner
            ) == false ) {
                continue;
            }

            return true;
        }

        return false;
    }

    template<
        typename THREAD_UNIQUES_T
        , typename THREAD_PROC_T
    >
    void addThread(
        THREAD_UNIQUES_T &                          _threadUniques
        , THREAD_PROC_T                             _procPtr
        , void *                                    _userDataPtr
        , brownsugar::ThreadJoiner &                _joiner
        , const std::unique_lock< std::mutex > &
    )
    {
        _threadUniques.push_back(
            THREAD_UNIQUES_T::value_type::Impl::create(
                _procPtr
                , _userDataPtr
                , _joiner
            )
        );
    }
}

namespace brownsugar {
    ThreadCache::ThreadCache(
    )
    {
    }

    ThreadCache::Unique ThreadCache::create(
    )
    {
        return new ThreadCache;
    }

    void ThreadCache::destroy(
        ThreadCache *   _this
    )
    {
        delete _this;
    }

    void ThreadCache::execute(
        ThreadProcImpl      _procPtr
        , void *            _userDataPtr
        , ThreadJoiner &    _joiner
    )
    {
        auto &  threadUniques = this->threadUniques;

        auto    lock = std::unique_lock< std::mutex >( this->threadUniquesMutex );

        if( resetThread(
            threadUniques
            , _procPtr
            , _userDataPtr
            , _joiner
            , lock
        ) == true ) {
            return;
        }

        addThread(
            threadUniques
            , _procPtr
            , _userDataPtr
            , _joiner
            , lock
        );
    }

    void ThreadCache::execute(
        ThreadSingletonProcImpl _procPtr
        , void *                _userDataPtr
        , ThreadJoiner &        _joiner
    )
    {
        auto &  threadUniques = this->singletonThreadUniques;

        auto    lock = std::unique_lock< std::mutex >( this->singletonThreadUniquesMutex );

        if( setReexecuteThread(
            threadUniques
            , _procPtr
            , _userDataPtr
            , _joiner
            , lock
        ) == true ) {
            return;
        }

        if( resetThread(
            threadUniques
            , _procPtr
            , _userDataPtr
            , _joiner
            , lock
        ) == true ) {
            return;
        }

        addThread(
            threadUniques
            , _procPtr
            , _userDataPtr
            , _joiner
            , lock
        );
    }
}
