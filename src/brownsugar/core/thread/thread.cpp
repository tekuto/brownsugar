﻿#include "brownsugar/core/thread/thread.h"
#include "brownsugar/core/thread/joiner.h"

#include <utility>
#include <mutex>

namespace brownsugar {
    void ThreadImpl::JoinThread::operator()(
        std::thread *   _threadPtr
    ) const
    {
        _threadPtr->join();
    }

    void ThreadImpl::EndThread::operator()(
        ThreadImpl *    _threadPtr
    ) const
    {
        auto    lock = std::unique_lock< std::mutex >( _threadPtr->mutex );

        _threadPtr->ended = true;

        _threadPtr->cond.notify_one();
    }

    bool ThreadImpl::waitForEnabled(
        std::unique_lock< std::mutex > &    _lock
    )
    {
        while( this->ended == false ) {
            if( this->enabled == true ) {
                return true;
            }

            this->cond.wait( _lock );
        }

        return false;
    }

    void ThreadImpl::threadProc(
    )
    {
        while( true ) {
            auto &  procPtr = this->procPtr;

            procPtr( *this );

            auto    lock = std::unique_lock< std::mutex >( this->mutex );

            this->enabled = false;
            procPtr = nullptr;
            this->userDataPtr = nullptr;
            this->procEnder.reset();

            if( this->waitForEnabled( lock ) == false ) {
                break;
            }
        }
    }

    ThreadImpl::ThreadImpl(
        ThreadProcImpl      _procPtr
        , void *            _userDataPtr
        , ThreadJoiner &    _joiner
    )
        : ended( false )
        , enabled( true )
        , procPtr( _procPtr )
        , userDataPtr( _userDataPtr )
        , procEnder( _joiner.startThread() )
        , thread(
            [
                this
            ]
            {
                this->threadProc();
            }
        )
        , joiner( &( this->thread ) )
        , ender( this )
    {
    }

    bool ThreadImpl::reset(
        ThreadProcImpl      _procPtr
        , void *            _userDataPtr
        , ThreadJoiner &    _joiner
    )
    {
        auto    lock = std::unique_lock< std::mutex >( this->mutex );

        if( this->enabled == true ) {
            return false;
        }

        this->enabled = true;
        this->procPtr = _procPtr;
        this->userDataPtr = _userDataPtr;
        this->procEnder = _joiner.startThread();

        this->cond.notify_one();

        return true;
    }

    void * ThreadImpl::getData(
    )
    {
        return this->userDataPtr;
    }
}
