﻿#include "fg/util/test.h"
#include "brownsugar/core/thread/cache.h"
#include "brownsugar/core/thread/thread.h"
#include "brownsugar/core/thread/singleton.h"
#include "brownsugar/core/thread/joiner.h"
#include "fg/common/unique.h"

#include <memory>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <vector>

struct ThreadCacheTest
{
    struct Thread : public fg::UniqueWrapper< Thread >
    {
        std::mutex              mutex;
        std::condition_variable cond;

        bool    ended;
        bool    enabled;

        brownsugar::ThreadProcImpl      procPtr;
        void *                          userDataPtr;
        brownsugar::ThreadJoiner::Ender procEnder;

        std::thread thread;

        std::unique_ptr< std::thread >  joiner;
        std::unique_ptr< Thread >       ender;
    };

    using ThreadUniques = std::vector< Thread::Unique >;

    struct ThreadSingleton : public fg::UniqueWrapper< ThreadSingleton >
    {
        std::mutex              mutex;
        std::condition_variable cond;

        bool    ended;
        bool    enabled;
        bool    reexecute;

        brownsugar::ThreadSingletonProcImpl procPtr;
        void *                              userDataPtr;
        brownsugar::ThreadJoiner::Ender     procEnder;

        std::thread thread;

        std::unique_ptr< std::thread >      joiner;
        std::unique_ptr< ThreadSingleton >  ender;
    };

    using ThreadSingletonUniques = std::vector< ThreadSingleton::Unique >;

    std::mutex  threadUniquesMutex;

    ThreadUniques   threadUniques;

    std::mutex  singletonThreadUniquesMutex;

    ThreadSingletonUniques  singletonThreadUniques;
};

TEST(
    ThreadCacheTest
    , Create
)
{
    auto    cacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, cacheUnique.get() );
    const auto &    CACHE = reinterpret_cast< const ThreadCacheTest & >( *cacheUnique );

    ASSERT_EQ( 0, CACHE.threadUniques.size() );
    ASSERT_EQ( 0, CACHE.singletonThreadUniques.size() );
}

struct ExecuteTestData
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;
    int     i = 10;
};

void executeTestProc(
    brownsugar::Thread< ExecuteTestData > & _thread
)
{
    auto &  data = _thread.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i = 20;
}

TEST(
    ThreadCacheTest
    , Execute
)
{
    auto    cacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, cacheUnique.get() );
    const auto &    CACHE = reinterpret_cast< const ThreadCacheTest & >( *cacheUnique );

    auto    data = ExecuteTestData();

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    cacheUnique->execute(
        executeTestProc
        , data
        , joiner
    );

    const auto &    THREAD_UNIQUES = CACHE.threadUniques;
    ASSERT_EQ( 1, THREAD_UNIQUES.size() );

    const auto &    THREAD = *( THREAD_UNIQUES[ 0 ] );
    ASSERT_FALSE( THREAD.ended );
    ASSERT_TRUE( THREAD.enabled );
    ASSERT_EQ( reinterpret_cast< brownsugar::ThreadProcImpl >( executeTestProc ), THREAD.procPtr );
    ASSERT_EQ( &data, THREAD.userDataPtr );
    ASSERT_NE( nullptr, THREAD.procEnder.get() );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_EQ( &( THREAD.thread ), THREAD.joiner.get() );
    ASSERT_EQ( &THREAD, THREAD.ender.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_FALSE( THREAD.enabled );
    ASSERT_EQ( nullptr, THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_EQ( nullptr, THREAD.procEnder.get() );

    ASSERT_EQ( 20, data.i );
}

void executeTestProc2(
    brownsugar::Thread< ExecuteTestData > & _thread
)
{
    auto &  data = _thread.getData();

    data.i = 20;
}

void executeTestProc3(
    brownsugar::Thread< ExecuteTestData > & _thread
)
{
    auto &  data = _thread.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i = 30;
}

TEST(
    ThreadCacheTest
    , Execute_existsIdleThread
)
{
    auto    cacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, cacheUnique.get() );
    auto &  cache = reinterpret_cast< ThreadCacheTest & >( *cacheUnique );

    auto    data = ExecuteTestData();

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    cacheUnique->execute(
        executeTestProc2
        , data
        , joiner
    );

    joiner.join();

    cacheUnique->execute(
        executeTestProc3
        , data
        , joiner
    );

    const auto &    THREAD_UNIQUES = cache.threadUniques;
    ASSERT_EQ( 1, THREAD_UNIQUES.size() );

    const auto &    THREAD = *( THREAD_UNIQUES[ 0 ] );
    ASSERT_FALSE( THREAD.ended );
    ASSERT_TRUE( THREAD.enabled );
    ASSERT_EQ( reinterpret_cast< brownsugar::ThreadProcImpl >( executeTestProc3 ), THREAD.procPtr );
    ASSERT_EQ( &data, THREAD.userDataPtr );
    ASSERT_NE( nullptr, THREAD.procEnder.get() );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_EQ( &( THREAD.thread ), THREAD.joiner.get() );
    ASSERT_EQ( &THREAD, THREAD.ender.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_FALSE( THREAD.enabled );
    ASSERT_EQ( nullptr, THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_EQ( nullptr, THREAD.procEnder.get() );

    ASSERT_EQ( 30, data.i );
}

void executeSingletonTestProc(
    brownsugar::ThreadSingleton< ExecuteTestData > &    _thread
)
{
    auto &  data = _thread.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i = 20;
}

TEST(
    ThreadCacheTest
    , ExecuteSingleton
)
{
    auto    cacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, cacheUnique.get() );
    const auto &    CACHE = reinterpret_cast< const ThreadCacheTest & >( *cacheUnique );

    auto    data = ExecuteTestData();

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    cacheUnique->execute(
        executeSingletonTestProc
        , data
        , joiner
    );

    const auto &    THREAD_UNIQUES = CACHE.singletonThreadUniques;
    ASSERT_EQ( 1, THREAD_UNIQUES.size() );

    const auto &    THREAD = *( THREAD_UNIQUES[ 0 ] );
    ASSERT_FALSE( THREAD.ended );
    ASSERT_TRUE( THREAD.enabled );
    ASSERT_FALSE( THREAD.reexecute );
    ASSERT_EQ( reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( executeSingletonTestProc ), THREAD.procPtr );
    ASSERT_EQ( &data, THREAD.userDataPtr );
    ASSERT_NE( nullptr, THREAD.procEnder.get() );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_EQ( &( THREAD.thread ), THREAD.joiner.get() );
    ASSERT_EQ( &THREAD, THREAD.ender.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_FALSE( THREAD.enabled );
    ASSERT_EQ( nullptr, THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_EQ( nullptr, THREAD.procEnder.get() );

    ASSERT_EQ( 20, data.i );
}

void executeSingletonTestProc2(
    brownsugar::ThreadSingleton< ExecuteTestData > &    _thread
)
{
    auto &  data = _thread.getData();

    data.i = 20;
}

void executeSingletonTestProc3(
    brownsugar::ThreadSingleton< ExecuteTestData > &    _thread
)
{
    auto &  data = _thread.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i = 30;
}

TEST(
    ThreadCacheTest
    , ExecuteSingleton_existsIdleThread
)
{
    auto    cacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, cacheUnique.get() );
    auto &  cache = reinterpret_cast< ThreadCacheTest & >( *cacheUnique );

    auto    data = ExecuteTestData();

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    cacheUnique->execute(
        executeSingletonTestProc2
        , data
        , joiner
    );

    joiner.join();

    cacheUnique->execute(
        executeSingletonTestProc3
        , data
        , joiner
    );

    const auto &    THREAD_UNIQUES = cache.singletonThreadUniques;
    ASSERT_EQ( 1, THREAD_UNIQUES.size() );

    const auto &    THREAD = *( THREAD_UNIQUES[ 0 ] );
    ASSERT_FALSE( THREAD.ended );
    ASSERT_TRUE( THREAD.enabled );
    ASSERT_FALSE( THREAD.reexecute );
    ASSERT_EQ( reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( executeSingletonTestProc3 ), THREAD.procPtr );
    ASSERT_EQ( &data, THREAD.userDataPtr );
    ASSERT_NE( nullptr, THREAD.procEnder.get() );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_EQ( &( THREAD.thread ), THREAD.joiner.get() );
    ASSERT_EQ( &THREAD, THREAD.ender.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_FALSE( THREAD.enabled );
    ASSERT_EQ( nullptr, THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_EQ( nullptr, THREAD.procEnder.get() );

    ASSERT_EQ( 30, data.i );
}

void executeSingletonTestProc4(
    brownsugar::ThreadSingleton< ExecuteTestData > &    _thread
)
{
    auto &  data = _thread.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += 10;
}

TEST(
    ThreadCacheTest
    , ExecuteSingleton_existsSameThread
)
{
    auto    cacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, cacheUnique.get() );
    auto &  cache = reinterpret_cast< ThreadCacheTest & >( *cacheUnique );

    auto    data = ExecuteTestData();

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    cacheUnique->execute(
        executeSingletonTestProc4
        , data
        , joiner
    );

    cacheUnique->execute(
        executeSingletonTestProc4
        , data
        , joiner
    );

    cacheUnique->execute(
        executeSingletonTestProc4
        , data
        , joiner
    );

    const auto &    THREAD_UNIQUES = cache.singletonThreadUniques;
    ASSERT_EQ( 1, THREAD_UNIQUES.size() );

    const auto &    THREAD = *( THREAD_UNIQUES[ 0 ] );
    ASSERT_FALSE( THREAD.ended );
    ASSERT_TRUE( THREAD.enabled );
    ASSERT_TRUE( THREAD.reexecute );
    ASSERT_EQ( reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( executeSingletonTestProc4 ), THREAD.procPtr );
    ASSERT_EQ( &data, THREAD.userDataPtr );
    ASSERT_NE( nullptr, THREAD.procEnder.get() );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_EQ( &( THREAD.thread ), THREAD.joiner.get() );
    ASSERT_EQ( &THREAD, THREAD.ender.get() );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_FALSE( THREAD.enabled );
    ASSERT_FALSE( THREAD.reexecute );
    ASSERT_EQ( nullptr, THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_EQ( nullptr, THREAD.procEnder.get() );

    ASSERT_EQ( 30, data.i );
}
