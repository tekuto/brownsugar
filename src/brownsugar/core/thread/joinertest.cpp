﻿#include "fg/util/test.h"
#include "brownsugar/core/thread/joiner.h"

#include <memory>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <cstddef>

struct ThreadJoinerTest
{
    using Joiner = std::unique_ptr< ThreadJoinerTest >;

    std::mutex              mutex;
    std::condition_variable cond;

    std::size_t runningThreadCount;

    Joiner  joiner;
};

struct JoinThread
{
    void operator()(
        std::thread *   _threadPtr
    )
    {
        _threadPtr->join();
    }
};

using ThreadJoiner = std::unique_ptr<
    std::thread
    , JoinThread
>;

TEST(
    ThreadJoinerTest
    , Create
)
{
    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    const auto &    JOINER = reinterpret_cast< const ThreadJoinerTest & >( *joinerUnique );

    ASSERT_EQ( 0, JOINER.runningThreadCount );
    ASSERT_EQ( &JOINER, JOINER.joiner.get() );
}

TEST(
    ThreadJoinerTest
    , Delete
)
{
    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );

    auto    ender = joinerUnique->startThread();

    auto    i = 10;

    std::mutex              mutex;
    std::condition_variable cond;

    auto    thread = std::thread(
        [
            ender = std::move( ender )
            , &mutex
            , &cond
            , &i
        ]
        {
            auto    lock = std::unique_lock< std::mutex >( mutex );

            cond.wait_for(
                lock
                , std::chrono::milliseconds( 10 )
            );

            i = 20;
        }
    );
    auto    threadJoiner = ThreadJoiner( &thread );

    joinerUnique.destroy();

    ASSERT_EQ( 20, i );

    auto    lock = std::unique_lock< std::mutex >( mutex );

    cond.notify_one();
}

TEST(
    ThreadJoinerTest
    , StartThread
)
{
    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = reinterpret_cast< ThreadJoinerTest & >( *joinerUnique );

    auto    ender = joinerUnique->startThread();

    ASSERT_EQ( joinerUnique.get(), ender.get() );
    ASSERT_EQ( 1, joiner.runningThreadCount );

    auto    ender2 = joinerUnique->startThread();

    ASSERT_EQ( joinerUnique.get(), ender2.get() );
    ASSERT_EQ( 2, joiner.runningThreadCount );

    ender.reset();
    ender2.reset();

    joiner.runningThreadCount = 0;
}

TEST(
    ThreadJoinerTest
    , Join
)
{
    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );

    auto    ender = joinerUnique->startThread();

    std::mutex              mutex;
    std::condition_variable cond;

    auto    i = 10;

    auto    thread = std::thread(
        [
            ender = std::move( ender )
            , &mutex
            , &cond
            , &i
        ]
        {
            auto    lock = std::unique_lock< std::mutex >( mutex );

            cond.wait_for(
                lock
                , std::chrono::milliseconds( 10 )
            );

            i = 20;
        }
    );
    auto    threadJoiner = ThreadJoiner( &thread );

    joinerUnique->join();

    ASSERT_EQ( 20, i );

    auto    lock = std::unique_lock< std::mutex >( mutex );

    cond.notify_one();
}

TEST(
    ThreadJoinerTest
    , Ender
)
{
    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    const auto &    JOINER = reinterpret_cast< const ThreadJoinerTest & >( *joinerUnique );

    auto    ender = joinerUnique->startThread();
    auto    ender2 = joinerUnique->startThread();

    ender.reset();

    ASSERT_EQ( 1, JOINER.runningThreadCount );

    ender2.reset();

    ASSERT_EQ( 0, JOINER.runningThreadCount );
}
