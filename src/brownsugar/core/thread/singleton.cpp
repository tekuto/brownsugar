﻿#include "brownsugar/core/thread/singleton.h"
#include "brownsugar/core/thread/joiner.h"

#include <utility>
#include <mutex>

namespace brownsugar {
    void ThreadSingletonImpl::JoinThread::operator()(
        std::thread *   _threadPtr
    ) const
    {
        _threadPtr->join();
    }

    void ThreadSingletonImpl::EndThread::operator()(
        ThreadSingletonImpl *   _threadPtr
    ) const
    {
        auto    lock = std::unique_lock< std::mutex >( _threadPtr->mutex );

        _threadPtr->ended = true;

        _threadPtr->cond.notify_one();
    }

    bool ThreadSingletonImpl::waitForEnabled(
        std::unique_lock< std::mutex > &    _lock
    )
    {
        while( this->ended == false ) {
            if( this->enabled == true ) {
                return true;
            }

            this->cond.wait( _lock );
        }

        return false;
    }

    void ThreadSingletonImpl::threadProc(
    )
    {
        auto &  procPtr = this->procPtr;
        auto &  reexecute = this->reexecute;

        while( true ) {
            procPtr( *this );

            auto    lock = std::unique_lock< std::mutex >( this->mutex );

            if( reexecute == true ) {
                reexecute = false;

                continue;
            }

            this->enabled = false;
            procPtr = nullptr;
            this->userDataPtr = nullptr;
            this->procEnder.reset();

            if( this->waitForEnabled( lock ) == false ) {
                break;
            }
        }
    }

    ThreadSingletonImpl::ThreadSingletonImpl(
        ThreadSingletonProcImpl _procPtr
        , void *                _userDataPtr
        , ThreadJoiner &        _joiner
    )
        : ended( false )
        , enabled( true )
        , reexecute( false )
        , procPtr( _procPtr )
        , userDataPtr( _userDataPtr )
        , procEnder( _joiner.startThread() )
        , thread(
            [
                this
            ]
            {
                this->threadProc();
            }
        )
        , joiner( &( this->thread ) )
        , ender( this )
    {
    }

    bool ThreadSingletonImpl::setReexecute(
        ThreadSingletonProcImpl _procPtr
        , void *                _userDataPtr
        , ThreadJoiner &        _joiner
    )
    {
        auto    lock = std::unique_lock< std::mutex >( this->mutex );

        if( this->enabled == false ) {
            return false;
        }

        if( this->procPtr != _procPtr ) {
            return false;
        }

        if( this->userDataPtr != _userDataPtr ) {
            return false;
        }

        if( this->procEnder.get() != &_joiner ) {
            return false;
        }

        this->reexecute = true;

        return true;
    }

    bool ThreadSingletonImpl::reset(
        ThreadSingletonProcImpl _procPtr
        , void *                _userDataPtr
        , ThreadJoiner &        _joiner
    )
    {
        auto    lock = std::unique_lock< std::mutex >( this->mutex );

        if( this->enabled == true ) {
            return false;
        }

        this->enabled = true;
        this->procPtr = _procPtr;
        this->userDataPtr = _userDataPtr;
        this->procEnder = _joiner.startThread();

        this->cond.notify_one();

        return true;
    }

    void * ThreadSingletonImpl::getData(
    )
    {
        return this->userDataPtr;
    }
}
