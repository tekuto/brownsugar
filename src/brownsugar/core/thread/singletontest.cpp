﻿#include "fg/util/test.h"
#include "brownsugar/core/thread/singleton.h"
#include "brownsugar/core/thread/joiner.h"

#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <memory>

struct ThreadSingletonImplTest
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended;
    bool    enabled;
    bool    reexecute;

    brownsugar::ThreadSingletonProcImpl procPtr;
    void *                              userDataPtr;
    brownsugar::ThreadJoiner::Ender     procEnder;

    std::thread thread;

    std::unique_ptr< std::thread >              joiner;
    std::unique_ptr< ThreadSingletonImplTest >  ender;
};

void createTestProc(
    brownsugar::ThreadSingletonImpl &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
}

TEST(
    ThreadSingletonImplTest
    , Create
)
{
    auto    i = 10;

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    threadUnique = brownsugar::ThreadSingletonImpl::create(
        createTestProc
        , &i
        , joiner
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    const auto &    THREAD = reinterpret_cast< const ThreadSingletonImplTest & >( *threadUnique );

    ASSERT_FALSE( THREAD.ended );
    ASSERT_TRUE( THREAD.enabled );
    ASSERT_FALSE( THREAD.reexecute );
    ASSERT_EQ( createTestProc, THREAD.procPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_EQ( &joiner, THREAD.procEnder.get() );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_EQ( &( THREAD.thread ), THREAD.joiner.get() );
    ASSERT_EQ( &THREAD, THREAD.ender.get() );

    joiner.join();

    ASSERT_FALSE( THREAD.enabled );
    ASSERT_EQ( nullptr, THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_EQ( nullptr, THREAD.procEnder.get() );
}

struct SetReexecuteTestData
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;
    int     i = 10;
};

void setReexecuteTestProc(
    brownsugar::ThreadSingleton< SetReexecuteTestData > &   _thread
)
{
    auto &  data = _thread.getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += 10;
}

void setReexecuteTestProc2(
    brownsugar::ThreadSingletonImpl &
)
{
}

TEST(
    ThreadSingletonImplTest
    , SetReexecute
)
{
    auto    data = SetReexecuteTestData();

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    joiner2Unique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joiner2Unique.get() );
    auto &  joiner2 = *joiner2Unique;

    auto    threadUnique = brownsugar::ThreadSingletonImpl::create(
        reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( setReexecuteTestProc )
        , &data
        , joiner
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    auto &  thread = *threadUnique;
    const auto &    THREAD = reinterpret_cast< const ThreadSingletonImplTest & >( thread );

    ASSERT_TRUE(
        thread.setReexecute(
            reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( setReexecuteTestProc )
            , &data
            , joiner
        )
    );

    ASSERT_TRUE( THREAD.reexecute );

    ASSERT_TRUE(
        thread.setReexecute(
            reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( setReexecuteTestProc )
            , &data
            , joiner
        )
    );

    ASSERT_FALSE(
        thread.setReexecute(
            reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( setReexecuteTestProc2 )
            , &data
            , joiner
        )
    );

    ASSERT_FALSE(
        thread.setReexecute(
            reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( setReexecuteTestProc )
            , nullptr
            , joiner
        )
    );

    ASSERT_FALSE(
        thread.setReexecute(
            reinterpret_cast< brownsugar::ThreadSingletonProcImpl >( setReexecuteTestProc )
            , &data
            , joiner2
        )
    );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_FALSE( THREAD.reexecute );

    ASSERT_EQ( 30, data.i );
}

void resetTestProc(
    brownsugar::ThreadSingletonImpl &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
}

TEST(
    ThreadSingletonImplTest
    , Reset
)
{
    auto    i = 10;

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    auto    joiner2Unique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joiner2Unique.get() );
    auto &  joiner2 = *joiner2Unique;

    auto    threadUnique = brownsugar::ThreadSingletonImpl::create(
        createTestProc
        , &i
        , joiner
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    auto &  thread = *threadUnique;
    const auto &    THREAD = reinterpret_cast< const ThreadSingletonImplTest & >( thread );

    ASSERT_FALSE(
        thread.reset(
            resetTestProc
            , &j
            , joiner2
        )
    );

    ASSERT_TRUE( THREAD.enabled );
    ASSERT_EQ( createTestProc, THREAD.procPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_EQ( &joiner, THREAD.procEnder.get() );

    joiner.join();

    ASSERT_TRUE(
        thread.reset(
            resetTestProc
            , &j
            , joiner2
        )
    );

    ASSERT_TRUE( THREAD.enabled );
    ASSERT_EQ( resetTestProc, THREAD.procPtr );
    ASSERT_EQ( 20, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_EQ( &joiner2, THREAD.procEnder.get() );
}

TEST(
    ThreadSingletonImplTest
    , GetData
)
{
    auto    i = 10;

    auto    joinerUnique = brownsugar::ThreadJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    threadUnique = brownsugar::ThreadSingletonImpl::create(
        createTestProc
        , &i
        , joiner
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    auto &  thread = *threadUnique;

    ASSERT_NE( nullptr, thread.getData() );
    ASSERT_EQ( 10, reinterpret_cast< brownsugar::ThreadSingleton< int > & >( thread ).getData() );
}
