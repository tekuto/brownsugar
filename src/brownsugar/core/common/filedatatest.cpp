﻿#include "fg/util/test.h"
#include "brownsugar/core/common/filedata.h"

#include <string>

TEST(
    FileDataTest
    , ReadFile
)
{
    auto    fileData = brownsugar::readFile( "filedatatest/readdatatest.txt" );

    auto    fileDataString = std::string(
        fileData.begin()
        , fileData.end()
    );

    ASSERT_STREQ( "READDATA\n", fileDataString.c_str() );
}

TEST(
    FileDataTest
    , ReadFile_failedWhenFileNotExists
)
{
    try {
        auto    fileData = brownsugar::readFile( "filedatatest/notfound.txt" );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    FileDataTest
    , WriteFile
)
{
    const auto  WRITE_DATA = std::string( "WRITEDATA" );

    const auto  FILE_DATA = brownsugar::FileData(
        WRITE_DATA.begin()
        , WRITE_DATA.end()
    );

    brownsugar::writeFile(
        "filedatatest/writefiletest"
        , FILE_DATA
    );

    auto    fileData = brownsugar::readFile( "filedatatest/writefiletest" );

    auto    fileDataString = std::string(
        fileData.begin()
        , fileData.end()
    );

    ASSERT_STREQ( "WRITEDATA", fileDataString.c_str() );
}

TEST(
    FileDataTest
    , WriteFile_failedWhenDirectoryNotExists
)
{
    const auto  WRITE_DATA = std::string( "WRITEDATA" );

    const auto  FILE_DATA = brownsugar::FileData(
        WRITE_DATA.begin()
        , WRITE_DATA.end()
    );

    try {
        brownsugar::writeFile(
            "filedatatest/notfound/notfound"
            , FILE_DATA
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
