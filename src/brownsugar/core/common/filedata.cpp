﻿#include "brownsugar/core/common/filedata.h"

#include <array>
#include <memory>
#include <cstdio>
#include <utility>
#include <stdexcept>

namespace {
    enum {
        BUFFER_SIZE = 1024,
    };

    using Buffer = std::array<
        char
        , BUFFER_SIZE
    >;

    struct CloseFile
    {
        void operator()(
            std::FILE * _filePtr
        ) const
        {
            std::fclose( _filePtr );
        }
    };

    using FileUnique = std::unique_ptr<
        std::FILE
        , CloseFile
    >;

    brownsugar::FileData readFileData(
        std::FILE & _file
    )
    {
        auto    fileData = brownsugar::FileData();
        while( std::feof( &_file ) == 0 ) {
            auto    buffer = Buffer();

            const auto  READ_SIZE = std::fread(
                buffer.data()
                , 1
                , std::tuple_size< decltype( buffer ) >::value
                , &_file
            );
            if( std::ferror( &_file ) != 0 ) {
                throw std::runtime_error( "fread()が失敗" );
            }

            auto    it = buffer.begin();

            fileData.insert(
                fileData.end()
                , it
                , it + READ_SIZE
            );
        }

        return std::move( fileData );
    }

    void writeFileData(
        std::FILE &                     _file
        , const brownsugar::FileData &  _FILE_DATA
    )
    {
        std::fwrite(
            _FILE_DATA.data()
            , _FILE_DATA.size()
            , 1
            , &_file
        );
        if( std::ferror( &_file ) != 0 ) {
            throw std::runtime_error( "fwrite()が失敗" );
        }
    }
}

namespace brownsugar {
    FileData readFile(
        const std::string & _FILE_PATH
    )
    {
        auto    fileUnique = FileUnique(
            fopen64(
                _FILE_PATH.c_str()
                , "rb"
            )
        );
        if( fileUnique.get() == nullptr ) {
            throw std::runtime_error( "fopen64()が失敗" );
        }

        return readFileData( *fileUnique );
    }

    void writeFile(
        const std::string & _FILE_PATH
        , const FileData &  _FILE_DATA
    )
    {
        auto    fileUnique = FileUnique(
            fopen64(
                _FILE_PATH.c_str()
                , "wb"
            )
        );
        if( fileUnique.get() == nullptr ) {
            throw std::runtime_error( "fopen64()が失敗" );
        }
        auto &  file = *fileUnique;

        writeFileData(
            file
            , _FILE_DATA
        );
    }
}
