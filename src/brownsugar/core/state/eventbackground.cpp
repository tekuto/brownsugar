﻿#include "fg/util/export.h"
#include "fg/core/state/eventbackground.h"
#include "brownsugar/core/state/eventimpl.h"

FgState * fgStateEventBackgroundGetState(
    FgStateEventBackground *    _implPtr
)
{
    return reinterpret_cast< brownsugar::StateEvent * >( _implPtr )->getState();
}

void * fgStateEventBackgroundGetData(
    FgStateEventBackground *    _implPtr
)
{
    return reinterpret_cast< brownsugar::StateEvent * >( _implPtr )->getData();
}
