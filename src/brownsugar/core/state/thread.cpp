﻿#include "fg/util/export.h"
#include "fg/core/state/thread.h"
#include "brownsugar/core/state/threadimpl.h"

FgState * fgStateThreadGetState(
    FgStateThread * _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadGetData(
    FgStateThread * _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getData();
}
