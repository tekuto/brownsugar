﻿#include "fg/util/export.h"
#include "fg/core/state/threadsingleton.h"
#include "brownsugar/core/state/threadimpl.h"

FgState * fgStateThreadSingletonGetState(
    FgStateThreadSingleton *    _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadSingletonGetData(
    FgStateThreadSingleton *    _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getData();
}
