﻿#include "fg/util/test.h"
#include "brownsugar/core/state/eventmanager.h"
#include "brownsugar/core/state/eventimpl.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/thread/cache.h"
#include "fg/core/state/event.h"
#include "fg/core/state/eventbackground.h"
#include "fg/core/state/joiner.h"
#include "fg/common/unique.h"

#include <thread>
#include <chrono>

void dummyDestroy(
    void *
)
{
}

void stateEventManagerTestProc(
    FgStateEvent *
)
{
}

TEST(
    StateEventManagerTest
    , Create
)
{
    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    const auto &    MANAGER = *managerUnique;

    ASSERT_EQ( 0, MANAGER->registerInfoUniques.size() );
}

TEST(
    StateEventManagerTest
    , RegisterEventProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = **managerUnique;

    auto    unregisterer = manager.registerEventProc(
        state
        , stateEventManagerTestProc
    );
    ASSERT_NE( nullptr, unregisterer.get() );

    const auto &    REGISTER_INFO_UNIQUES = manager.registerInfoUniques;
    ASSERT_EQ( 1, REGISTER_INFO_UNIQUES.size() );
    const auto &    REGISTER_INFO = *( REGISTER_INFO_UNIQUES[ 0 ] );
    ASSERT_FALSE( REGISTER_INFO.BACKGROUND );
    ASSERT_EQ( &manager, &( REGISTER_INFO.manager ) );
    ASSERT_EQ( &state, &( REGISTER_INFO.state ) );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( stateEventManagerTestProc ), REGISTER_INFO.eventProcPtr );
    ASSERT_NE( nullptr, REGISTER_INFO.joinerUnique.get() );
}

TEST(
    StateEventManagerTest
    , UnregisterEventProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = **managerUnique;

    auto    unregisterer = manager.registerEventProc(
        state
        , stateEventManagerTestProc
    );
    ASSERT_NE( nullptr, unregisterer.get() );

    unregisterer.reset();

    ASSERT_EQ( 0, manager.registerInfoUniques.size() );
}

void registerEventBackgroundTestProc(
    FgStateEventBackground *
)
{
}

TEST(
    StateEventManagerTest
    , RegisterEventProc_background
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = **managerUnique;

    auto    unregisterer = manager.registerEventProc(
        state
        , registerEventBackgroundTestProc
    );
    ASSERT_NE( nullptr, unregisterer.get() );

    const auto &    REGISTER_INFO_UNIQUES = manager.registerInfoUniques;
    ASSERT_EQ( 1, REGISTER_INFO_UNIQUES.size() );
    const auto &    REGISTER_INFO = *( REGISTER_INFO_UNIQUES[ 0 ] );
    ASSERT_TRUE( REGISTER_INFO.BACKGROUND );
    ASSERT_EQ( &manager, &( REGISTER_INFO.manager ) );
    ASSERT_EQ( &state, &( REGISTER_INFO.state ) );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( registerEventBackgroundTestProc ), REGISTER_INFO.eventProcPtr );
    ASSERT_NE( nullptr, REGISTER_INFO.joinerUnique.get() );
}

struct ExecuteTestData : public fg::UniqueWrapper< ExecuteTestData >
{
    int &   i;

    ExecuteTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new ExecuteTestData( _i );
    }
};

void executeTestProc(
    fg::StateEvent< ExecuteTestData, int > &    _event
)
{
    _event.getState().getData().i += _event.getData();
}

TEST(
    StateEventManagerTest
    , Execute
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = **stateUnique;

    state->threadJoinerUnique->join();

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = **managerUnique;

    auto    unregisterer = manager.registerEventProc(
        state
        , reinterpret_cast< FgStateEventProc >( executeTestProc )
    );
    ASSERT_NE( nullptr, unregisterer.get() );

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    managerUnique->execute(
        joiner
        , j
    );

    joiner.join();

    ASSERT_EQ( 30, i );
}

void executeJoinedWhenUnregisteredTestProc(
    fg::StateEvent< ExecuteTestData, int > &    _event
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    _event.getState().getData().i += _event.getData();
}

TEST(
    StateEventManagerTest
    , Execute_joinedWhenUnregistered
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = **stateUnique;

    state->threadJoinerUnique->join();

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = **managerUnique;

    auto    unregisterer = manager.registerEventProc(
        state
        , reinterpret_cast< FgStateEventProc >( executeJoinedWhenUnregisteredTestProc )
    );
    ASSERT_NE( nullptr, unregisterer.get() );

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    managerUnique->execute(
        joiner
        , j
    );

    unregisterer.reset();

    ASSERT_EQ( 30, i );
}

void executeBackgroundTestProc(
    fg::StateEventBackground< ExecuteTestData, int > &  _event
)
{
    _event.getState().getData().i += _event.getData();
}

TEST(
    StateEventManagerTest
    , Execute_background
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state->nextStateUnique->threadJoinerUnique->join();

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = **managerUnique;

    auto    unregisterer = manager.registerEventProc(
        *state
        , reinterpret_cast< FgStateEventBackgroundProc >( executeBackgroundTestProc )
    );
    ASSERT_NE( nullptr, unregisterer.get() );

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    managerUnique->execute(
        joiner
        , j
    );

    joiner.join();

    ASSERT_EQ( 30, i );
}
