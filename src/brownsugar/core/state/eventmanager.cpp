﻿#include "fg/util/export.h"
#include "brownsugar/core/state/eventmanager.h"
#include "brownsugar/core/state/eventimpl.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/state/joiner.h"

#include <mutex>
#include <algorithm>
#include <utility>

namespace {
    void addRegisterInfo(
        FgStateEventManager &                           _manager
        , FgStateEventManager::RegisterInfo::Unique &&  _infoUnique
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        _manager.registerInfoUniques.push_back( std::move( _infoUnique ) );
    }

    void removeRegisterInfo(
        FgStateEventManager &                       _manager
        , const FgStateEventManager::RegisterInfo & _INFO
    )
    {
        auto &  registerInfoUniques = _manager.registerInfoUniques;

        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        const auto  END = registerInfoUniques.end();

        const auto  IT = std::find_if(
            registerInfoUniques.begin()
            , END
            , [
                &_INFO
            ]
            (
                const FgStateEventManager::RegisterInfoUniques::value_type &    _INFO_UNIQUE
            )
            {
                return _INFO_UNIQUE.get() == &_INFO;
            }
        );
        if( IT == END ) {
            return;
        }

        registerInfoUniques.erase( IT );
    }
}

FgStateEventManager::RegisterInfo::RegisterInfo(
    bool                            _background
    , FgStateEventManager &         _manager
    , FgState &                     _state
    , brownsugar::StateEvent::Proc  _eventProcPtr
)
    : BACKGROUND( _background )
    , manager( _manager )
    , state( _state )
    , eventProcPtr( _eventProcPtr )
    , joinerUnique( fg::StateJoiner::create() )
{
}

FgStateEventManager::RegisterInfo::Unique FgStateEventManager::RegisterInfo::create(
    FgStateEventManager &   _manager
    , FgState &             _state
    , FgStateEventProc      _eventProcPtr
)
{
    return new FgStateEventManager::RegisterInfo(
        false
        , _manager
        , _state
        , reinterpret_cast< brownsugar::StateEvent::Proc >( _eventProcPtr )
    );
}

FgStateEventManager::RegisterInfo::Unique FgStateEventManager::RegisterInfo::create(
    FgStateEventManager &           _manager
    , FgState &                     _state
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    return new FgStateEventManager::RegisterInfo(
        true
        , _manager
        , _state
        , reinterpret_cast< brownsugar::StateEvent::Proc >( _eventProcPtr )
    );
}

void FgStateEventManager::UnregisterEvent::operator()(
    FgStateEventManager::RegisterInfo * _infoPtr
) const
{
    auto &  info = *_infoPtr;
    auto &  manager = info.manager;

    removeRegisterInfo(
        manager
        , info
    );
}

FgStateEventManager::EventProcUnregisterer FgStateEventManager::registerEventProc(
    FgState &           _state
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  manager = *this;

    auto    infoUnique = FgStateEventManager::RegisterInfo::create(
        manager
        , _state
        , _eventProcPtr
    );
    auto &  info = *infoUnique;

    addRegisterInfo(
        manager
        , std::move( infoUnique )
    );

    return FgStateEventManager::EventProcUnregisterer( &info );
}

FgStateEventManager::EventProcUnregisterer FgStateEventManager::registerEventProc(
    FgState &                       _state
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    auto &  manager = *this;

    auto    infoUnique = FgStateEventManager::RegisterInfo::create(
        manager
        , _state
        , _eventProcPtr
    );
    auto &  info = *infoUnique;

    addRegisterInfo(
        manager
        , std::move( infoUnique )
    );

    return FgStateEventManager::EventProcUnregisterer( &info );
}

FgStateEventManager * fgStateEventManagerCreate(
)
{
    return new FgStateEventManager;
}

void fgStateEventManagerDestroy(
    FgStateEventManager *   _implPtr
)
{
    delete _implPtr;
}

void fgStateEventManagerExecute(
    FgStateEventManager *   _implPtr
    , FgStateJoiner *       _joinerPtr
    , void *                _eventDataPtr
)
{
    auto    lock = std::unique_lock< std::mutex >( _implPtr->mutex );

    for( auto & registerInfoUnique : _implPtr->registerInfoUniques ) {
        auto &  registerInfo = *registerInfoUnique;

        if( registerInfo.BACKGROUND == false ) {
            registerInfo.state.executeEvent(
                registerInfo.eventProcPtr
                , *( registerInfo.joinerUnique )
                , *_joinerPtr
                , _eventDataPtr
            );
        } else {
            registerInfo.state.executeEventBackground(
                registerInfo.eventProcPtr
                , *( registerInfo.joinerUnique )
                , *_joinerPtr
                , _eventDataPtr
            );
        }
    }
}
