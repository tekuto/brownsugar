﻿#include "fg/util/export.h"
#include "fg/core/state/creating.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/state/eventimpl.h"

FgCreatingState * fgCreatingStateCreate(
    FgState *   _prevStatePtr
)
{
    return reinterpret_cast< FgCreatingState * >(
        FgState::create(
            _prevStatePtr->threadCache
            , _prevStatePtr
            , true
            , nullptr
            , nullptr
        ).release()
    );
}

void fgCreatingStateDestroy(
    FgCreatingState *   _implPtr
)
{
    delete reinterpret_cast< FgState * >( _implPtr );
}

void fgCreatingStateSetData(
    FgCreatingState *                   _implPtr
    , void *                            _dataPtr
    , FgCreatingStateDataDestroyProc    _dataDestroyProcPtr
)
{
    reinterpret_cast< FgState * >( _implPtr )->setData(
        _dataPtr
        , _dataDestroyProcPtr
    );
}

void fgCreatingStateSetEnterEventProc(
    FgCreatingState *   _implPtr
    , FgStateEventProc  _procPtr
)
{
    reinterpret_cast< FgState * >( _implPtr )->setEnterEventProc( reinterpret_cast< brownsugar::StateEvent::Proc >( _procPtr ) );
}

void fgCreatingStateSetExitEventProc(
    FgCreatingState *   _implPtr
    , FgStateEventProc  _procPtr
)
{
    reinterpret_cast< FgState * >( _implPtr )->setExitEventProc( reinterpret_cast< brownsugar::StateEvent::Proc >( _procPtr ) );
}

void fgCreatingStateSetReenterEventProc(
    FgCreatingState *   _implPtr
    , FgStateEventProc  _procPtr
)
{
    reinterpret_cast< FgState * >( _implPtr )->setReenterEventProc( reinterpret_cast< brownsugar::StateEvent::Proc >( _procPtr ) );
}

void fgCreatingStateSetLeaveEventProc(
    FgCreatingState *   _implPtr
    , FgStateEventProc  _procPtr
)
{
    reinterpret_cast< FgState * >( _implPtr )->setLeaveEventProc( reinterpret_cast< brownsugar::StateEvent::Proc >( _procPtr ) );
}

void * fgCreatingStateGetBasesystem(
    FgCreatingState *   _implPtr
)
{
    return fgStateGetBasesystem( reinterpret_cast< FgState * >( _implPtr ) );
}
