﻿#include "fg/util/test.h"
#include "fg/core/state/threadsingleton.h"
#include "fg/core/state/state.h"
#include "brownsugar/core/state/threadimpl.h"

struct StateThreadSingletonTestData;

TEST(
    StateThreadSingletonTest
    , GetState
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThreadSingleton< StateThreadSingletonTestData, int > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadSingletonTestData > * >( 20 ), &( thread.getState() ) );
}

TEST(
    StateThreadSingletonTest
    , GetState_withoutUserData
)
{
    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , nullptr
    );
    auto &  thread = reinterpret_cast< fg::StateThreadSingleton< StateThreadSingletonTestData > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadSingletonTestData > * >( 10 ), &( thread.getState() ) );
}

TEST(
    StateThreadSingletonTest
    , GetData
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThreadSingleton< StateThreadSingletonTestData, int > & >( threadImpl );

    ASSERT_EQ( 10, thread.getData() );
}
