﻿#include "fg/util/test.h"
#include "fg/core/state/threadbackground.h"
#include "fg/core/state/state.h"
#include "brownsugar/core/state/threadimpl.h"

struct StateThreadBackgroundTestData;

TEST(
    StateThreadBackgroundTest
    , GetState
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThreadBackground< StateThreadBackgroundTestData, int > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadBackgroundTestData > * >( 20 ), &( thread.getState() ) );
}

TEST(
    StateThreadBackgroundTest
    , GetState_withoutUserData
)
{
    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , nullptr
    );
    auto &  thread = reinterpret_cast< fg::StateThreadBackground< StateThreadBackgroundTestData > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadBackgroundTestData > * >( 10 ), &( thread.getState() ) );
}

TEST(
    StateThreadBackgroundTest
    , GetData
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThreadBackground< StateThreadBackgroundTestData, int > & >( threadImpl );

    ASSERT_EQ( 10, thread.getData() );
}
