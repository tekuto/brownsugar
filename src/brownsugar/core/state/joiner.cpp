﻿#include "fg/util/export.h"
#include "brownsugar/core/state/joiner.h"

#include <mutex>

void FgStateJoiner::End::operator()(
    FgStateJoiner * _joinerPtr
) const
{
    auto &  runningCount = _joinerPtr->runningCount;

    auto    lock = std::unique_lock< std::mutex >( _joinerPtr->mutex );

    runningCount--;

    if( runningCount <= 0 ) {
        _joinerPtr->cond.notify_all();
    }
}

void FgStateJoiner::Join::operator()(
    FgStateJoiner * _joinerPtr
) const
{
    fgStateJoinerJoin( _joinerPtr );
}

FgStateJoiner::Ender FgStateJoiner::start(
)
{
    auto    lock = std::unique_lock< std::mutex >( this->mutex );

    this->runningCount++;

    return FgStateJoiner::Ender( this );
}

FgStateJoiner::FgStateJoiner(
)
    : runningCount( 0 )
    , joiner( this )
{
}

FgStateJoiner * fgStateJoinerCreate(
)
{
    return new FgStateJoiner;
}

void fgStateJoinerDestroy(
    FgStateJoiner * _implPtr
)
{
    delete _implPtr;
}

void fgStateJoinerJoin(
    FgStateJoiner * _implPtr
)
{
    auto    lock = std::unique_lock< std::mutex >( _implPtr->mutex );

    while( _implPtr->runningCount > 0 ) {
        _implPtr->cond.wait( lock );
    }
}
