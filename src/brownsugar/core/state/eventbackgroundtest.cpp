﻿#include "fg/util/test.h"
#include "fg/core/state/eventbackground.h"
#include "fg/core/state/state.h"
#include "brownsugar/core/state/eventimpl.h"

struct StateEventBackgroundTestData;

TEST(
    StateEventBackgroundTest
    , GetState
)
{
    auto    i = 10;

    auto    eventImpl = brownsugar::StateEvent(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  event = reinterpret_cast< fg::StateEventBackground< StateEventBackgroundTestData, int > & >( eventImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateEventBackgroundTestData > * >( 20 ), &( event.getState() ) );
}

TEST(
    StateEventBackgroundTest
    , GetData
)
{
    auto    i = 10;

    auto    eventImpl = brownsugar::StateEvent(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  event = reinterpret_cast< fg::StateEventBackground< StateEventBackgroundTestData, int > & >( eventImpl );

    ASSERT_EQ( 10, event.getData() );
}
