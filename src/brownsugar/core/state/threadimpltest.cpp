﻿#include "fg/util/test.h"
#include "brownsugar/core/state/threadimpl.h"

TEST(
    StateThreadImplTest
    , GetState
)
{
    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , nullptr
    );

    ASSERT_EQ( reinterpret_cast< FgState * >( 10 ), threadImpl.getState() );
}

TEST(
    StateThreadImplTest
    , GetData
)
{
    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , reinterpret_cast< void * >( 20 )
    );

    ASSERT_EQ( reinterpret_cast< void * >( 20 ), threadImpl.getData() );
}
