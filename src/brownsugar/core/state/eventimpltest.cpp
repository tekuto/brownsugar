﻿#include "fg/util/test.h"
#include "brownsugar/core/state/eventimpl.h"

TEST(
    StateEventImplTest
    , GetState
)
{
    auto    i = 10;

    auto    event = brownsugar::StateEvent(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );

    ASSERT_EQ( reinterpret_cast< FgState * >( 20 ), event.getState() );
}

TEST(
    StateEventImplTest
    , GetData
)
{
    auto    i = 10;

    auto    event = brownsugar::StateEvent(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );

    ASSERT_EQ( 10, *static_cast< const int * >( event.getData() ) );
}
