﻿#include "fg/util/export.h"
#include "brownsugar/core/state/eventregistermanager.h"
#include "brownsugar/core/state/eventimpl.h"

FgStateEventRegisterManager * fgStateEventRegisterManagerCreateForCreatingState(
    FgStateEventManager *   _managerPtr
    , FgCreatingState *     _statePtr
    , FgStateEventProc      _eventProcPtr
)
{
    return fgStateEventRegisterManagerCreateForState(
        _managerPtr
        , reinterpret_cast< FgState * >( _statePtr )
        , _eventProcPtr
    );
}

FgStateEventRegisterManager * fgStateEventRegisterManagerCreateForState(
    FgStateEventManager *   _managerPtr
    , FgState *             _statePtr
    , FgStateEventProc      _eventProcPtr
)
{
    return new FgStateEventRegisterManager{
        _managerPtr->registerEventProc(
            *_statePtr
            , _eventProcPtr
        ),
    };
}

FgStateEventRegisterManager * fgStateEventRegisterManagerCreateBackgroundForCreatingState(
    FgStateEventManager *           _managerPtr
    , FgCreatingState *             _statePtr
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    return fgStateEventRegisterManagerCreateBackgroundForState(
        _managerPtr
        , reinterpret_cast< FgState * >( _statePtr )
        , _eventProcPtr
    );
}

FgStateEventRegisterManager * fgStateEventRegisterManagerCreateBackgroundForState(
    FgStateEventManager *           _managerPtr
    , FgState *                     _statePtr
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    return new FgStateEventRegisterManager{
        _managerPtr->registerEventProc(
            *_statePtr
            , _eventProcPtr
        ),
    };
}

void fgStateEventRegisterManagerDestroy(
    FgStateEventRegisterManager *   _implPtr
)
{
    delete _implPtr;
}
