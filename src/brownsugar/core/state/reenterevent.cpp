﻿#include "fg/util/export.h"
#include "brownsugar/core/state/reenterevent.h"

FgStateReenterEventData::FgStateReenterEventData(
    FgCreatingStateDataDestroyProc  _prevDataDestroyProcPtr
)
    : prevDataDestroyProcPtr( _prevDataDestroyProcPtr )
{
}

const void * fgStateReenterEventDataGetPrevDataDestroyProcPtr(
    const FgStateReenterEventData * _IMPL_PTR
)
{
    return reinterpret_cast< const void * >( _IMPL_PTR->prevDataDestroyProcPtr );
}
