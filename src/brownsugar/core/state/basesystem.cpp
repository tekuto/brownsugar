﻿#include "fg/util/export.h"
#include "brownsugar/core/state/basesystem.h"
#include "brownsugar/core/state/state.h"
#include "fg/core/state/state.h"

FgBasesystemState::FgBasesystemState(
    FgState &           _state
    , FgModuleContext & _moduleContext
)
    : state( _state )
    , moduleContext( _moduleContext )
    , existsWaitEndProc( false )
{
}

FgBasesystemState * FgBasesystemState::create_(
    FgState &           _state
    , FgModuleContext & _moduleContext
)
{
    return new FgBasesystemState(
        _state
        , _moduleContext
    );
}

void FgBasesystemState::destroy(
    FgBasesystemState * _this
)
{
    delete _this;
}

bool FgBasesystemState::isExistsWaitEndProc(
) const
{
    return this->existsWaitEndProc;
}

void fgBasesystemStateSetData(
    FgBasesystemState *                 _implPtr
    , void *                            _dataPtr
    , FgBasesystemStateDataDestroyProc  _dataDestroyProcPtr
)
{
    _implPtr->state.setData(
        _dataPtr
        , _dataDestroyProcPtr
    );
}

void fgBasesystemStateSetWaitEndProc(
    FgBasesystemState *             _implPtr
    , FgBasesystemStateWaitEndProc
    , void *
)
{
    _implPtr->existsWaitEndProc = true;
}

FgState * fgBasesystemStateGetState(
    FgBasesystemState * _implPtr
)
{
    return &( _implPtr->state );
}

FgModuleContext * fgBasesystemStateGetModuleContext(
    FgBasesystemState * _implPtr
)
{
    return &( _implPtr->moduleContext );
}
