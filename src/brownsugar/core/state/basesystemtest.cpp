﻿#include "fg/util/test.h"
#include "brownsugar/core/state/basesystem.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/thread/cache.h"
#include "fg/common/unique.h"

void dummyDestroy(
    void *
)
{
}

struct BasesystemStateTestData : public fg::UniqueWrapper< BasesystemStateTestData >
{
    static Unique create(
    )
    {
        return new BasesystemStateTestData;
    }
};

TEST(
    BasesystemStateTest
    , Create
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    const auto  BASESYSTEM_STATE_UNIQUE = FgBasesystemState::create(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    ASSERT_NE( nullptr, BASESYSTEM_STATE_UNIQUE.get() );
    const auto &    BASESYSTEM_STATE = *BASESYSTEM_STATE_UNIQUE;

    ASSERT_EQ( &state, &( BASESYSTEM_STATE.state ) );
    ASSERT_EQ( reinterpret_cast< FgModuleContext * >( 10 ), &( BASESYSTEM_STATE.moduleContext ) );
    ASSERT_FALSE( BASESYSTEM_STATE.existsWaitEndProc );
}

void isExistsWaitEndProcTestProc(
    int &
)
{
}

TEST(
    BasesystemStateTest
    , IsExistsWaitEndProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemStateUnique = FgBasesystemState::create(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    ASSERT_NE( nullptr, basesystemStateUnique.get() );
    auto &  basesystemState = reinterpret_cast< fg::BasesystemState< BasesystemStateTestData > & >( *basesystemStateUnique );

    ASSERT_FALSE( basesystemState->isExistsWaitEndProc() );

    auto    i = 20;

    basesystemState.setWaitEndProc(
        isExistsWaitEndProcTestProc
        , i
    );

    ASSERT_TRUE( basesystemState->isExistsWaitEndProc() );
}

struct SetDataTestData : public fg::UniqueWrapper< SetDataTestData >
{
    int &   i;

    SetDataTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new SetDataTestData( _i );
    }

    static void destroy(
        SetDataTestData *   _this
    )
    {
        _this->i = 20;

        delete _this;
    }
};

TEST(
    BasesystemStateTest
    , SetData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemStateUnique = FgBasesystemState::create(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    ASSERT_NE( nullptr, basesystemStateUnique.get() );
    auto &  basesystemState = reinterpret_cast< fg::BasesystemState< SetDataTestData > & >( *basesystemStateUnique );

    auto    i = 10;

    auto    dataUnique = SetDataTestData::create( i );
    ASSERT_NE( nullptr, dataUnique.get() );

    basesystemState.setData(
        dataUnique.release()
        , SetDataTestData::destroy
    );

    stateUnique.destroy();

    ASSERT_EQ( 20, i );
}

void setWaitEndProcTestProc(
    int &
)
{
}

TEST(
    BasesystemStateTest
    , SetWaitEndProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    basesystemStateUnique = FgBasesystemState::create(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    ASSERT_NE( nullptr, basesystemStateUnique.get() );
    auto &  basesystemState = reinterpret_cast< fg::BasesystemState< BasesystemStateTestData > & >( *basesystemStateUnique );

    auto    i = 20;

    basesystemState.setWaitEndProc(
        setWaitEndProcTestProc
        , i
    );

    ASSERT_TRUE( basesystemState->existsWaitEndProc );
}

TEST(
    BasesystemStateTest
    , GetState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< BasesystemStateTestData > & >( *stateUnique );

    auto    basesystemStateUnique = FgBasesystemState::create(
        *state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    ASSERT_NE( nullptr, basesystemStateUnique.get() );
    auto &  basesystemState = reinterpret_cast< fg::BasesystemState< BasesystemStateTestData > & >( *basesystemStateUnique );

    ASSERT_EQ( &state, &( basesystemState.getState() ) );
}

TEST(
    BasesystemStateTest
    , GetModuleContext
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< BasesystemStateTestData > & >( *stateUnique );

    auto    basesystemStateUnique = FgBasesystemState::create(
        *state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    ASSERT_NE( nullptr, basesystemStateUnique.get() );
    auto &  basesystemState = reinterpret_cast< fg::BasesystemState< BasesystemStateTestData > & >( *basesystemStateUnique );

    ASSERT_EQ( reinterpret_cast< fg::ModuleContext * >( 10 ), &( basesystemState.getModuleContext() ) );
}
