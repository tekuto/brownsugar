﻿#include "fg/util/test.h"
#include "fg/core/state/event.h"
#include "fg/core/state/state.h"
#include "brownsugar/core/state/eventimpl.h"

struct StateEventTestData;

TEST(
    StateEventTest
    , GetState
)
{
    auto    i = 10;

    auto    eventImpl = brownsugar::StateEvent(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  event = reinterpret_cast< fg::StateEvent< StateEventTestData, int > & >( eventImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateEventTestData > * >( 20 ), &( event.getState() ) );
}

TEST(
    StateEventTest
    , GetData
)
{
    auto    i = 10;

    auto    eventImpl = brownsugar::StateEvent(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  event = reinterpret_cast< fg::StateEvent< StateEventTestData, int > & >( eventImpl );

    ASSERT_EQ( 10, event.getData() );
}
