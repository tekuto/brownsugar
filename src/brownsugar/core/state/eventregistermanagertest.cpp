﻿#include "fg/util/test.h"
#include "brownsugar/core/state/eventregistermanager.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/state/eventmanager.h"
#include "brownsugar/core/thread/cache.h"
#include "fg/core/state/event.h"
#include "fg/core/state/eventbackground.h"
#include "fg/common/unique.h"

void dummyDestroy(
    void *
)
{
}

struct StateEventRegisterManagerTestData : public fg::UniqueWrapper< StateEventRegisterManagerTestData >
{
    static Unique create(
    )
    {
        return new StateEventRegisterManagerTestData;
    }
};

void stateEventRegisterManagerTestProc(
    fg::StateEvent< StateEventRegisterManagerTestData, int > &
)
{
}

TEST(
    StateEventRegisterManagerTest
    , CreateForCreatingState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState< StateEventRegisterManagerTestData > &
        )
        {
            return StateEventRegisterManagerTestData::create().release();
        }
        , StateEventRegisterManagerTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< StateEventRegisterManagerTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto &  creatingState = reinterpret_cast< fg::CreatingState< StateEventRegisterManagerTestData > & >( state );

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    const auto  REGISTER_MANAGER_UNIQUE = fg::StateEventRegisterManager< int >::create(
        manager
        , creatingState
        , stateEventRegisterManagerTestProc
    );
    ASSERT_NE( nullptr, REGISTER_MANAGER_UNIQUE.get() );
    const auto &    REGISTER_MANAGER = *REGISTER_MANAGER_UNIQUE;

    ASSERT_NE( nullptr, REGISTER_MANAGER->unregisterer.get() );

    const auto &    REGISTER_INFO_UNIQUES = manager->registerInfoUniques;
    ASSERT_EQ( 1, REGISTER_INFO_UNIQUES.size() );
    const auto &    REGISTER_INFO = *( REGISTER_INFO_UNIQUES[ 0 ] );
    ASSERT_FALSE( REGISTER_INFO.BACKGROUND );
    ASSERT_EQ( &*manager, &( REGISTER_INFO.manager ) );
    ASSERT_EQ( &*state, &( REGISTER_INFO.state ) );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( stateEventRegisterManagerTestProc ), REGISTER_INFO.eventProcPtr );
    ASSERT_NE( nullptr, REGISTER_INFO.joinerUnique.get() );
}

TEST(
    StateEventRegisterManagerTest
    , CreateForState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState< StateEventRegisterManagerTestData > &
        )
        {
            return StateEventRegisterManagerTestData::create().release();
        }
        , StateEventRegisterManagerTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< StateEventRegisterManagerTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    const auto  REGISTER_MANAGER_UNIQUE = fg::StateEventRegisterManager< int >::create(
        manager
        , state
        , stateEventRegisterManagerTestProc
    );
    ASSERT_NE( nullptr, REGISTER_MANAGER_UNIQUE.get() );
    const auto &    REGISTER_MANAGER = *REGISTER_MANAGER_UNIQUE;

    ASSERT_NE( nullptr, REGISTER_MANAGER->unregisterer.get() );

    const auto &    REGISTER_INFO_UNIQUES = manager->registerInfoUniques;
    ASSERT_EQ( 1, REGISTER_INFO_UNIQUES.size() );
    const auto &    REGISTER_INFO = *( REGISTER_INFO_UNIQUES[ 0 ] );
    ASSERT_FALSE( REGISTER_INFO.BACKGROUND );
    ASSERT_EQ( &*manager, &( REGISTER_INFO.manager ) );
    ASSERT_EQ( &*state, &( REGISTER_INFO.state ) );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( stateEventRegisterManagerTestProc ), REGISTER_INFO.eventProcPtr );
    ASSERT_NE( nullptr, REGISTER_INFO.joinerUnique.get() );
}

void createBackgroundTestProc(
    fg::StateEventBackground< StateEventRegisterManagerTestData, int > &
)
{
}

TEST(
    StateEventRegisterManagerTest
    , CreateBackgroundForCreatingState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState< StateEventRegisterManagerTestData > &
        )
        {
            return StateEventRegisterManagerTestData::create().release();
        }
        , StateEventRegisterManagerTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< StateEventRegisterManagerTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto &  creatingState = reinterpret_cast< fg::CreatingState< StateEventRegisterManagerTestData > & >( state );

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    const auto  REGISTER_MANAGER_UNIQUE = fg::StateEventRegisterManager< int >::create(
        manager
        , creatingState
        , createBackgroundTestProc
    );
    ASSERT_NE( nullptr, REGISTER_MANAGER_UNIQUE.get() );
    const auto &    REGISTER_MANAGER = *REGISTER_MANAGER_UNIQUE;

    ASSERT_NE( nullptr, REGISTER_MANAGER->unregisterer.get() );

    const auto &    REGISTER_INFO_UNIQUES = manager->registerInfoUniques;
    ASSERT_EQ( 1, REGISTER_INFO_UNIQUES.size() );
    const auto &    REGISTER_INFO = *( REGISTER_INFO_UNIQUES[ 0 ] );
    ASSERT_TRUE( REGISTER_INFO.BACKGROUND );
    ASSERT_EQ( &*manager, &( REGISTER_INFO.manager ) );
    ASSERT_EQ( &*state, &( REGISTER_INFO.state ) );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( createBackgroundTestProc ), REGISTER_INFO.eventProcPtr );
    ASSERT_NE( nullptr, REGISTER_INFO.joinerUnique.get() );
}

TEST(
    StateEventRegisterManagerTest
    , CreateBackgroundForState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState< StateEventRegisterManagerTestData > &
        )
        {
            return StateEventRegisterManagerTestData::create().release();
        }
        , StateEventRegisterManagerTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< StateEventRegisterManagerTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    const auto  REGISTER_MANAGER_UNIQUE = fg::StateEventRegisterManager< int >::create(
        manager
        , state
        , createBackgroundTestProc
    );
    ASSERT_NE( nullptr, REGISTER_MANAGER_UNIQUE.get() );
    const auto &    REGISTER_MANAGER = *REGISTER_MANAGER_UNIQUE;

    ASSERT_NE( nullptr, REGISTER_MANAGER->unregisterer.get() );

    const auto &    REGISTER_INFO_UNIQUES = manager->registerInfoUniques;
    ASSERT_EQ( 1, REGISTER_INFO_UNIQUES.size() );
    const auto &    REGISTER_INFO = *( REGISTER_INFO_UNIQUES[ 0 ] );
    ASSERT_TRUE( REGISTER_INFO.BACKGROUND );
    ASSERT_EQ( &*manager, &( REGISTER_INFO.manager ) );
    ASSERT_EQ( &*state, &( REGISTER_INFO.state ) );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( createBackgroundTestProc ), REGISTER_INFO.eventProcPtr );
    ASSERT_NE( nullptr, REGISTER_INFO.joinerUnique.get() );
}

TEST(
    StateEventRegisterManagerTest
    , Destroy
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState< StateEventRegisterManagerTestData > &
        )
        {
            return StateEventRegisterManagerTestData::create().release();
        }
        , StateEventRegisterManagerTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< StateEventRegisterManagerTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    managerUnique = fg::StateEventManager< int >::create();
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    registerManagerUnique = fg::StateEventRegisterManager< int >::create(
        manager
        , state
        , stateEventRegisterManagerTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );

    registerManagerUnique.destroy();

    ASSERT_EQ( 0, manager->registerInfoUniques.size() );
}
