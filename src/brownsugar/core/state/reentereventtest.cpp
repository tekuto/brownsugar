﻿#include "fg/util/test.h"
#include "brownsugar/core/state/reenterevent.h"
#include "fg/common/unique.h"

void stateReenterEventDataTestProc(
    void *
)
{
}

TEST(
    StateReenterEventDataTest
    , Constructor
)
{
    auto    data = FgStateReenterEventData( stateReenterEventDataTestProc );

    ASSERT_EQ( stateReenterEventDataTestProc, data.prevDataDestroyProcPtr );
}

TEST(
    StateReenterEventDataTest
    , GetPrevDataDestroyProcPtr
)
{
    const auto  DATA_IMPL = FgStateReenterEventData( stateReenterEventDataTestProc );

    const auto &    DATA = reinterpret_cast< const fg::StateReenterEventData & >( DATA_IMPL );

    ASSERT_EQ( stateReenterEventDataTestProc, DATA.getPrevDataDestroyProcPtr() );
}

struct IsPrevStateTestData : public fg::UniqueWrapper< IsPrevStateTestData >
{
};

struct IsPrevStateTestData2 : public fg::UniqueWrapper< IsPrevStateTestData2 >
{
};

TEST(
    StateReenterEventDataTest
    , IsPrevState
)
{
    const auto  DATA_IMPL = FgStateReenterEventData( reinterpret_cast< FgCreatingStateDataDestroyProc >( IsPrevStateTestData::destroy ) );

    const auto &    DATA = reinterpret_cast< const fg::StateReenterEventData & >( DATA_IMPL );

    ASSERT_TRUE( DATA.isPrevState< IsPrevStateTestData >() );
    ASSERT_FALSE( DATA.isPrevState< IsPrevStateTestData2 >() );
}
