﻿#include "fg/util/export.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/state/eventimpl.h"
#include "brownsugar/core/state/reenterevent.h"
#include "brownsugar/core/state/threadimpl.h"
#include "brownsugar/core/state/joiner.h"
#include "brownsugar/core/thread/joiner.h"
#include "brownsugar/core/thread/cache.h"
#include "brownsugar/core/thread/thread.h"
#include "brownsugar/core/thread/singleton.h"
#include "fg/common/unique.h"

#include <utility>
#include <mutex>
#include <algorithm>
#include <stdexcept>

namespace {
    struct ExecuteEventData : public fg::UniqueWrapper< ExecuteEventData >
    {
        FgState &                       state;
        brownsugar::StateEvent::Proc    eventProcPtr;
        void *                          eventDataPtr;
        FgStateJoiner::Ender            eventProcEnder;
        FgStateJoiner::Ender            executeEnder;

        ExecuteEventData(
            FgState &                       _state
            , brownsugar::StateEvent::Proc  _eventProcPtr
            , void *                        _eventDataPtr
            , FgStateJoiner::Ender &&       _eventProcEnder
            , FgStateJoiner::Ender &&       _executeEnder
        )
            : state( _state )
            , eventProcPtr( _eventProcPtr )
            , eventDataPtr( _eventDataPtr )
            , eventProcEnder( std::move( _eventProcEnder ) )
            , executeEnder( std::move( _executeEnder ) )
        {
        }

        static Unique create(
            FgState &                       _state
            , brownsugar::StateEvent::Proc  _eventProcPtr
            , void *                        _eventDataPtr
            , FgStateJoiner::Ender &&       _eventProcEnder
            , FgStateJoiner::Ender &&       _executeEnder
        )
        {
            return new ExecuteEventData(
                _state
                , _eventProcPtr
                , _eventDataPtr
                , std::move( _eventProcEnder )
                , std::move( _executeEnder )
            );
        }
    };

    void executeEventProc(
        brownsugar::Thread< ExecuteEventData > &    _thread
    )
    {
        auto    dataUnique = ExecuteEventData::Unique( &( _thread.getData() ) );

        auto    event = brownsugar::StateEvent(
            dataUnique->state
            , dataUnique->eventDataPtr
        );

        dataUnique->eventProcPtr( event );
    }

    struct ThreadData : public fg::UniqueWrapper< ThreadData >
    {
        FgState &               state;
        FgStateThreadProc       threadProcPtr;
        FgStateJoiner::Ender    ender;
        void *                  userDataPtr;

        ThreadData(
            FgState &                   _state
            , FgStateThreadProc         _threadProcPtr
            , FgStateJoiner::Ender &&   _ender
            , void *                    _userDataPtr
        )
            : state( _state )
            , threadProcPtr( _threadProcPtr )
            , ender( std::move( _ender ) )
            , userDataPtr( _userDataPtr )
        {
        }

        static Unique create(
            FgState &           _state
            , FgStateThreadProc _threadProcPtr
        )
        {
            return new ThreadData(
                _state
                , _threadProcPtr
                , nullptr
                , nullptr
            );
        }

        static Unique create(
            FgState &                   _state
            , FgStateThreadProc         _threadProcPtr
            , FgStateJoiner::Ender &&   _ender
            , void *                    _userDataPtr
        )
        {
            return new ThreadData(
                _state
                , _threadProcPtr
                , std::move( _ender )
                , _userDataPtr
            );
        }
    };

    void threadProc(
        brownsugar::Thread< ThreadData > &  _thread
    )
    {
        auto    dataUnique = ThreadData::Unique( &( _thread.getData() ) );

        auto    thread = brownsugar::StateThread(
            dataUnique->state
            , dataUnique->userDataPtr
        );

        dataUnique->threadProcPtr( reinterpret_cast< FgStateThread * >( &thread ) );
    }

    FgState::ThreadSingletonData * findThreadData(
        FgState::ThreadSingletonDataSet &           _threadDataSet
        , FgStateThreadSingletonProc                _procPtr
        , FgStateJoiner *                           _joinerPtr
        , void *                                    _userDataPtr
        , const std::unique_lock< std::mutex > &
    )
    {
        for( auto & threadData : _threadDataSet ) {
            if( threadData.threadProcPtr != _procPtr ) {
                continue;
            }

            if( threadData.ender.get() != _joinerPtr ) {
                continue;
            }

            if( threadData.userDataPtr != _userDataPtr ) {
                continue;
            }

            return const_cast< FgState::ThreadSingletonData * >( &threadData );
        }

        return nullptr;
    }

    FgState::ThreadSingletonData & addThreadData(
        FgState::ThreadSingletonDataSet &           _threadDataSet
        , FgState &                                 _state
        , FgStateThreadSingletonProc                _procPtr
        , FgStateJoiner *                           _joinerPtr
        , void *                                    _userDataPtr
        , const std::unique_lock< std::mutex > &
    )
    {
        const auto  RESULT = _threadDataSet.insert(
            FgState::ThreadSingletonData(
                _state
                , _procPtr
                , _joinerPtr != nullptr
                    ? _joinerPtr->start()
                    : nullptr
                , _userDataPtr
            )
        );

        return const_cast< FgState::ThreadSingletonData & >( *( RESULT.first ) );
    }

    FgState::ThreadSingletonData & getThreadData(
        FgState &                       _state
        , FgStateThreadSingletonProc    _procPtr
        , FgStateJoiner *               _joinerPtr
        , void *                        _userDataPtr
    )
    {
        auto &  threadDataSet = _state.threadSingletonDataSet;

        auto    lock = std::unique_lock< std::mutex >( _state.threadSingletonMutex );

        auto    threadDataPtr = findThreadData(
            threadDataSet
            , _procPtr
            , _joinerPtr
            , _userDataPtr
            , lock
        );
        if( threadDataPtr != nullptr ) {
            threadDataPtr->reexecute = true;

            return *threadDataPtr;
        }

        return addThreadData(
            threadDataSet
            , _state
            , _procPtr
            , _joinerPtr
            , _userDataPtr
            , lock
        );
    }

    FgState::ThreadSingletonDataSet::iterator findThreadDataIterator(
        FgState::ThreadSingletonDataSet &           _threadDataSet
        , const FgState::ThreadSingletonData &      _DATA
        , const std::unique_lock< std::mutex > &
    )
    {
        const auto  END = _threadDataSet.end();

        auto    it = std::find_if(
            _threadDataSet.begin()
            , END
            , [
                &_DATA
            ]
            (
                const FgState::ThreadSingletonDataSet::value_type & _VALUE
            )
            {
                return &_VALUE == &_DATA;
            }
        );
        if( it == END ) {
            throw std::runtime_error( "スレッドに対応したスレッド情報が存在しない" );
        }

        return it;
    }

    void threadSingletonProc(
        brownsugar::ThreadSingleton< FgState::ThreadSingletonData > &   _thread
    )
    {
        auto &  data = _thread.getData();

        auto    thread = brownsugar::StateThread(
            data.state
            , data.userDataPtr
        );

        data.threadProcPtr( reinterpret_cast< FgStateThreadSingleton * >( &thread ) );

        auto &  state = data.state;

        auto &  threadDataSet = state.threadSingletonDataSet;

        auto    lock = std::unique_lock< std::mutex >( state.threadSingletonMutex );

        auto    it = findThreadDataIterator(
            threadDataSet
            , data
            , lock
        );

        auto &  reexecute = const_cast< bool & >( it->reexecute );

        if( reexecute == true ) {
            reexecute = false;
        } else {
            threadDataSet.erase( it );
        }
    }

    bool setDisabled(
        FgState &                                   _state
        , const std::unique_lock< std::mutex > &
    )
    {
        auto &  disabled = _state.disabled;

        if( disabled == true ) {
            return false;
        }

        disabled = true;

        return true;
    }

    void setEnabled(
        FgState &                                   _state
        , const std::unique_lock< std::mutex > &
    )
    {
        _state.disabled = false;
    }

    template< typename CALL_EVENT_PROC_T >
    void callEvent_(
        const bool &                            _EXISTS_EVENT
        , const brownsugar::StateEvent::Proc &  _EVENT_PROC_PTR
        , FgState &                             _state
        , const CALL_EVENT_PROC_T &             _CALL_EVENT_PROC
    )
    {
        if( _EXISTS_EVENT == false ) {
            return;
        }

        _CALL_EVENT_PROC(
            _EVENT_PROC_PTR
            , _state
        );
    }

    void callEvent_(
        const brownsugar::StateEvent::Proc &    _EVENT_PROC_PTR
        , FgState &                             _state
        , void *                                _eventDataPtr
    )
    {
        auto    event = brownsugar::StateEvent(
            _state
            , _eventDataPtr
        );

        _EVENT_PROC_PTR( event );
    }

    void callEvent(
        const bool &                            _EXISTS_EVENT
        , const brownsugar::StateEvent::Proc &  _EVENT_PROC_PTR
        , FgState &                             _state
    )
    {
        callEvent_(
            _EXISTS_EVENT
            , _EVENT_PROC_PTR
            , _state
            , [](
                const brownsugar::StateEvent::Proc &    _EVENT_PROC_PTR
                , FgState &                             _state
            )
            {
                callEvent_(
                    _EVENT_PROC_PTR
                    , _state
                    , nullptr
                );
            }
        );
    }

    template< typename EVENT_DATA_PROC_T >
    void callEvent(
        const bool &                            _EXISTS_EVENT
        , const brownsugar::StateEvent::Proc &  _EVENT_PROC_PTR
        , FgState &                             _state
        , const EVENT_DATA_PROC_T &             _EVENT_DATA_PROC
    )
    {
        callEvent_(
            _EXISTS_EVENT
            , _EVENT_PROC_PTR
            , _state
            , [
                &_EVENT_DATA_PROC
            ]
            (
                const brownsugar::StateEvent::Proc &    _EVENT_PROC_PTR
                , FgState &                             _state
            )
            {
                auto    eventData = _EVENT_DATA_PROC();

                callEvent_(
                    _EVENT_PROC_PTR
                    , _state
                    , &eventData
                );
            }
        );
    }

    void callEnterEvent(
        FgState &   _state
    )
    {
        callEvent(
            _state.existsEnterEvent
            , _state.enterEventProcPtr
            , _state
        );
    }

    void callExitEvent(
        FgState &   _state
    )
    {
        callEvent(
            _state.existsExitEvent
            , _state.exitEventProcPtr
            , _state
        );
    }

    void callReenterEvent(
        FgState &                           _state
        , FgCreatingStateDataDestroyProc    _prevDataDestroyProcPtr
    )
    {
        callEvent(
            _state.existsReenterEvent
            , _state.reenterEventProcPtr
            , _state
            , [
                &_prevDataDestroyProcPtr
            ]
            {
                return FgStateReenterEventData( _prevDataDestroyProcPtr );
            }
        );
    }

    void callLeaveEvent(
        FgState &   _state
    )
    {
        callEvent(
            _state.existsLeaveEvent
            , _state.leaveEventProcPtr
            , _state
        );
    }

    void enterThread(
        brownsugar::Thread< FgState > & _thread
    )
    {
        auto &  state = _thread.getData();

        auto &  prevState = *( state.prevStatePtr );

        prevState.threadJoinerUnique->join();

        callLeaveEvent( prevState );

        {
            auto    lock = std::unique_lock< std::mutex >( state.disabledMutex );

            setEnabled(
                state
                , lock
            );
        }

        callEnterEvent( state );
    }

    void exitNextThread(
        brownsugar::Thread< FgState > & _thread
    )
    {
        auto &  state = _thread.getData();

        auto &  nextStateUnique = state.nextStateUnique;

        nextStateUnique->threadJoinerUnique->join();
        nextStateUnique->threadJoinerBackgroundUnique->join();

        callExitEvent( *nextStateUnique );

        auto    prevDataDestroyProcPtr = nextStateUnique->dataDestroyProcPtr;

        {
            auto    lock = std::unique_lock< std::mutex >( state.disabledMutex );

            nextStateUnique.destroy();

            setEnabled(
                state
                , lock
            );
        }

        callReenterEvent(
            state
            , prevDataDestroyProcPtr
        );
    }

    void executeEvent(
        FgState &                                   _state
        , brownsugar::StateEvent::Proc              _eventProcPtr
        , fg::StateJoiner &                         _eventProcJoiner
        , FgStateJoiner &                           _executeJoiner
        , void *                                    _eventDataPtr
        , const std::unique_lock< std::mutex > &
    )
    {
        auto    eventProcEnder = _eventProcJoiner->start();

        auto    executeEnder = _executeJoiner.start();

        auto    dataUnique = ExecuteEventData::create(
            _state
            , _eventProcPtr
            , _eventDataPtr
            , std::move( eventProcEnder )
            , std::move( executeEnder )
        );

        _state.threadCache.execute(
            executeEventProc
            , *( dataUnique.release() )
            , *( _state.threadJoinerUnique )
        );
    }
}

void FgState::DestroyData::operator()(
    FgState *   _statePtr
) const
{
    _statePtr->dataDestroyProcPtr( _statePtr->dataPtr );
}

FgState::ThreadSingletonData::ThreadSingletonData(
    FgState &                       _state
    , FgStateThreadSingletonProc    _threadProcPtr
    , FgStateJoiner::Ender &&       _ender
    , void *                        _userDataPtr
)
    : state( _state )
    , threadProcPtr( _threadProcPtr )
    , ender( std::move( _ender ) )
    , userDataPtr( _userDataPtr )
    , reexecute( false )
{
}

bool FgState::LessThreadSingletonData::operator()(
    const FgState::ThreadSingletonData &    _DATA1
    , const FgState::ThreadSingletonData &  _DATA2
) const
{
    const auto &    THREAD_PROC_PTR1 = _DATA1.threadProcPtr;
    const auto &    THREAD_PROC_PTR2 = _DATA2.threadProcPtr;
    if( THREAD_PROC_PTR1 < THREAD_PROC_PTR2 ) {
        return true;
    } else if( THREAD_PROC_PTR1 > THREAD_PROC_PTR2 ) {
        return false;
    }

    const auto  JOINER_PTR1 = _DATA1.ender.get();
    const auto  JOINER_PTR2 = _DATA2.ender.get();
    if( JOINER_PTR1 < JOINER_PTR2 ) {
        return true;
    } else if( JOINER_PTR1 > JOINER_PTR2 ) {
        return false;
    }

    const auto &    USER_DATA_PTR1 = _DATA1.userDataPtr;
    const auto &    USER_DATA_PTR2 = _DATA2.userDataPtr;
    if( USER_DATA_PTR1 < USER_DATA_PTR2 ) {
        return true;
    } else if( USER_DATA_PTR1 > USER_DATA_PTR2 ) {
        return false;
    }

    return false;
}

void FgState::EndState::operator()(
    FgState *   _statePtr
) const
{
    auto    lock = std::unique_lock< std::mutex >( _statePtr->disabledMutex );

    _statePtr->disabled = true;
}

FgState::FgState(
    brownsugar::ThreadCache &           _threadCache
    , FgState *                         _prevStatePtr
    , bool                              _disabled
    , void *                            _dataPtr
    , FgCreatingStateDataDestroyProc    _dataDestroyProcPtr
)
    : threadCache( _threadCache )
    , prevStatePtr( _prevStatePtr )
    , disabled( _disabled )
    , dataPtr( _dataPtr )
    , dataDestroyProcPtr( _dataDestroyProcPtr )
    , dataDestroyer(
        _dataDestroyProcPtr != nullptr
            ? this
            : nullptr
    )
    , existsEnterEvent( false )
    , existsExitEvent( false )
    , existsReenterEvent( false )
    , existsLeaveEvent( false )
    , threadJoinerBackgroundUnique( brownsugar::ThreadJoiner::create() )
    , threadJoinerUnique( brownsugar::ThreadJoiner::create() )
    , ender( this )
    , nextStateUnique( nullptr )
{
}

FgState * FgState::create_(
    brownsugar::ThreadCache &           _threadCache
    , FgState *                         _prevStatePtr
    , bool                              _disabled
    , void *                            _dataPtr
    , FgCreatingStateDataDestroyProc    _dataDestroyProcPtr
)
{
    return new FgState(
        _threadCache
        , _prevStatePtr
        , _disabled
        , _dataPtr
        , _dataDestroyProcPtr
    );
}

void FgState::destroy(
    FgState *   _this
)
{
    delete _this;
}

void FgState::setData(
    void *                              _dataPtr
    , FgCreatingStateDataDestroyProc    _dataDestroyProcPtr
)
{
    this->dataDestroyer.reset( this );
    this->dataPtr = _dataPtr;
    this->dataDestroyProcPtr = _dataDestroyProcPtr;
}

void FgState::setEnterEventProc(
    brownsugar::StateEvent::Proc    _procPtr
)
{
    this->existsEnterEvent = true;
    this->enterEventProcPtr = _procPtr;
}

void FgState::setExitEventProc(
    brownsugar::StateEvent::Proc    _procPtr
)
{
    this->existsExitEvent = true;
    this->exitEventProcPtr = _procPtr;
}

void FgState::setReenterEventProc(
    brownsugar::StateEvent::Proc    _procPtr
)
{
    this->existsReenterEvent = true;
    this->reenterEventProcPtr = _procPtr;
}

void FgState::setLeaveEventProc(
    brownsugar::StateEvent::Proc    _procPtr
)
{
    this->existsLeaveEvent = true;
    this->leaveEventProcPtr = _procPtr;
}

FgState & FgState::getNextState(
)
{
    return *( this->nextStateUnique );
}

void FgState::setNextState(
    FgState::Unique &&  _stateUnique
)
{
    this->nextStateUnique = std::move( _stateUnique );
}

FgState & FgState::getRootState(
)
{
    auto    statePtr = this;
    while( statePtr->prevStatePtr != nullptr ) {
        statePtr = statePtr->prevStatePtr;
    }

    return *statePtr;
}

void FgState::executeEnterThread(
)
{
    this->threadCache.execute(
        enterThread
        , *this
        , *( this->threadJoinerUnique )
    );
}

void FgState::executeEvent(
    brownsugar::StateEvent::Proc    _eventProcPtr
    , fg::StateJoiner &             _eventProcJoiner
    , FgStateJoiner &               _executeJoiner
    , void *                        _eventDataPtr
)
{
    auto &  state = *this;

    auto    lock = std::unique_lock< std::mutex >( state.disabledMutex );

    if( state.disabled == true ) {
        return;
    }

    ::executeEvent(
        state
        , _eventProcPtr
        , _eventProcJoiner
        , _executeJoiner
        , _eventDataPtr
        , lock
    );
}

void FgState::executeEventBackground(
    brownsugar::StateEvent::Proc    _eventProcPtr
    , fg::StateJoiner &             _eventProcJoiner
    , FgStateJoiner &               _executeJoiner
    , void *                        _eventDataPtr
)
{
    auto &  state = *this;

    auto    lock = std::unique_lock< std::mutex >( state.disabledMutex );

    if( state.nextStateUnique.get() == nullptr ) {
        if( state.disabled == true ) {
            return;
        }
    }

    ::executeEvent(
        state
        , _eventProcPtr
        , _eventProcJoiner
        , _executeJoiner
        , _eventDataPtr
        , lock
    );
}

void * fgStateGetData(
    FgState *   _implPtr
)
{
    return _implPtr->dataPtr;
}

void fgStateExecute(
    FgState *           _implPtr
    , FgStateThreadProc _procPtr
)
{
    fgStateExecuteWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , nullptr
        , nullptr
    );
}

void fgStateExecuteWithJoiner(
    FgState *           _implPtr
    , FgStateThreadProc _procPtr
    , FgStateJoiner *   _joinerPtr
)
{
    fgStateExecuteWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , _joinerPtr
        , nullptr
    );
}

void fgStateExecuteWithJoinerAndUserData(
    FgState *           _implPtr
    , FgStateThreadProc _procPtr
    , FgStateJoiner *   _joinerPtr
    , void *            _userDataPtr
)
{
    auto &  impl = *_implPtr;

    auto    lock = std::unique_lock< std::mutex >( impl.disabledMutex );

    if( impl.disabled == true ) {
        return;
    }

    auto    dataUnique = ThreadData::create(
        impl
        , _procPtr
        , _joinerPtr != nullptr
            ? _joinerPtr->start()
            : nullptr
        , _userDataPtr
    );

    _implPtr->threadCache.execute(
        threadProc
        , *( dataUnique.release() )
        , *( impl.threadJoinerUnique )
    );
}

void fgStateExecuteSingleton(
    FgState *                       _implPtr
    , FgStateThreadSingletonProc    _procPtr
)
{
    fgStateExecuteSingletonWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , nullptr
        , nullptr
    );
}

void fgStateExecuteSingletonWithJoiner(
    FgState *                       _implPtr
    , FgStateThreadSingletonProc    _procPtr
    , FgStateJoiner *               _joinerPtr
)
{
    fgStateExecuteSingletonWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , _joinerPtr
        , nullptr
    );
}

void fgStateExecuteSingletonWithJoinerAndUserData(
    FgState *                       _implPtr
    , FgStateThreadSingletonProc    _procPtr
    , FgStateJoiner *               _joinerPtr
    , void *                        _userDataPtr
)
{
    auto &  impl = *_implPtr;

    auto    lock = std::unique_lock< std::mutex >( impl.disabledMutex );

    if( impl.disabled == true ) {
        return;
    }

    auto &  data = getThreadData(
        impl
        , _procPtr
        , _joinerPtr
        , _userDataPtr
    );

    _implPtr->threadCache.execute(
        threadSingletonProc
        , data
        , *( impl.threadJoinerUnique )
    );
}

void fgStateExecuteBackground(
    FgState *                       _implPtr
    , FgStateThreadBackgroundProc   _procPtr
)
{
    fgStateExecuteBackgroundWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , nullptr
        , nullptr
    );
}

void fgStateExecuteBackgroundWithJoiner(
    FgState *                       _implPtr
    , FgStateThreadBackgroundProc   _procPtr
    , FgStateJoiner *               _joinerPtr
)
{
    fgStateExecuteBackgroundWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , _joinerPtr
        , nullptr
    );
}

void fgStateExecuteBackgroundWithJoinerAndUserData(
    FgState *                       _implPtr
    , FgStateThreadBackgroundProc   _procPtr
    , FgStateJoiner *               _joinerPtr
    , void *                        _userDataPtr
)
{
    auto &  impl = *_implPtr;

    auto    lock = std::unique_lock< std::mutex >( impl.disabledMutex );

    if( impl.nextStateUnique.get() == nullptr ) {
        if( impl.disabled == true ) {
            return;
        }
    }

    auto    dataUnique = ThreadData::create(
        impl
        , reinterpret_cast< FgStateThreadProc >( _procPtr )
        , _joinerPtr != nullptr
            ? _joinerPtr->start()
            : nullptr
        , _userDataPtr
    );

    _implPtr->threadCache.execute(
        threadProc
        , *( dataUnique.release() )
        , *( impl.threadJoinerBackgroundUnique )
    );
}

void fgStateExecuteBackgroundSingleton(
    FgState *                               _implPtr
    , FgStateThreadBackgroundSingletonProc  _procPtr
)
{
    fgStateExecuteBackgroundSingletonWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , nullptr
        , nullptr
    );
}

void fgStateExecuteBackgroundSingletonWithJoiner(
    FgState *                               _implPtr
    , FgStateThreadBackgroundSingletonProc  _procPtr
    , FgStateJoiner *                       _joinerPtr
)
{
    fgStateExecuteBackgroundSingletonWithJoinerAndUserData(
        _implPtr
        , _procPtr
        , _joinerPtr
        , nullptr
    );
}

void fgStateExecuteBackgroundSingletonWithJoinerAndUserData(
    FgState *                               _implPtr
    , FgStateThreadBackgroundSingletonProc  _procPtr
    , FgStateJoiner *                       _joinerPtr
    , void *                                _userDataPtr
)
{
    auto &  impl = *_implPtr;

    auto    lock = std::unique_lock< std::mutex >( impl.disabledMutex );

    if( impl.nextStateUnique.get() == nullptr ) {
        if( impl.disabled == true ) {
            return;
        }
    }

    auto &  data = getThreadData(
        impl
        , reinterpret_cast< FgStateThreadSingletonProc >( _procPtr )
        , _joinerPtr
        , _userDataPtr
    );

    _implPtr->threadCache.execute(
        threadSingletonProc
        , data
        , *( impl.threadJoinerBackgroundUnique )
    );
}

void fgStateEnter(
    FgState *           _implPtr
    , FgCreatingState * _nextStatePtr
)
{
    auto &  impl = *_implPtr;

    auto    nextStateUnique = FgState::Unique( reinterpret_cast< FgState * >( _nextStatePtr ) );
    auto &  nextState = *nextStateUnique;

    {
        auto    lock = std::unique_lock< std::mutex >( impl.disabledMutex );

        if( setDisabled(
            impl
            , lock
        ) == false ) {
            return;
        }

        impl.setNextState( std::move( nextStateUnique ) );
    }

    nextState.threadCache.execute(
        enterThread
        , nextState
        , *( nextState.threadJoinerUnique )
    );
}

void fgStateExit(
    FgState *   _implPtr
)
{
    auto &  impl = *_implPtr;

    auto &  prevState = *( impl.prevStatePtr );

    {
        auto    lock = std::unique_lock< std::mutex >( impl.disabledMutex );

        if( setDisabled(
            impl
            , lock
        ) == false ) {
            return;
        }
    }

    prevState.threadCache.execute(
        exitNextThread
        , prevState
        , *( prevState.threadJoinerUnique )
    );
}

void * fgStateGetBasesystem(
    FgState *   _implPtr
)
{
    return fgStateGetData( &( _implPtr->getRootState().getNextState() ) );
}
