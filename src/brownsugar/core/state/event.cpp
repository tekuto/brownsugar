﻿#include "fg/util/export.h"
#include "fg/core/state/event.h"
#include "brownsugar/core/state/eventimpl.h"

FgState * fgStateEventGetState(
    FgStateEvent *  _implPtr
)
{
    return reinterpret_cast< brownsugar::StateEvent * >( _implPtr )->getState();
}

void * fgStateEventGetData(
    FgStateEvent *  _implPtr
)
{
    return reinterpret_cast< brownsugar::StateEvent * >( _implPtr )->getData();
}
