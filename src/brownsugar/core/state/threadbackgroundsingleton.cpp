﻿#include "fg/util/export.h"
#include "fg/core/state/threadbackgroundsingleton.h"
#include "brownsugar/core/state/threadimpl.h"

FgState * fgStateThreadBackgroundSingletonGetState(
    FgStateThreadBackgroundSingleton *  _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadBackgroundSingletonGetData(
    FgStateThreadBackgroundSingleton *  _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getData();
}
