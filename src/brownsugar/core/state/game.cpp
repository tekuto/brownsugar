﻿#include "fg/util/export.h"
#include "brownsugar/core/state/game.h"
#include "brownsugar/core/state/state.h"

FgGameState::FgGameState(
    FgState &           _state
    , FgModuleContext & _moduleContext
)
    : state( _state )
    , moduleContext( _moduleContext )
{
}

FgGameState * FgGameState::create_(
    FgState &           _state
    , FgModuleContext & _moduleContext
)
{
    return new FgGameState(
        _state
        , _moduleContext
    );
}

void FgGameState::destroy(
    FgGameState *   _this
)
{
    delete _this;
}

void fgGameStateSetData(
    FgGameState *                   _implPtr
    , void *                        _dataPtr
    , FgGameStateDataDestroyProc    _dataDestroyProcPtr
)
{
    _implPtr->state.setData(
        _dataPtr
        , _dataDestroyProcPtr
    );
}

FgCreatingState * fgGameStateGetCreatingState(
    FgGameState *   _implPtr
)
{
    return reinterpret_cast< FgCreatingState * >( &( _implPtr->state ) );
}

FgModuleContext * fgGameStateGetModuleContext(
    FgGameState *   _implPtr
)
{
    return &( _implPtr->moduleContext );
}
