﻿#include "fg/util/test.h"
#include "fg/core/state/threadbackgroundsingleton.h"
#include "fg/core/state/state.h"
#include "brownsugar/core/state/threadimpl.h"

struct StateThreadBackgroundSingletonTestData;

TEST(
    StateThreadBackgroundSingletonTest
    , GetState
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThreadBackgroundSingleton< StateThreadBackgroundSingletonTestData, int > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadBackgroundSingletonTestData > * >( 20 ), &( thread.getState() ) );
}

TEST(
    StateThreadBackgroundSingletonTest
    , GetState_withoutUserData
)
{
    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , nullptr
    );
    auto &  thread = reinterpret_cast< fg::StateThreadBackgroundSingleton< StateThreadBackgroundSingletonTestData > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadBackgroundSingletonTestData > * >( 10 ), &( thread.getState() ) );
}

TEST(
    StateThreadBackgroundSingletonTest
    , GetData
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThreadBackgroundSingleton< StateThreadBackgroundSingletonTestData, int > & >( threadImpl );

    ASSERT_EQ( 10, thread.getData() );
}
