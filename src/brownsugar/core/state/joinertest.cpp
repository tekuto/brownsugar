﻿#include "fg/util/test.h"
#include "brownsugar/core/state/joiner.h"

#include <memory>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>

struct JoinThread
{
    void operator()(
        std::thread *   _threadPtr
    )
    {
        _threadPtr->join();
    }
};

using ThreadJoiner = std::unique_ptr<
    std::thread
    , JoinThread
>;

TEST(
    StateJoinerTest
    , Create
)
{
    const auto  JOINER_UNIQUE = fg::StateJoiner::create();
    ASSERT_NE( nullptr, JOINER_UNIQUE.get() );
    const auto &    JOINER = *JOINER_UNIQUE;

    ASSERT_EQ( 0, JOINER->runningCount );
    ASSERT_EQ( &*JOINER, JOINER->joiner.get() );
}

TEST(
    StateJoinerTest
    , Destroy
)
{
    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    ender = joiner->start();

    std::mutex              mutex;
    std::condition_variable cond;

    auto    i = 10;

    auto    thread = std::thread(
        [
            ender = std::move( ender )
            , &mutex
            , &cond
            , &i
        ]
        {
            auto    lock = std::unique_lock< std::mutex >( mutex );

            cond.wait_for(
                lock
                , std::chrono::milliseconds( 10 )
            );

            i = 20;
        }
    );
    auto    threadJoiner = ThreadJoiner( &thread );

    joinerUnique.destroy();

    ASSERT_EQ( 20, i );

    auto    lock = std::unique_lock< std::mutex >( mutex );

    cond.notify_one();
}

TEST(
    StateJoinerTest
    , Join
)
{
    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    ender = joiner->start();

    std::mutex              mutex;
    std::condition_variable cond;

    auto    i = 10;

    auto    thread = std::thread(
        [
            ender = std::move( ender )
            , &mutex
            , &cond
            , &i
        ]
        {
            auto    lock = std::unique_lock< std::mutex >( mutex );

            cond.wait_for(
                lock
                , std::chrono::milliseconds( 10 )
            );

            i = 20;
        }
    );
    auto    threadJoiner = ThreadJoiner( &thread );

    joinerUnique->join();

    ASSERT_EQ( 20, i );

    auto    lock = std::unique_lock< std::mutex >( mutex );

    cond.notify_one();
}

TEST(
    StateJoinerTest
    , Start
)
{
    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = **joinerUnique;

    auto    ender = joiner.start();

    ASSERT_EQ( &joiner, ender.get() );
    ASSERT_EQ( 1, joiner.runningCount );

    auto    ender2 = joiner.start();

    ASSERT_EQ( &joiner, ender2.get() );
    ASSERT_EQ( 2, joiner.runningCount );

    ender.reset();
    ender2.reset();

    joiner.runningCount = 0;
}

TEST(
    StateJoinerTest
    , Ender
)
{
    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = **joinerUnique;

    auto    ender = joiner.start();
    auto    ender2 = joiner.start();

    ender.reset();

    ASSERT_EQ( 1, joiner.runningCount );

    ender2.reset();

    ASSERT_EQ( 0, joiner.runningCount );
}
