﻿#include "fg/util/test.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/state/eventmanager.h"
#include "brownsugar/core/thread/cache.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/event.h"
#include "fg/core/state/eventbackground.h"
#include "fg/core/state/enterevent.h"
#include "fg/core/state/exitevent.h"
#include "fg/core/state/reenterevent.h"
#include "fg/core/state/leaveevent.h"
#include "fg/core/state/thread.h"
#include "fg/core/state/threadsingleton.h"
#include "fg/core/state/threadbackground.h"
#include "fg/core/state/threadbackgroundsingleton.h"
#include "fg/core/state/joiner.h"
#include "fg/core/state/event.h"
#include "fg/common/unique.h"

#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <utility>

void dummyDestroy(
    void *
)
{
}

struct StateTestData : public fg::UniqueWrapper< StateTestData >
{
    static Unique create(
    )
    {
        return new StateTestData;
    }
};

void stateTestProc(
    void *  _dataPtr
)
{
    *static_cast< int * >( _dataPtr ) = 30;
}

TEST(
    StateTest
    , Create
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    {
        const auto  STATE_UNIQUE = FgState::create(
            threadCache
            , reinterpret_cast< FgState * >( 20 )
            , true
            , &i
            , stateTestProc
        );
        ASSERT_NE( nullptr, STATE_UNIQUE.get() );
        const auto &    STATE = *STATE_UNIQUE;

        ASSERT_EQ( &threadCache, &( STATE.threadCache ) );
        ASSERT_EQ( reinterpret_cast< FgState * >( 20 ), STATE.prevStatePtr );
        ASSERT_TRUE( STATE.disabled );
        ASSERT_EQ( 10, *static_cast< const int * >( STATE.dataPtr ) );
        ASSERT_EQ( stateTestProc, STATE.dataDestroyProcPtr );
        ASSERT_EQ( &STATE, STATE.dataDestroyer.get() );
        ASSERT_FALSE( STATE.existsEnterEvent );
        ASSERT_FALSE( STATE.existsExitEvent );
        ASSERT_FALSE( STATE.existsReenterEvent );
        ASSERT_FALSE( STATE.existsLeaveEvent );
        ASSERT_EQ( 0, STATE.threadSingletonDataSet.size() );
        ASSERT_NE( nullptr, STATE.threadJoinerBackgroundUnique.get() );
        ASSERT_NE( nullptr, STATE.threadJoinerUnique.get() );
        ASSERT_EQ( &STATE, STATE.ender.get() );
        ASSERT_EQ( nullptr, STATE.nextStateUnique.get() );
    }

    ASSERT_EQ( 30, i );
}

void setDataTestProc(
    void *  _dataPtr
)
{
    *static_cast< int * >( _dataPtr ) = 50;
}

TEST(
    StateTest
    , SetData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;
    auto    j = 40;

    {
        auto    stateUnique = FgState::create(
            threadCache
            , reinterpret_cast< FgState * >( 20 )
            , true
            , &i
            , stateTestProc
        );
        ASSERT_NE( nullptr, stateUnique.get() );
        auto &  state = *stateUnique;

        state.setData(
            &j
            , setDataTestProc
        );
        ASSERT_EQ( 30, i );
    }

    ASSERT_EQ( 50, j );
}

void setEventProcTestProc(
    brownsugar::StateEvent &
)
{
}

TEST(
    StateTest
    , SetEnterEventProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , reinterpret_cast< FgState * >( 20 )
        , true
        , &i
        , stateTestProc
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    state.setEnterEventProc( setEventProcTestProc );

    ASSERT_TRUE( state.existsEnterEvent );
    ASSERT_EQ( setEventProcTestProc, state.enterEventProcPtr );
}

TEST(
    StateTest
    , SetExitEventProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , reinterpret_cast< FgState * >( 20 )
        , true
        , &i
        , stateTestProc
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    state.setExitEventProc( setEventProcTestProc );

    ASSERT_TRUE( state.existsExitEvent );
    ASSERT_EQ( setEventProcTestProc, state.exitEventProcPtr );
}

TEST(
    StateTest
    , SetReenterEventProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , reinterpret_cast< FgState * >( 20 )
        , true
        , &i
        , stateTestProc
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    state.setReenterEventProc( setEventProcTestProc );

    ASSERT_TRUE( state.existsReenterEvent );
    ASSERT_EQ( setEventProcTestProc, state.reenterEventProcPtr );
}

TEST(
    StateTest
    , SetLeaveEventProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , reinterpret_cast< FgState * >( 20 )
        , true
        , &i
        , stateTestProc
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    state.setLeaveEventProc( setEventProcTestProc );

    ASSERT_TRUE( state.existsLeaveEvent );
    ASSERT_EQ( setEventProcTestProc, state.leaveEventProcPtr );
}

TEST(
    StateTest
    , GetNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    nextStateUnique = FgState::create(
        threadCache
        , &state
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );
    const auto &    NEXT_STATE = *nextStateUnique;

    state.setNextState( std::move( nextStateUnique ) );

    ASSERT_EQ( &NEXT_STATE, &( state.getNextState() ) );
}

TEST(
    StateTest
    , SetNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    nextStateUnique = FgState::create(
        threadCache
        , &state
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );
    const auto &    NEXT_STATE = *nextStateUnique;

    state.setNextState( std::move( nextStateUnique ) );

    ASSERT_EQ( &NEXT_STATE, state.nextStateUnique.get() );
}

TEST(
    StateTest
    , GetRootState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = *rootStateUnique;

    auto    nextStateUnique = FgState::create(
        threadCache
        , &rootState
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = *nextStateUnique;

    rootState.setNextState( std::move( nextStateUnique ) );

    auto    stateUnique = FgState::create(
        threadCache
        , &nextState
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    nextState.setNextState( std::move( stateUnique ) );

    ASSERT_EQ( &rootState, &( state.getRootState() ) );
}

void executeEnterThreadTestProc(
    fg::StateEnterEvent< int > &    _event
)
{
    _event.getState().getData() = 20;
}

TEST(
    StateTest
    , ExecuteEnterThread
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = *prevStateUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , &prevState
        , true
        , &i
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    state.setEnterEventProc( reinterpret_cast< brownsugar::StateEvent::Proc >( executeEnterThreadTestProc ) );

    state.executeEnterThread();

    state.threadJoinerUnique->join();

    ASSERT_FALSE( state.disabled );
    ASSERT_EQ( 20, i );
}

struct ExecuteTestData : public fg::UniqueWrapper< ExecuteTestData >
{
    int &   i;

    ExecuteTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new ExecuteTestData( _i );
    }
};

void executeEventTestProc(
    fg::StateEvent< ExecuteTestData, int > &    _event
)
{
    _event.getState().getData().i += _event.getData();
}

TEST(
    StateTest
    , ExecuteEvent
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , ExecuteTestData::create( i ).release()
        , reinterpret_cast< FgCreatingStateDataDestroyProc >( ExecuteTestData::destroy )
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    state->threadJoinerUnique->join();

    auto    eventProcJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, eventProcJoinerUnique.get() );
    auto &  eventProcJoiner = *eventProcJoinerUnique;

    auto    executeJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, executeJoinerUnique.get() );
    auto &  executeJoiner = **executeJoinerUnique;

    auto    j = 20;

    stateUnique->executeEvent(
        reinterpret_cast< brownsugar::StateEvent::Proc >( executeEventTestProc )
        , eventProcJoiner
        , executeJoiner
        , &j
    );

    executeJoinerUnique->join();

    ASSERT_EQ( 30, i );
}

TEST(
    StateTest
    , ExecuteEvent_failedWhenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state->nextStateUnique->threadJoinerUnique->join();

    auto    eventProcJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, eventProcJoinerUnique.get() );
    auto &  eventProcJoiner = *eventProcJoinerUnique;

    auto    executeJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, executeJoinerUnique.get() );
    auto &  executeJoiner = **executeJoinerUnique;

    auto    j = 20;

    stateUnique->executeEvent(
        reinterpret_cast< brownsugar::StateEvent::Proc >( executeEventTestProc )
        , eventProcJoiner
        , executeJoiner
        , &j
    );

    executeJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

struct ExecuteEventFailedWhenExitedTestData : public fg::UniqueWrapper< ExecuteEventFailedWhenExitedTestData >
{
    std::mutex &                mutex;
    std::condition_variable &   cond;

    bool &  ended;

    int &   i;

    ExecuteEventFailedWhenExitedTestData(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
        , int &                     _i
    )
        : mutex( _mutex )
        , cond( _cond )
        , ended( _ended )
        , i( _i )
    {
    }

    static Unique create(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
        , int &                     _i
    )
    {
        return new ExecuteEventFailedWhenExitedTestData(
            _mutex
            , _cond
            , _ended
            , _i
        );
    }
};

void executeEventFailedWhenExitedTestThread(
    fg::StateThread< ExecuteEventFailedWhenExitedTestData > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }
}

void executeEventFailedWhenExitedTestProc(
    fg::StateEvent< ExecuteEventFailedWhenExitedTestData, int > &   _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteEvent_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    auto    i = 10;

    prevState.enter(
        [
            &mutex
            , &cond
            , &ended
            , &i
        ]
        (
            fg::CreatingState< ExecuteEventFailedWhenExitedTestData > &
        )
        {
            return ExecuteEventFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
                , i
            ).release();
        }
        , ExecuteEventFailedWhenExitedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteEventFailedWhenExitedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeEventFailedWhenExitedTestThread );

    state.exit();

    auto    eventProcJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, eventProcJoinerUnique.get() );
    auto &  eventProcJoiner = *eventProcJoinerUnique;

    auto    executeJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, executeJoinerUnique.get() );
    auto &  executeJoiner = **executeJoinerUnique;

    auto    j = 20;

    stateUnique->executeEvent(
        reinterpret_cast< brownsugar::StateEvent::Proc >( executeEventFailedWhenExitedTestProc )
        , eventProcJoiner
        , executeJoiner
        , &j
    );

    executeJoinerUnique->join();

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

void executeEventBackgroundTestProc(
    fg::StateEventBackground< ExecuteTestData, int > &  _event
)
{
    _event.getState().getData().i += _event.getData();
}

TEST(
    StateTest
    , ExecuteEventBackground
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , ExecuteTestData::create( i ).release()
        , reinterpret_cast< FgCreatingStateDataDestroyProc >( ExecuteTestData::destroy )
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    state->threadJoinerUnique->join();

    auto    eventProcJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, eventProcJoinerUnique.get() );
    auto &  eventProcJoiner = *eventProcJoinerUnique;

    auto    executeJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, executeJoinerUnique.get() );
    auto &  executeJoiner = **executeJoinerUnique;

    auto    j = 20;

    stateUnique->executeEventBackground(
        reinterpret_cast< brownsugar::StateEvent::Proc >( executeEventBackgroundTestProc )
        , eventProcJoiner
        , executeJoiner
        , &j
    );

    executeJoinerUnique->join();

    ASSERT_EQ( 30, i );
}

TEST(
    StateTest
    , ExecuteEventBackground_whenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state->nextStateUnique->threadJoinerUnique->join();

    auto    eventProcJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, eventProcJoinerUnique.get() );
    auto &  eventProcJoiner = *eventProcJoinerUnique;

    auto    executeJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, executeJoinerUnique.get() );
    auto &  executeJoiner = **executeJoinerUnique;

    auto    j = 20;

    stateUnique->executeEventBackground(
        reinterpret_cast< brownsugar::StateEvent::Proc >( executeEventBackgroundTestProc )
        , eventProcJoiner
        , executeJoiner
        , &j
    );

    executeJoinerUnique->join();

    ASSERT_EQ( 30, i );
}

void executeEventBackgroundFailedWhenExitedTestProc(
    fg::StateEventBackground< ExecuteEventFailedWhenExitedTestData, int > & _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteEventBackground_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    auto    i = 10;

    prevState.enter(
        [
            &mutex
            , &cond
            , &ended
            , &i
        ]
        (
            fg::CreatingState< ExecuteEventFailedWhenExitedTestData > &
        )
        {
            return ExecuteEventFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
                , i
            ).release();
        }
        , ExecuteEventFailedWhenExitedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteEventFailedWhenExitedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeEventFailedWhenExitedTestThread );

    state.exit();

    auto    eventProcJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, eventProcJoinerUnique.get() );
    auto &  eventProcJoiner = *eventProcJoinerUnique;

    auto    executeJoinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, executeJoinerUnique.get() );
    auto &  executeJoiner = **executeJoinerUnique;

    auto    j = 20;

    stateUnique->executeEventBackground(
        reinterpret_cast< brownsugar::StateEvent::Proc >( executeEventBackgroundFailedWhenExitedTestProc )
        , eventProcJoiner
        , executeJoiner
        , &j
    );

    executeJoinerUnique->join();

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

TEST(
    StateTest
    , GetData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    i = 10;

    auto    stateUnique = FgState::create(
        threadCache
        , reinterpret_cast< FgState * >( 20 )
        , true
        , &i
        , stateTestProc
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< int > & >( *stateUnique );

    ASSERT_EQ( 10, state.getData() );
}

void executeTestProc(
    fg::StateThread< ExecuteTestData > &    _thread
)
{
    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , Execute
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeTestProc );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 20, i );
}

auto    executeWithoutStateDataTestData = 10;

void executeWithoutStateDataTestProc(
    fg::StateThread<> &
)
{
    executeWithoutStateDataTestData = 20;
}

TEST(
    StateTest
    , Execute_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeWithoutStateDataTestProc );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 20, executeWithoutStateDataTestData );
}

void executeWithJoinerTestProc(
    fg::StateThread< ExecuteTestData > &    _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteWithJoiner
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeWithJoinerTestProc
        , joiner
    );

    joiner.join();

    ASSERT_EQ( 20, i );
}

auto    executeWithJoinerWithoutStateDataTestData = 10;

void executeWithJoinerWithoutStateDataTestProc(
    fg::StateThread<> &
)
{
    executeWithJoinerWithoutStateDataTestData = 20;
}

TEST(
    StateTest
    , ExecuteWithJoiner_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeWithJoinerWithoutStateDataTestProc
        , joiner
    );

    joiner.join();

    ASSERT_EQ( 20, executeWithJoinerWithoutStateDataTestData );
}

void executeWithJoinerAndUserDataTestProc(
    fg::StateThread< ExecuteTestData, int > &   _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getState().getData().i = _thread.getData();
}

TEST(
    StateTest
    , ExecuteWithJoinerAndUserData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    state.execute(
        executeWithJoinerAndUserDataTestProc
        , joiner
        , j
    );

    joiner.join();

    ASSERT_EQ( 20, i );
}

void executeWithJoinerAndUserDataWithoutStateDataTestProc(
    fg::StateThread< void, int > &  _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getData() = 20;
}

TEST(
    StateTest
    , ExecuteWithJoinerAndUserData_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    i = 10;

    state.execute(
        executeWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );

    joiner.join();

    ASSERT_EQ( 20, i );
}

struct ExecuteJoinedTestData : public fg::UniqueWrapper< ExecuteJoinedTestData >
{
    std::mutex  mutex;

    int &   i;
    int &   j;

    ExecuteJoinedTestData(
        int &   _i
        , int & _j
    )
        : i( _i )
        , j( _j )
    {
    }

    static Unique create(
        int &   _i
        , int & _j
    )
    {
        return new ExecuteJoinedTestData(
            _i
            , _j
        );
    }
};

void executeJoinedTestProc(
    fg::StateThread< ExecuteJoinedTestData > &  _thread
)
{
    auto &  data = _thread.getState().getData();

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.i = 30;
}

void executeJoinedWhenEnteredTestLeaveEventProc(
    fg::StateLeaveEvent< ExecuteJoinedTestData > &  _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.j = data.i;
}

TEST(
    StateTest
    , Execute_joinedWhenEntered
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenEnteredTestLeaveEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeJoinedTestProc );

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state->nextStateUnique->threadJoinerUnique->join();

    ASSERT_EQ( 30, j );
}

void executeJoinedWhenExitedTestExitEventProc(
    fg::StateExitEvent< ExecuteJoinedTestData > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.j = data.i;
}

TEST(
    StateTest
    , Execute_joinedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenExitedTestExitEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeJoinedTestProc );

    state.exit();

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 30, j );
}

TEST(
    StateTest
    , Execute_failedWhenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state.execute( executeTestProc );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

struct ExecuteFailedWhenExitedTestData : public fg::UniqueWrapper< ExecuteFailedWhenExitedTestData >
{
    std::mutex &                mutex;
    std::condition_variable &   cond;

    bool &  ended;

    int &   i;

    ExecuteFailedWhenExitedTestData(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
        , int &                     _i
    )
        : mutex( _mutex )
        , cond( _cond )
        , ended( _ended )
        , i( _i )
    {
    }

    static Unique create(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
        , int &                     _i
    )
    {
        return new ExecuteFailedWhenExitedTestData(
            _mutex
            , _cond
            , _ended
            , _i
        );
    }
};

void executeFailedWhenExitedTestThread(
    fg::StateThread< ExecuteFailedWhenExitedTestData > &    _thread
)
{
    auto &  data = _thread.getState().getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }
}

void executeFailedWhenExitedTestThread2(
    fg::StateThread< ExecuteFailedWhenExitedTestData > &    _thread
)
{
    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , Execute_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    auto    i = 10;

    prevState.enter(
        [
            &mutex
            , &cond
            , &ended
            , &i
        ]
        (
            fg::CreatingState< ExecuteFailedWhenExitedTestData > &
        )
        {
            return ExecuteFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
                , i
            ).release();
        }
        , ExecuteFailedWhenExitedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteFailedWhenExitedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeFailedWhenExitedTestThread );

    state.exit();

    state.execute( executeFailedWhenExitedTestThread2 );

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

struct ExecuteSingletonTestData : public fg::UniqueWrapper< ExecuteSingletonTestData >
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    int &   i;

    ExecuteSingletonTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new ExecuteSingletonTestData( _i );
    }
};

void executeSingletonTestProc(
    fg::StateThreadSingleton< ExecuteSingletonTestData > &  _thread
)
{
    auto &  data = _thread.getState().getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    data.i += 20;
}

TEST(
    StateTest
    , ExecuteSingleton
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeSingletonTestProc );
    state.execute( executeSingletonTestProc );
    state.execute( executeSingletonTestProc );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    state->threadJoinerUnique->join();

    ASSERT_EQ( 50, i );

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

auto    executeSingletonWithoutStateDataTestData = 10;

void executeSingletonWithoutStateDataTestProc(
    fg::StateThreadSingleton<> &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    executeSingletonWithoutStateDataTestData += 20;
}

TEST(
    StateTest
    , ExecuteSingleton_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeSingletonWithoutStateDataTestProc );
    state.execute( executeSingletonWithoutStateDataTestProc );
    state.execute( executeSingletonWithoutStateDataTestProc );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 50, executeSingletonWithoutStateDataTestData );

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

TEST(
    StateTest
    , ExecuteSingletonWithJoiner
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeSingletonTestProc
        , joiner
    );
    state.execute(
        executeSingletonTestProc
        , joiner
    );
    state.execute(
        executeSingletonTestProc
        , joiner
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 50, i );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

auto    executeSingletonWithJoinerWithoutStateDataTestData = 10;

void executeSingletonWithJoinerWithoutStateDataTestProc(
    fg::StateThreadSingleton<> &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    executeSingletonWithJoinerWithoutStateDataTestData += 20;
}

TEST(
    StateTest
    , ExecuteSingletonWithJoiner_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeSingletonWithJoinerWithoutStateDataTestProc
        , joiner
    );
    state.execute(
        executeSingletonWithJoinerWithoutStateDataTestProc
        , joiner
    );
    state.execute(
        executeSingletonWithJoinerWithoutStateDataTestProc
        , joiner
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    joiner.join();

    ASSERT_EQ( 50, executeSingletonWithJoinerWithoutStateDataTestData );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeSingletonWithJoinerAndUserDataTestProc(
    fg::StateThreadSingleton< ExecuteSingletonTestData, int > &    _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += _thread.getData();
}

TEST(
    StateTest
    , ExecuteSingletonWithJoinerAndUserData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 50, i );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeSingletonWithJoinerAndUserDataWithoutStateDataTestProc(
    fg::StateThreadSingleton< void, int > & _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getData() += 20;
}

TEST(
    StateTest
    , ExecuteSingletonWithJoinerAndUserData_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    i = 10;

    state.execute(
        executeSingletonWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    joiner.join();

    ASSERT_EQ( 50, i );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeSingletonWithJoinerAndUserDataTestProc2(
    fg::StateThreadSingleton< ExecuteSingletonTestData, int > &    _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += _thread.getData() * 2;
}

void executeSingletonWithJoinerAndUserDataTestProc3(
    fg::StateThreadSingleton< ExecuteSingletonTestData, int > &    _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += _thread.getData() * 3;
}

TEST(
    StateTest
    , ExecuteSingletonWithJoinerAndUserData_diffProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc2
        , joiner
        , j
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc3
        , joiner
        , j
    );

    ASSERT_EQ( 3, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 130, i );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

TEST(
    StateTest
    , ExecuteSingletonWithJoinerAndUserData_diffJoiner
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joiner1Unique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joiner1Unique.get() );
    auto &  joiner1 = *joiner1Unique;

    auto    joiner2Unique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joiner2Unique.get() );
    auto &  joiner2 = *joiner2Unique;

    auto    joiner3Unique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joiner3Unique.get() );
    auto &  joiner3 = *joiner3Unique;

    auto    j = 20;

    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner1
        , j
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner2
        , j
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner3
        , j
    );

    ASSERT_EQ( 3, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner1.join();
    joiner2.join();
    joiner3.join();

    ASSERT_EQ( 70, i );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

TEST(
    StateTest
    , ExecuteSingletonWithJoinerAndUserData_diffUserData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;
    auto    k = 30;
    auto    l = 40;

    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner
        , k
    );
    state.execute(
        executeSingletonWithJoinerAndUserDataTestProc
        , joiner
        , l
    );

    ASSERT_EQ( 3, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 100, i );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeSingletonJoinedTestProc(
    fg::StateThreadSingleton< ExecuteJoinedTestData > & _thread
)
{
    auto &  data = _thread.getState().getData();

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.i = 30;
}

TEST(
    StateTest
    , ExecuteSingleton_joinedWhenEntered
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenEnteredTestLeaveEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeSingletonJoinedTestProc );

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state->nextStateUnique->threadJoinerUnique->join();

    ASSERT_EQ( 30, j );
}

TEST(
    StateTest
    , ExecuteSingleton_joinedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenExitedTestExitEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeSingletonJoinedTestProc );

    state.exit();

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 30, j );
}

void executeSingletonFailedWhenExistsNextStateTestProc(
    fg::StateThreadSingleton< ExecuteTestData > &   _thread
)
{
    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteSingleton_failedWhenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state.execute( executeSingletonFailedWhenExistsNextStateTestProc );

    state->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

void executeSingletonFailedWhenExitedTestThread(
    fg::StateThreadSingleton< ExecuteFailedWhenExitedTestData > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }
}

void executeSingletonFailedWhenExitedTestThread2(
    fg::StateThreadSingleton< ExecuteFailedWhenExitedTestData > &   _thread
)
{
    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteSingleton_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    auto    i = 10;

    prevState.enter(
        [
            &mutex
            , &cond
            , &ended
            , &i
        ]
        (
            fg::CreatingState< ExecuteFailedWhenExitedTestData > &
        )
        {
            return ExecuteFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
                , i
            ).release();
        }
        , ExecuteFailedWhenExitedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteFailedWhenExitedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeSingletonFailedWhenExitedTestThread );

    state.exit();

    state.execute( executeSingletonFailedWhenExitedTestThread2 );

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

void executeBackgroundTestProc(
    fg::StateThreadBackground< ExecuteTestData > &  _thread
)
{
    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteBackground
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundTestProc );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 20, i );
}

auto    executeBackgroundWithoutStateDataTestData = 10;

void executeBackgroundWithoutStateDataTestProc(
    fg::StateThreadBackground<> &
)
{
    executeBackgroundWithoutStateDataTestData = 20;
}

TEST(
    StateTest
    , ExecuteBackground_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundWithoutStateDataTestProc );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 20, executeBackgroundWithoutStateDataTestData );
}

void executeBackgroundWithJoinerTestProc(
    fg::StateThreadBackground< ExecuteTestData > &  _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteBackgroundWithJoiner
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeBackgroundWithJoinerTestProc
        , joiner
    );

    joiner.join();

    ASSERT_EQ( 20, i );
}

auto    executeBackgroundWithJoinerWithoutStateDataTestData = 10;

void executeBackgroundWithJoinerWithoutStateDataTestProc(
    fg::StateThreadBackground<> &
)
{
    executeBackgroundWithJoinerWithoutStateDataTestData = 20;
}

TEST(
    StateTest
    , ExecuteBackgroundWithJoiner_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeBackgroundWithJoinerWithoutStateDataTestProc
        , joiner
    );

    joiner.join();

    ASSERT_EQ( 20, executeBackgroundWithJoinerWithoutStateDataTestData );
}

void executeBackgroundWithJoinerAndUserDataTestProc(
    fg::StateThreadBackground< ExecuteTestData, int > & _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getState().getData().i = _thread.getData();
}

TEST(
    StateTest
    , ExecuteBackgroundWithJoinerAndUserData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    state.execute(
        executeBackgroundWithJoinerAndUserDataTestProc
        , joiner
        , j
    );

    joiner.join();

    ASSERT_EQ( 20, i );
}

void executeBackgroundWithJoinerAndUserDataWithoutStateDataTestProc(
    fg::StateThreadBackground< void, int > &    _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getData() = 20;
}

TEST(
    StateTest
    , ExecuteBackgroundWithJoinerAndUserData_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    i = 10;

    state.execute(
        executeBackgroundWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );

    joiner.join();

    ASSERT_EQ( 20, i );
}

void executeBackgroundJoinedTestProc(
    fg::StateThreadBackground< ExecuteJoinedTestData > &    _thread
)
{
    auto &  data = _thread.getState().getData();

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.i = 30;
}

TEST(
    StateTest
    , ExecuteBackground_notJoinedWhenEntered
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenEnteredTestLeaveEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundJoinedTestProc );

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state->nextStateUnique->threadJoinerUnique->join();

    ASSERT_EQ( 10, j );
}

TEST(
    StateTest
    , ExecuteBackground_joinedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenExitedTestExitEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundJoinedTestProc );

    state.exit();

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 30, j );
}

TEST(
    StateTest
    , ExecuteBackground_whenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteTestData > &
        )
        {
            return ExecuteTestData::create( i ).release();
        }
        , ExecuteTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state.execute( executeBackgroundTestProc );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 20, i );
}

void executeBackgroundFailedWhenExitedTestThread(
    fg::StateThreadBackground< ExecuteFailedWhenExitedTestData > &  _thread
)
{
    auto &  data = _thread.getState().getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }
}

void executeBackgroundFailedWhenExitedTestThread2(
    fg::StateThreadBackground< ExecuteFailedWhenExitedTestData > &  _thread
)
{
    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteBackground_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    auto    i = 10;

    prevState.enter(
        [
            &mutex
            , &cond
            , &ended
            , &i
        ]
        (
            fg::CreatingState< ExecuteFailedWhenExitedTestData > &
        )
        {
            return ExecuteFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
                , i
            ).release();
        }
        , ExecuteFailedWhenExitedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteFailedWhenExitedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundFailedWhenExitedTestThread );

    state.exit();

    state.execute( executeBackgroundFailedWhenExitedTestThread2 );

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

void executeBackgroundSingletonTestProc(
    fg::StateThreadBackgroundSingleton< ExecuteSingletonTestData > &    _thread
)
{
    auto &  data = _thread.getState().getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    data.i += 20;
}

TEST(
    StateTest
    , ExecuteBackgroundSingleton
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundSingletonTestProc );
    state.execute( executeBackgroundSingletonTestProc );
    state.execute( executeBackgroundSingletonTestProc );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 50, i );

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

auto    executeBackgroundSingletonWithoutStateDataTestData = 10;

void executeBackgroundSingletonWithoutStateDataTestProc(
    fg::StateThreadBackgroundSingleton<> &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    executeBackgroundSingletonWithoutStateDataTestData += 20;
}

TEST(
    StateTest
    , ExecuteBackgroundSingleton_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundSingletonWithoutStateDataTestProc );
    state.execute( executeBackgroundSingletonWithoutStateDataTestProc );
    state.execute( executeBackgroundSingletonWithoutStateDataTestProc );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 50, executeBackgroundSingletonWithoutStateDataTestData );

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

TEST(
    StateTest
    , ExecuteBackgroundSingletonWithJoiner
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeBackgroundSingletonTestProc
        , joiner
    );
    state.execute(
        executeBackgroundSingletonTestProc
        , joiner
    );
    state.execute(
        executeBackgroundSingletonTestProc
        , joiner
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 50, i );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

auto    executeBackgroundSingletonWithJoinerWithoutStateDataTestData = 10;

void executeBackgroundSingletonWithJoinerWithoutStateDataTestProc(
    fg::StateThreadBackgroundSingleton<> &
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    executeBackgroundSingletonWithJoinerWithoutStateDataTestData += 20;
}

TEST(
    StateTest
    , ExecuteBackgroundSingletonWithJoiner_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    state.execute(
        executeBackgroundSingletonWithJoinerWithoutStateDataTestProc
        , joiner
    );
    state.execute(
        executeBackgroundSingletonWithJoinerWithoutStateDataTestProc
        , joiner
    );
    state.execute(
        executeBackgroundSingletonWithJoinerWithoutStateDataTestProc
        , joiner
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    joiner.join();

    ASSERT_EQ( 50, executeBackgroundSingletonWithJoinerWithoutStateDataTestData );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeBackgroundSingletonWithJoinerAndUserDataTestProc(
    fg::StateThreadBackgroundSingleton< ExecuteSingletonTestData, int > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += _thread.getData();
}

TEST(
    StateTest
    , ExecuteBackgroundSingletonWithJoinerAndUserData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 50, i );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeBackgroundSingletonWithJoinerAndUserDataWithoutStateDataTestProc(
    fg::StateThreadBackgroundSingleton< void, int > &   _thread
)
{
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    _thread.getData() += 20;
}

TEST(
    StateTest
    , ExecuteBackgroundSingletonWithJoinerAndUserData_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    prevState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    i = 10;

    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataWithoutStateDataTestProc
        , joiner
        , i
    );

    ASSERT_EQ( 1, state->threadSingletonDataSet.size() );

    joiner.join();

    ASSERT_EQ( 50, i );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeBackgroundSingletonWithJoinerAndUserDataTestProc2(
    fg::StateThreadBackgroundSingleton< ExecuteSingletonTestData, int > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += _thread.getData() * 2;
}

void executeBackgroundSingletonWithJoinerAndUserDataTestProc3(
    fg::StateThreadBackgroundSingleton< ExecuteSingletonTestData, int > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }

    data.i += _thread.getData() * 3;
}

TEST(
    StateTest
    , ExecuteBackgroundSingletonWithJoinerAndUserData_diffProc
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;

    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc2
        , joiner
        , j
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc3
        , joiner
        , j
    );

    ASSERT_EQ( 3, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 130, i );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

TEST(
    StateTest
    , ExecuteBackgroundSingletonWithJoinerAndUserData_diffJoiner
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joiner1Unique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joiner1Unique.get() );
    auto &  joiner1 = *joiner1Unique;

    auto    joiner2Unique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joiner2Unique.get() );
    auto &  joiner2 = *joiner2Unique;

    auto    joiner3Unique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joiner3Unique.get() );
    auto &  joiner3 = *joiner3Unique;

    auto    j = 20;

    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner1
        , j
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner2
        , j
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner3
        , j
    );

    ASSERT_EQ( 3, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner1.join();
    joiner2.join();
    joiner3.join();

    ASSERT_EQ( 70, i );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

TEST(
    StateTest
    , ExecuteBackgroundSingletonWithJoinerAndUserData_diffUserData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    j = 20;
    auto    k = 30;
    auto    l = 40;

    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner
        , j
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner
        , k
    );
    state.execute(
        executeBackgroundSingletonWithJoinerAndUserDataTestProc
        , joiner
        , l
    );

    ASSERT_EQ( 3, state->threadSingletonDataSet.size() );

    auto &  data = state.getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        data.ended = true;

        data.cond.notify_one();
    }

    joiner.join();

    ASSERT_EQ( 100, i );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 0, state->threadSingletonDataSet.size() );
}

void executeBackgroundSingletonJoinedTestProc(
    fg::StateThreadBackgroundSingleton< ExecuteJoinedTestData > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    data.i = 30;
}

TEST(
    StateTest
    , ExecuteBackgroundSingleton_notJoinedWhenEntered
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenEnteredTestLeaveEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundSingletonJoinedTestProc );

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state->nextStateUnique->threadJoinerUnique->join();

    ASSERT_EQ( 10, j );
}

TEST(
    StateTest
    , ExecuteBackgroundSingleton_joinedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;
    auto    j = 20;

    prevState.enter(
        [
            &i
            , &j
        ]
        (
            fg::CreatingState< ExecuteJoinedTestData > &    _state
        )
        {
            _state.setEventProcs( executeJoinedWhenExitedTestExitEventProc );

            return ExecuteJoinedTestData::create(
                i
                , j
            ).release();
        }
        , ExecuteJoinedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteJoinedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundSingletonJoinedTestProc );

    state.exit();

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 30, j );
}

TEST(
    StateTest
    , ExecuteBackgroundSingleton_whenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    auto    i = 10;

    prevState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< ExecuteSingletonTestData > &
        )
        {
            return ExecuteSingletonTestData::create( i ).release();
        }
        , ExecuteSingletonTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteSingletonTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    state.execute( executeBackgroundSingletonTestProc );

    state->threadJoinerBackgroundUnique->join();

    ASSERT_EQ( 30, i );
}

void executeBackgroundSingletonFailedWhenExitedTestThread(
    fg::StateThreadBackgroundSingleton< ExecuteFailedWhenExitedTestData > & _thread
)
{
    auto &  data = _thread.getState().getData();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }
}

void executeBackgroundSingletonFailedWhenExitedTestThread2(
    fg::StateThreadBackgroundSingleton< ExecuteFailedWhenExitedTestData > & _thread
)
{
    _thread.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExecuteBackgroundSingleton_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    prevStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, prevStateUnique.get() );
    auto &  prevState = reinterpret_cast< fg::State<> & >( *prevStateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    auto    i = 10;

    prevState.enter(
        [
            &mutex
            , &cond
            , &ended
            , &i
        ]
        (
            fg::CreatingState< ExecuteFailedWhenExitedTestData > &
        )
        {
            return ExecuteFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
                , i
            ).release();
        }
        , ExecuteFailedWhenExitedTestData::destroy
    );

    auto &  stateUnique = prevState->nextStateUnique;
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< ExecuteFailedWhenExitedTestData > & >( *stateUnique );

    state->threadJoinerUnique->join();

    state.execute( executeBackgroundSingletonFailedWhenExitedTestThread );

    state.exit();

    state.execute( executeBackgroundSingletonFailedWhenExitedTestThread2 );

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    prevState->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

void enterTestEnterEventProc(
    fg::StateEnterEvent< StateTestData > &
)
{
}

void enterTestExitEventProc(
    fg::StateExitEvent< StateTestData > &
)
{
}

void enterTestReenterEventProc(
    fg::StateReenterEvent< StateTestData > &
)
{
}

void enterTestLeaveEventProc(
    fg::StateLeaveEvent< StateTestData > &
)
{
}

TEST(
    StateTest
    , Enter
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< int > & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState< StateTestData > &    _state
        )
        {
            _state.setEventProcs(
                enterTestEnterEventProc
                , enterTestExitEventProc
                , enterTestReenterEventProc
                , enterTestLeaveEventProc
            );

            return StateTestData::create().release();
        }
        , StateTestData::destroy
    );

    ASSERT_TRUE( state->disabled );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );

    nextStateUnique->threadJoinerUnique->join();

    ASSERT_FALSE( nextStateUnique->disabled );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestEnterEventProc ), nextStateUnique->enterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestExitEventProc ), nextStateUnique->exitEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestReenterEventProc ), nextStateUnique->reenterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestLeaveEventProc ), nextStateUnique->leaveEventProcPtr );
}

TEST(
    StateTest
    , Enter_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState< StateTestData > &    _state
        )
        {
            _state.setEventProcs(
                enterTestEnterEventProc
                , enterTestExitEventProc
                , enterTestReenterEventProc
                , enterTestLeaveEventProc
            );

            return StateTestData::create().release();
        }
        , StateTestData::destroy
    );

    ASSERT_TRUE( state->disabled );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );

    nextStateUnique->threadJoinerUnique->join();

    ASSERT_FALSE( nextStateUnique->disabled );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestEnterEventProc ), nextStateUnique->enterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestExitEventProc ), nextStateUnique->exitEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestReenterEventProc ), nextStateUnique->reenterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterTestLeaveEventProc ), nextStateUnique->leaveEventProcPtr );
}

TEST(
    StateTest
    , Enter_failedWhenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = reinterpret_cast< fg::State<> & >( *nextStateUnique );

    nextState->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    nextState->threadJoinerUnique->join();

    ASSERT_EQ( &*nextState, nextStateUnique.get() );
}

struct EnterFailedWhenExitedTestData : public fg::UniqueWrapper< EnterFailedWhenExitedTestData >
{
    std::mutex &                mutex;
    std::condition_variable &   cond;

    bool &  ended;

    EnterFailedWhenExitedTestData(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
    )
        : mutex( _mutex )
        , cond( _cond )
        , ended( _ended )
    {
    }

    static Unique create(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
    )
    {
        return new EnterFailedWhenExitedTestData(
            _mutex
            , _cond
            , _ended
        );
    }
};

void enterFailedWhenExitedTestThread(
    fg::StateThread< EnterFailedWhenExitedTestData > &  _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }
}

struct EnterFailedWhenExitedTestData2 : public fg::UniqueWrapper< EnterFailedWhenExitedTestData2 >
{
    int &   i;

    EnterFailedWhenExitedTestData2(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new EnterFailedWhenExitedTestData2( _i );
    }
};

void enterFailedWhenExitedTestEnterEventProc(
    fg::StateEnterEvent< EnterFailedWhenExitedTestData2 > & _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    StateTest
    , Enter_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    state.enter(
        [
            &mutex
            , &cond
            , &ended
        ]
        (
            fg::CreatingState< EnterFailedWhenExitedTestData > &
        )
        {
            return EnterFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
            ).release();
        }
        , EnterFailedWhenExitedTestData::destroy
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = reinterpret_cast< fg::State< EnterFailedWhenExitedTestData > & >( *nextStateUnique );

    nextState->threadJoinerUnique->join();

    nextState.execute( enterFailedWhenExitedTestThread );

    nextState.exit();

    auto    i = 10;

    nextState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< EnterFailedWhenExitedTestData2 > &   _state
        )
        {
            _state.setEventProcs( enterFailedWhenExitedTestEnterEventProc );

            return EnterFailedWhenExitedTestData2::create( i ).release();
        }
        , EnterFailedWhenExitedTestData2::destroy
    );

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    state->threadJoinerUnique->join();

    ASSERT_EQ( 10, i );
}

void enterWithoutStateDataTestEnterEventProc(
    fg::StateEnterEvent<> &
)
{
}

void enterWithoutStateDataTestExitEventProc(
    fg::StateExitEvent<> &
)
{
}

void enterWithoutStateDataTestReenterEventProc(
    fg::StateReenterEvent<> &
)
{
}

void enterWithoutStateDataTestLeaveEventProc(
    fg::StateLeaveEvent<> &
)
{
}

TEST(
    StateTest
    , EnterWithoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State< int > & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState<> &   _state
        )
        {
            _state.setEventProcs(
                enterWithoutStateDataTestEnterEventProc
                , enterWithoutStateDataTestExitEventProc
                , enterWithoutStateDataTestReenterEventProc
                , enterWithoutStateDataTestLeaveEventProc
            );
        }
    );

    ASSERT_TRUE( state->disabled );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );

    nextStateUnique->threadJoinerUnique->join();

    ASSERT_FALSE( nextStateUnique->disabled );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestEnterEventProc ), nextStateUnique->enterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestExitEventProc ), nextStateUnique->exitEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestReenterEventProc ), nextStateUnique->reenterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestLeaveEventProc ), nextStateUnique->leaveEventProcPtr );
}

TEST(
    StateTest
    , EnterWithoutStateData_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState<> &   _state
        )
        {
            _state.setEventProcs(
                enterWithoutStateDataTestEnterEventProc
                , enterWithoutStateDataTestExitEventProc
                , enterWithoutStateDataTestReenterEventProc
                , enterWithoutStateDataTestLeaveEventProc
            );
        }
    );

    ASSERT_TRUE( state->disabled );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );

    nextStateUnique->threadJoinerUnique->join();

    ASSERT_FALSE( nextStateUnique->disabled );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestEnterEventProc ), nextStateUnique->enterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestExitEventProc ), nextStateUnique->exitEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestReenterEventProc ), nextStateUnique->reenterEventProcPtr );
    ASSERT_EQ( reinterpret_cast< brownsugar::StateEvent::Proc >( enterWithoutStateDataTestLeaveEventProc ), nextStateUnique->leaveEventProcPtr );
}

struct EventTestData : public fg::UniqueWrapper< EventTestData >
{
    int &   i;

    EventTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new EventTestData( _i );
    }
};

void enterEventTestProc(
    fg::StateEnterEvent< EventTestData > &  _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    StateTest
    , EnterEvent
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< EventTestData > &    _state
        )
        {
            _state.setEventProcs( enterEventTestProc );

            return EventTestData::create( i ).release();
        }
        , EventTestData::destroy
    );

    auto &  nextState = reinterpret_cast< fg::State< EventTestData > & >( *( state->nextStateUnique ) );

    nextState->threadJoinerUnique->join();

    ASSERT_EQ( 20, i );
}

void leaveEventTestProc(
    fg::StateLeaveEvent< EventTestData > &  _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    StateTest
    , LeaveEvent
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< EventTestData > &    _state
        )
        {
            _state.setEventProcs( leaveEventTestProc );

            return EventTestData::create( i ).release();
        }
        , EventTestData::destroy
    );

    auto &  nextState = reinterpret_cast< fg::State< StateTestData > & >( *( state->nextStateUnique ) );

    nextState->threadJoinerUnique->join();

    nextState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    nextState->nextStateUnique->threadJoinerUnique->join();

    ASSERT_EQ( 20, i );
}

TEST(
    StateTest
    , Exit
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState< StateTestData > &
        )
        {
            return StateTestData::create().release();
        }
        , StateTestData::destroy
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = reinterpret_cast< fg::State< StateTestData > & >( *nextStateUnique );

    nextState->threadJoinerUnique->join();

    nextState.exit();

    state->threadJoinerUnique->join();

    ASSERT_EQ( nullptr, nextStateUnique.get() );
    ASSERT_FALSE( state->disabled );
}

TEST(
    StateTest
    , Exit_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = reinterpret_cast< fg::State<> & >( *nextStateUnique );

    nextState->threadJoinerUnique->join();

    nextState.exit();

    state->threadJoinerUnique->join();

    ASSERT_EQ( nullptr, nextStateUnique.get() );
    ASSERT_FALSE( state->disabled );
}

TEST(
    StateTest
    , Exit_failedWhenExistsNextState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    state.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = reinterpret_cast< fg::State<> & >( *nextStateUnique );

    nextState->threadJoinerUnique->join();

    nextState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    nextState->nextStateUnique->threadJoinerUnique->join();

    nextState.exit();

    state->threadJoinerUnique->join();

    ASSERT_NE( nullptr, nextStateUnique.get() );
}

struct ExitFailedWhenExitedTestData : public fg::UniqueWrapper< ExitFailedWhenExitedTestData >
{
    std::mutex &                mutex;
    std::condition_variable &   cond;

    bool &  ended;

    int &   i;

    ExitFailedWhenExitedTestData(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
        , int &                     _i
    )
        : mutex( _mutex )
        , cond( _cond )
        , ended( _ended )
        , i( _i )
    {
    }

    static Unique create(
        std::mutex &                _mutex
        , std::condition_variable & _cond
        , bool &                    _ended
        , int &                     _i
    )
    {
        return new ExitFailedWhenExitedTestData(
            _mutex
            , _cond
            , _ended
            , _i
        );
    }
};

void exitFailedWhenExitedTestExitEventProc(
    fg::StateExitEvent< ExitFailedWhenExitedTestData > &    _event
)
{
    _event.getState().getData().i += 10;
}

void exitFailedWhenExitedTestThread(
    fg::StateThread< ExitFailedWhenExitedTestData > &   _thread
)
{
    auto &  data = _thread.getState().getData();

    auto    lock = std::unique_lock< std::mutex >( data.mutex );

    if( data.ended == false ) {
        data.cond.wait_for(
            lock
            , std::chrono::milliseconds( 100 )
        );
    }
}

TEST(
    StateTest
    , Exit_failedWhenExited
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    auto    i = 10;

    state.enter(
        [
            &mutex
            , &cond
            , &ended
            , &i
        ]
        (
            fg::CreatingState< ExitFailedWhenExitedTestData > & _state
        )
        {
            _state.setEventProcs( exitFailedWhenExitedTestExitEventProc );

            return ExitFailedWhenExitedTestData::create(
                mutex
                , cond
                , ended
                , i
            ).release();
        }
        , ExitFailedWhenExitedTestData::destroy
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = reinterpret_cast< fg::State< ExitFailedWhenExitedTestData > & >( *nextStateUnique );

    nextState->threadJoinerUnique->join();

    nextState.execute( exitFailedWhenExitedTestThread );

    nextState.exit();
    nextState.exit();

    {
        auto    lock = std::unique_lock< std::mutex >( mutex );

        ended = true;

        cond.notify_one();
    }

    state->threadJoinerUnique->join();

    ASSERT_EQ( 20, i );
}

void exitEventTestProc(
    fg::StateExitEvent< EventTestData > &   _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    StateTest
    , ExitEvent
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< EventTestData > &    _state
        )
        {
            _state.setEventProcs( exitEventTestProc );

            return EventTestData::create( i ).release();
        }
        , EventTestData::destroy
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );
    auto &  nextState = reinterpret_cast< fg::State< EventTestData > & >( *nextStateUnique );

    nextState->threadJoinerUnique->join();

    nextState.exit();

    state->threadJoinerUnique->join();

    ASSERT_EQ( 20, i );
}

struct ReenterEventTestData : public fg::UniqueWrapper< ReenterEventTestData >
{
    const void * &  prevDataDestroyProcPtr;

    ReenterEventTestData(
        const void * &  _prevDataDestroyProcPtr
    )
        : prevDataDestroyProcPtr( _prevDataDestroyProcPtr )
    {
    }

    static Unique create(
        const void * &  _prevDataDestroyProcPtr
    )
    {
        return new ReenterEventTestData( _prevDataDestroyProcPtr );
    }
};

void reenterEventTestProc(
    fg::StateReenterEvent< ReenterEventTestData > & _event
)
{
    _event.getState().getData().prevDataDestroyProcPtr = _event.getData().getPrevDataDestroyProcPtr();
}

void reenterEventTestDestroyProc(
    StateTestData * _dataPtr
)
{
    StateTestData::destroy( _dataPtr );
}

TEST(
    StateTest
    , ReenterEvent
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = reinterpret_cast< fg::State<> & >( *stateUnique );

    const void *    prevDataDestroyProcPtr = nullptr;

    state.enter(
        [
            &prevDataDestroyProcPtr
        ]
        (
            fg::CreatingState< ReenterEventTestData > & _state
        )
        {
            _state.setEventProcs( reenterEventTestProc );

            return ReenterEventTestData::create( prevDataDestroyProcPtr ).release();
        }
        , ReenterEventTestData::destroy
    );

    auto &  nextState = reinterpret_cast< fg::State< ReenterEventTestData > & >( *( state->nextStateUnique ) );

    nextState->threadJoinerUnique->join();

    nextState.enter(
        [](
            fg::CreatingState< StateTestData > &    _state
        )
        {
            return StateTestData::create().release();
        }
        , reenterEventTestDestroyProc
    );

    nextState->nextStateUnique->threadJoinerUnique->join();

    reinterpret_cast< fg::State<> & >( *( nextState->nextStateUnique ) ).exit();

    nextState->threadJoinerUnique->join();

    ASSERT_EQ( reenterEventTestDestroyProc, prevDataDestroyProcPtr );
}

TEST(
    StateTest
    , GetBasesystem
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = reinterpret_cast< fg::State<> & >( *rootStateUnique );

    auto    i = 10;

    rootState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< int > &  _state
        )
        {
            return &i;
        }
        , reinterpret_cast< void ( * )( int * ) >( dummyDestroy )
    );

    auto &  state = reinterpret_cast< fg::State< int > & >( *( rootState->nextStateUnique ) );

    state->threadJoinerUnique->join();

    auto    j = 20;

    state.enter(
        [
            &j
        ]
        (
            fg::CreatingState< int > &  _state
        )
        {
            return &j;
        }
        , reinterpret_cast< void ( * )( int * ) >( dummyDestroy )
    );

    auto &  nextState = reinterpret_cast< fg::State< int > & >( *( state->nextStateUnique ) );

    ASSERT_EQ( 10, nextState.getBasesystem_< int >() );
}

TEST(
    StateTest
    , GetBasesystem_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    rootStateUnique = FgState::create(
        threadCache
        , nullptr
        , false
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, rootStateUnique.get() );
    auto &  rootState = reinterpret_cast< fg::State<> & >( *rootStateUnique );

    auto    i = 10;

    rootState.enter(
        [
            &i
        ]
        (
            fg::CreatingState< int > &  _state
        )
        {
            return &i;
        }
        , reinterpret_cast< void ( * )( int * ) >( dummyDestroy )
    );

    auto &  state = reinterpret_cast< fg::State< int > & >( *( rootState->nextStateUnique ) );

    state->threadJoinerUnique->join();

    state.enter(
        [](
            fg::CreatingState<> &   _state
        )
        {
        }
    );

    auto &  nextState = reinterpret_cast< fg::State<> & >( *( state->nextStateUnique ) );

    ASSERT_EQ( 10, nextState.getBasesystem_< int >() );
}
