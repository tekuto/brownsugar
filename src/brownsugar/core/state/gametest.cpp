﻿#include "fg/util/test.h"
#include "brownsugar/core/state/game.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/thread/cache.h"
#include "fg/core/state/creating.h"
#include "fg/common/unique.h"

void dummyDestroy(
    void *
)
{
}

struct GameStateTestData : public fg::UniqueWrapper< GameStateTestData >
{
    static Unique create(
    )
    {
        return new GameStateTestData;
    }
};

TEST(
    GameStateTest
    , Constructor
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameState = FgGameState(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );

    ASSERT_EQ( &state, &( gameState.state ) );
    ASSERT_EQ( reinterpret_cast< FgModuleContext * >( 10 ), &( gameState.moduleContext ) );
}

struct SetDataTestData : public fg::UniqueWrapper< SetDataTestData >
{
    int &   i;

    SetDataTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new SetDataTestData( _i );
    }

    static void destroy(
        SetDataTestData *   _this
    )
    {
        _this->i = 20;

        delete _this;
    }
};

TEST(
    GameStateTest
    , SetData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameStateImpl = FgGameState(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    auto &  gameState = reinterpret_cast< fg::GameState< SetDataTestData > & >( gameStateImpl );

    auto    i = 10;

    auto    dataUnique = SetDataTestData::create( i );
    ASSERT_NE( nullptr, dataUnique.get() );

    gameState.setData(
        dataUnique.release()
        , SetDataTestData::destroy
    );

    stateUnique.destroy();

    ASSERT_EQ( 20, i );
}

TEST(
    GameStateTest
    , GetCreatingState
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameStateImpl = FgGameState(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    auto &  gameState = reinterpret_cast< fg::GameState< GameStateTestData > & >( gameStateImpl );

    ASSERT_EQ( reinterpret_cast< fg::CreatingState< GameStateTestData > * >( &state ), &( gameState.getCreatingState() ) );
}

TEST(
    GameStateTest
    , GetCreatingState_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameStateImpl = FgGameState(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    auto &  gameState = reinterpret_cast< fg::GameState<> & >( gameStateImpl );

    ASSERT_EQ( reinterpret_cast< fg::CreatingState<> * >( &state ), &( gameState.getCreatingState() ) );
}

TEST(
    GameStateTest
    , GetModuleContext
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameStateImpl = FgGameState(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    auto &  gameState = reinterpret_cast< fg::GameState< GameStateTestData > & >( gameStateImpl );

    ASSERT_EQ( reinterpret_cast< fg::ModuleContext * >( 10 ), &( gameState.getModuleContext() ) );
}

TEST(
    GameStateTest
    , GetModuleContext_withoutStateData
)
{
    auto    threadCacheUnique = brownsugar::ThreadCache::create();
    ASSERT_NE( nullptr, threadCacheUnique.get() );
    auto &  threadCache = *threadCacheUnique;

    auto    stateUnique = FgState::create(
        threadCache
        , nullptr
        , true
        , nullptr
        , dummyDestroy
    );
    ASSERT_NE( nullptr, stateUnique.get() );
    auto &  state = *stateUnique;

    auto    gameStateImpl = FgGameState(
        state
        , *reinterpret_cast< FgModuleContext * >( 10 )
    );
    auto &  gameState = reinterpret_cast< fg::GameState<> & >( gameStateImpl );

    ASSERT_EQ( reinterpret_cast< fg::ModuleContext * >( 10 ), &( gameState.getModuleContext() ) );
}
