﻿#include "fg/util/test.h"
#include "fg/core/state/thread.h"
#include "fg/core/state/state.h"
#include "brownsugar/core/state/threadimpl.h"

struct StateThreadTestData;

TEST(
    StateThreadTest
    , GetState
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThread< StateThreadTestData, int > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadTestData > * >( 20 ), &( thread.getState() ) );
}

TEST(
    StateThreadTest
    , GetState_withoutUserData
)
{
    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 10 )
        , nullptr
    );
    auto &  thread = reinterpret_cast< fg::StateThread< StateThreadTestData > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< StateThreadTestData > * >( 10 ), &( thread.getState() ) );
}

TEST(
    StateThreadTest
    , GetData
)
{
    auto    i = 10;

    auto    threadImpl = brownsugar::StateThread(
        *reinterpret_cast< FgState * >( 20 )
        , &i
    );
    auto &  thread = reinterpret_cast< fg::StateThread< StateThreadTestData, int > & >( threadImpl );

    ASSERT_EQ( 10, thread.getData() );
}
