﻿#include "fg/util/export.h"
#include "fg/core/state/threadbackground.h"
#include "brownsugar/core/state/threadimpl.h"

FgState * fgStateThreadBackgroundGetState(
    FgStateThreadBackground *   _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getState();
}

void * fgStateThreadBackgroundGetData(
    FgStateThreadBackground *   _implPtr
)
{
    return reinterpret_cast< brownsugar::StateThread * >( _implPtr )->getData();
}
