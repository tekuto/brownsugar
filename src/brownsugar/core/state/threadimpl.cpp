﻿#include "brownsugar/core/state/threadimpl.h"

namespace brownsugar {
    StateThread::StateThread(
        FgState &   _state
        , void *    _userDataPtr
    )
        : state( _state )
        , userDataPtr( _userDataPtr )
    {
    }

    FgState * StateThread::getState(
    )
    {
        return &( this->state );
    }

    void * StateThread::getData(
    )
    {
        return this->userDataPtr;
    }
}
