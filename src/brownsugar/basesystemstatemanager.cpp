﻿#include "fg/util/export.h"
#include "brownsugar/basesystemstatemanager.h"
#include "brownsugar/core/state/state.h"
#include "brownsugar/core/state/basesystem.h"
#include "brownsugar/core/module/context.h"

#include <string>
#include <utility>

namespace {
    const auto  PACKAGE_PATH = std::string( "." );

    void dummyDestroy(
        void *
    )
    {
    }

    brownsugar::StateManager::Unique createStateManager(
    )
    {
        auto    thisUnique = brownsugar::StateManager::create();

        auto &  rootState = thisUnique->getState();
        rootState->disabled = true;

        auto    basesystemStateUnique = FgState::create(
            rootState->threadCache
            , &*rootState
            , true
            , nullptr
            , dummyDestroy
        );
        auto &  basesystemState = *basesystemStateUnique;

        rootState->setNextState( std::move( basesystemStateUnique ) );

        auto    gameStateUnique = FgState::create(
            rootState->threadCache
            , &*basesystemState
            , true
            , nullptr
            , dummyDestroy
        );

        basesystemState->setNextState( std::move( gameStateUnique ) );

        return std::move( thisUnique );
    }

    FgState & getBasesystemState(
        brownsugar::StateManager &  _stateManager
    )
    {
        auto &  rootState = _stateManager.getState();

        return rootState->getNextState();
    }
}

namespace brownsugar {
    BasesystemStateManager::BasesystemStateManager(
    )
        : stateManagerUnique( createStateManager() )
        , moduleContextUnique( FgModuleContext::create( PACKAGE_PATH ) )
        , basesystemStateUnique(
            FgBasesystemState::create(
                ::getBasesystemState( *( this->stateManagerUnique ) )
                , **( this->moduleContextUnique )
            )
        )
    {
    }

    BasesystemStateManager * BasesystemStateManager::create_(
    )
    {
        return new BasesystemStateManager;
    }

    void BasesystemStateManager::destroy(
        BasesystemStateManager *    _this
    )
    {
        delete _this;
    }

    FgBasesystemState & BasesystemStateManager::getBasesystemState_(
    )
    {
        return *( this->basesystemStateUnique );
    }

    FgState & BasesystemStateManager::getTopState_(
    )
    {
        return this->getBasesystemState< void >().getState()->getNextState();
    }

    void BasesystemStateManager::start(
    )
    {
        auto &  state = this->getTopState_();

        state.executeEnterThread();

        state.threadJoinerUnique->join();
    }
}
