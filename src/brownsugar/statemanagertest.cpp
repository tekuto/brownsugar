﻿#include "fg/util/test.h"
#include "brownsugar/statemanager.h"
#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/enterevent.h"
#include "fg/common/unique.h"

struct GetStateTestData : public fg::UniqueWrapper< GetStateTestData >
{
    int &   i;

    GetStateTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new GetStateTestData( _i );
    }
};

void getStateTestEnterEventProc(
    fg::StateEnterEvent< GetStateTestData > &   _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    StateManagerTest
    , GetState
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< GetStateTestData > & _state
        )
        {
            _state.setEventProcs( getStateTestEnterEventProc );

            return GetStateTestData::create( i ).release();
        }
        , GetStateTestData::destroy
    );

    auto &  nextStateUnique = state->nextStateUnique;
    ASSERT_NE( nullptr, nextStateUnique.get() );

    nextStateUnique->threadJoinerUnique->join();

    ASSERT_EQ( 20, i );
}
