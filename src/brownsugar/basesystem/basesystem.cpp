﻿#include "fg/util/export.h"
#include "brownsugar/basesystem/basesystem.h"
#include "brownsugar/basesystem/window/mainwindowdrawevent.h"
#include "brownsugar/basesystem/controller/raw/event.h"
#include "fg/core/state/eventmanager.h"
#include "fg/core/state/joiner.h"
#include "fg/core/state/state.h"
#include "fg/window/window.h"
#include "fg/window/eventmanagers.h"
#include "fg/window/paintevent.h"
#include "fg/window/eventprocessor.h"
#include "fg/gl/context.h"
#include "fg/gl/threadexecutor.h"
#include "fg/gl/thread.h"
#include "fg/controller/raw/actionbuffer.h"
#include "sucrose/controller/raw/manager.h"

namespace {
    const auto  WINDOW_TITLE = "BROWNSUGAR";
    const auto  WINDOW_WIDTH = 800;
    const auto  WINDOW_HEIGHT = 600;

    auto createWindow(
    )
    {
        return fg::Window::create(
            WINDOW_TITLE
            , WINDOW_WIDTH
            , WINDOW_HEIGHT
        );
    }

    void paintEvent(
        fg::WindowPaintEventBackground< brownsugar::Basesystem > &  _event
    )
    {
        _event.getState().getData().glThreadExecutorUnique->execute();
    }

    void glThread(
        fg::GLThread< brownsugar::Basesystem > &    _thread
    )
    {
        auto &  basesystem = _thread.getState().getData();

        auto    eventData = FgMainWindowDrawEventData{
            *( basesystem.windowUnique ),
        };

        auto &  joiner = *( basesystem.mainWindowDrawEventJoinerUnique );

        basesystem.mainWindowDrawEventManagerUnique->execute(
            joiner
            , eventData
        );

        joiner.join();
    }

    void rawControllerEventProc(
        const fg::RawControllerActionBuffer &   _ACTION_BUFFER
        , brownsugar::Basesystem &              _basesystem
    )
    {
        auto    eventData = FgRawControllerEventData{
            *_ACTION_BUFFER,
        };

        auto &  joiner = *( _basesystem.rawControllerEventJoinerUnique );

        _basesystem.rawControllerEventManagerUnique->execute(
            joiner
            , eventData
        );

        joiner.join();
    }

    auto createRawControllerManager(
        fg::State< brownsugar::Basesystem > &   _state
        , brownsugar::Basesystem &              _basesystem
    )
    {
        return sucrose::RawControllerManager::create(
            _state
            , rawControllerEventProc
            , _basesystem
        );
    }
}

namespace brownsugar {
    Basesystem::Basesystem(
    )
    {
    }

    void Basesystem::initialize(
        fg::State< Basesystem > &   _state
    )
    {
        this->mainWindowDrawEventManagerUnique = fg::StateEventManager< FgMainWindowDrawEventData >::create();
        this->mainWindowDrawEventJoinerUnique = fg::StateJoiner::create();

        this->rawControllerEventManagerUnique = fg::StateEventManager< FgRawControllerEventData >::create();
        this->rawControllerEventJoinerUnique = fg::StateJoiner::create();

        auto &  windowUnique = this->windowUnique;
        windowUnique = createWindow();
        auto &  window = *windowUnique;

        auto &  windowEventManagersUnique = this->windowEventManagersUnique;
        windowEventManagersUnique = fg::WindowEventManagers::create();
        auto &  windowEventManagers = *windowEventManagersUnique;

        this->windowPaintEventRegisterManagerUnique = fg::WindowPaintEventRegisterManager::create(
            windowEventManagers.getPaintEventManager()
            , _state
            , paintEvent
        );

        auto &  glContextUnique = this->glContextUnique;
        glContextUnique = fg::GLContext::create( window );
        auto &  glContext = *glContextUnique;

        this->glThreadExecutorUnique = fg::GLThreadExecutor::create(
            _state
            , glContext
            , glThread
        );

        this->windowEventProcessorUnique = fg::WindowEventProcessor::create(
            _state
            , window
            , windowEventManagers
        );

        this->rawControllerManagerUnique = createRawControllerManager(
            _state
            , *this
        );
    }

    sucrose::RawControllerManager & Basesystem::getRawControllerManager(
    )
    {
        return *( this->rawControllerManagerUnique );
    }
}
