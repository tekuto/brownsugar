﻿#include "fg/util/test.h"
#include "fg/basesystem/window/mainwindow.h"
#include "fg/core/state/basesystem.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/state.h"
#include "brownsugar/basesystemstatemanager.h"
#include "brownsugar/basesystem/basesystem.h"

#include <X11/Xlib.h>

TEST(
    MainWindowTest
    , GetMainWindowForCreatingState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    const auto &    BASESYSTEM = state.getData();
    ASSERT_NE( nullptr, &BASESYSTEM );

    ASSERT_EQ( BASESYSTEM.windowUnique.get(), &( fg::getMainWindow( reinterpret_cast< fg::CreatingState<> & >( state ) ) ) );
}

TEST(
    MainWindowTest
    , GetMainWindowForState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    const auto &    BASESYSTEM = state.getData();
    ASSERT_NE( nullptr, &BASESYSTEM );

    ASSERT_EQ( BASESYSTEM.windowUnique.get(), &( fg::getMainWindow( state ) ) );
}
