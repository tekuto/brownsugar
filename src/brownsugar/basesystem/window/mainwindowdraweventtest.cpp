﻿#include "fg/util/test.h"
#include "brownsugar/basesystem/window/mainwindowdrawevent.h"
#include "brownsugar/basesystem/basesystem.h"
#include "brownsugar/basesystemstatemanager.h"
#include "fg/window/window.h"
#include "fg/core/state/creating.h"

#include <X11/Xlib.h>

TEST(
    MainWindowDrawEventDataTest
    , GetWindow
)
{
    auto    windowUnique = fg::Window::create(
        "MainWindowDrawEventDataTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    dataImpl = FgMainWindowDrawEventData{ window };
    auto &  data = reinterpret_cast< fg::MainWindowDrawEventData & >( dataImpl );

    ASSERT_EQ( &window, &( data.getWindow() ) );
}

void createTestProc(
    fg::MainWindowDrawEvent<> &
)
{
}

TEST(
    MainWindowDrawEventRegisterManagerTest
    , CreateForCreatingState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    auto    registerManagerUnique = fg::MainWindowDrawEventRegisterManager::create(
        reinterpret_cast< fg::CreatingState<> & >( basesystemStateManagerUnique->getTopState() )
        , createTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}

TEST(
    MainWindowDrawEventRegisterManagerTest
    , CreateForState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    auto    registerManagerUnique = fg::MainWindowDrawEventRegisterManager::create(
        basesystemStateManagerUnique->getTopState()
        , createTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}
