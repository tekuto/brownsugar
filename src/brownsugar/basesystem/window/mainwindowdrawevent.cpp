﻿#include "fg/util/export.h"
#include "brownsugar/basesystem/window/mainwindowdrawevent.h"
#include "brownsugar/basesystem/basesystem.h"
#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/eventregistermanager.h"

FgWindow * fgBasesystemMainWindowDrawEventDataGetWindow(
    FgMainWindowDrawEventData * _implPtr
)
{
    return &*( _implPtr->window );
}

FgMainWindowDrawEventRegisterManager * fgBasesystemMainWindowDrawEventRegisterManagerCreateForCreatingState(
    FgCreatingState *   _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );
    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return reinterpret_cast< FgMainWindowDrawEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForCreatingState(
            &**( basesystem.mainWindowDrawEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgMainWindowDrawEventRegisterManager * fgBasesystemMainWindowDrawEventRegisterManagerCreateForState(
    FgState *           _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );
    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return reinterpret_cast< FgMainWindowDrawEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForState(
            &**( basesystem.mainWindowDrawEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

void fgBasesystemMainWindowDrawEventRegisterManagerDestroy(
    FgMainWindowDrawEventRegisterManager *  _implPtr
)
{
    fgStateEventRegisterManagerDestroy( reinterpret_cast< FgStateEventRegisterManager * >( _implPtr ) );
}
