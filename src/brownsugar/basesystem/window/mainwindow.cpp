﻿#include "fg/util/export.h"
#include "fg/basesystem/window/mainwindow.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/state.h"
#include "brownsugar/basesystem/basesystem.h"

FgWindow * fgBasesystemGetMainWindowForCreatingState(
    FgCreatingState *   _statePtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );

    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return &**( basesystem.windowUnique );
}

FgWindow * fgBasesystemGetMainWindowForState(
    FgState *   _statePtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );

    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return &**( basesystem.windowUnique );
}
