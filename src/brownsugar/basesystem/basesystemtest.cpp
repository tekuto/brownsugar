﻿#include "fg/util/test.h"
#include "brownsugar/basesystem/basesystem.h"
#include "brownsugar/basesystemstatemanager.h"
#include "fg/basesystem/window/mainwindow.h"
#include "fg/basesystem/window/mainwindowdrawevent.h"
#include "fg/basesystem/controller/raw/event.h"

#include <X11/Xlib.h>

TEST(
    BasesystemTest
    , GetRawControllerManager
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    ASSERT_NE( nullptr, &( basesystem.getRawControllerManager() ) );
}

struct MainWindowDrawEventTestData : public fg::UniqueWrapper< MainWindowDrawEventTestData >
{
    int &   i;

    fg::MainWindowDrawEventRegisterManager::Unique  registerManagerUnique;

    MainWindowDrawEventTestData(
        int &                                               _i
        , fg::MainWindowDrawEventRegisterManager::Unique && _registerManagerUnique
    )
        : i( _i )
        , registerManagerUnique( std::move( _registerManagerUnique ) )
    {
    }

    static Unique create(
        int &                                               _i
        , fg::MainWindowDrawEventRegisterManager::Unique && _registerManagerUnique
    )
    {
        return new MainWindowDrawEventTestData(
            _i
            , std::move( _registerManagerUnique )
        );
    }
};

void mainWindowDrawEventTestProc(
    fg::MainWindowDrawEvent< MainWindowDrawEventTestData > &    _event
)
{
    auto &  data = _event.getState().getData();

    data.i = 20;
}

TEST(
    BasesystemTest
    , MainWindowDrawEvent
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( basesystemState.getState() );

    basesystemStateManagerUnique->start();

    auto &  state = basesystemStateManagerUnique->getTopState();

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< MainWindowDrawEventTestData > &  _state
        )
        {
            auto    registerManagerUnique = fg::MainWindowDrawEventRegisterManager::create(
                _state
                , mainWindowDrawEventTestProc
            );

            return MainWindowDrawEventTestData::create(
                i
                , std::move( registerManagerUnique )
            ).release();
        }
        , MainWindowDrawEventTestData::destroy
    );

    fg::getMainWindow( state ).repaint();

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    ASSERT_EQ( 20, i );
}

struct RawControllerEventTestData : public fg::UniqueWrapper< RawControllerEventTestData >
{
    int &   i;

    fg::RawControllerEventRegisterManager::Unique   registerManagerUnique;

    RawControllerEventTestData(
        int &                                               _i
        , fg::RawControllerEventRegisterManager::Unique && _registerManagerUnique
    )
        : i( _i )
        , registerManagerUnique( std::move( _registerManagerUnique ) )
    {
    }

    static Unique create(
        int &                                               _i
        , fg::RawControllerEventRegisterManager::Unique && _registerManagerUnique
    )
    {
        return new RawControllerEventTestData(
            _i
            , std::move( _registerManagerUnique )
        );
    }
};

void rawControllerEventTestProc(
    fg::RawControllerEvent< RawControllerEventTestData > &  _event
)
{
    auto &  data = _event.getState().getData();

    data.i = 20;
}

TEST(
    BasesystemTest
    , RawControllerEvent
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( basesystemState.getState() );

    basesystemStateManagerUnique->start();

    auto &  state = basesystemStateManagerUnique->getTopState();

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< RawControllerEventTestData > &   _state
        )
        {
            auto    registerManagerUnique = fg::RawControllerEventRegisterManager::create(
                _state
                , rawControllerEventTestProc
            );

            return RawControllerEventTestData::create(
                i
                , std::move( registerManagerUnique )
            ).release();
        }
        , RawControllerEventTestData::destroy
    );

    auto &  zarame = basesystemState.getState().getData();

    zarame.rawControllerManagerUnique->connect(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    ASSERT_EQ( 20, i );
}
