﻿#include "fg/util/test.h"
#include "brownsugar/basesystem/controller/raw/event.h"
#include "brownsugar/basesystem/basesystem.h"
#include "brownsugar/basesystemstatemanager.h"
#include "fg/controller/raw/actionbuffer.h"

#include <X11/Xlib.h>

TEST(
    RawControllerEventDataTest
    , GetActionBuffer
)
{
    const auto  EVENT_DATA_IMPL = FgRawControllerEventData{
        *reinterpret_cast< const FgRawControllerActionBuffer * >( 10 ),
    };
    const auto &    EVENT_DATA = reinterpret_cast< const fg::RawControllerEventData & >( EVENT_DATA_IMPL );

    ASSERT_EQ( reinterpret_cast< const fg::RawControllerActionBuffer * >( 10 ), &( EVENT_DATA.getActionBuffer() ) );
}

void createTestProc(
    fg::RawControllerEvent<> &
)
{
}

TEST(
    RawControllerEventRegisterManagerTest
    , CreateForCreatingState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    auto    registerManagerUnique = fg::RawControllerEventRegisterManager::create(
        reinterpret_cast< fg::CreatingState<> & >( basesystemStateManagerUnique->getTopState() )
        , createTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}

TEST(
    RawControllerEventRegisterManagerTest
    , CreateForState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    auto    registerManagerUnique = fg::RawControllerEventRegisterManager::create(
        basesystemStateManagerUnique->getTopState()
        , createTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}

void createBackgroundTestProc(
    fg::RawControllerEventBackground<> &
)
{
}

TEST(
    RawControllerEventRegisterManagerTest
    , CreateBackgroundForCreatingState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    auto    registerManagerUnique = fg::RawControllerEventRegisterManager::create(
        reinterpret_cast< fg::CreatingState<> & >( basesystemStateManagerUnique->getTopState() )
        , createBackgroundTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}

TEST(
    RawControllerEventRegisterManagerTest
    , CreateBackgroundForState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< brownsugar::Basesystem >();
    auto &  state = basesystemState.getState();

    auto    basesystemUnique = brownsugar::Basesystem::create();
    ASSERT_NE( nullptr, basesystemUnique.get() );
    auto &  basesystem = *basesystemUnique;

    basesystemState.setData(
        basesystemUnique.release()
        , brownsugar::Basesystem::destroy
    );

    basesystem.initialize( state );

    auto    registerManagerUnique = fg::RawControllerEventRegisterManager::create(
        basesystemStateManagerUnique->getTopState()
        , createBackgroundTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}
