﻿#include "fg/util/export.h"
#include "brownsugar/basesystem/controller/raw/event.h"
#include "brownsugar/basesystem/basesystem.h"
#include "fg/core/state/eventregistermanager.h"

const FgRawControllerActionBuffer * fgBasesystemRawControllerEventDataGetActionBuffer(
    const FgRawControllerEventData *    _IMPL_PTR
)
{
    return &( _IMPL_PTR->ACTION_BUFFER );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateForCreatingState(
    FgCreatingState *   _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );
    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForCreatingState(
            &**( basesystem.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateForState(
    FgState *           _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );
    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForState(
            &**( basesystem.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForCreatingState(
    FgCreatingState *               _statePtr
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );
    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateBackgroundForCreatingState(
            &**( basesystem.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForState(
    FgState *                       _statePtr
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );
    auto &  basesystem = state.getBasesystem_< brownsugar::Basesystem >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateBackgroundForState(
            &**( basesystem.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

void fgBasesystemRawControllerEventRegisterManagerDestroy(
    FgRawControllerEventRegisterManager *   _implPtr
)
{
    fgStateEventRegisterManagerDestroy( reinterpret_cast< FgStateEventRegisterManager * >( _implPtr ) );
}
