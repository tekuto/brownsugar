﻿#ifndef BROWNSUGAR_DEF_CORE_THREAD_THREAD_H
#define BROWNSUGAR_DEF_CORE_THREAD_THREAD_H

namespace brownsugar {
    class ThreadImpl;

    using ThreadProcImpl = void ( * )(
        ThreadImpl &
    );

    template< typename >
    class Thread;

    template< typename USER_DATA_T >
    using ThreadProc = void ( * )(
        Thread< USER_DATA_T > &
    );
}

#endif  // BROWNSUGAR_DEF_CORE_THREAD_THREAD_H
