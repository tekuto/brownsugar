﻿#ifndef BROWNSUGAR_DEF_CORE_THREAD_SINGLETON_H
#define BROWNSUGAR_DEF_CORE_THREAD_SINGLETON_H

namespace brownsugar {
    class ThreadSingletonImpl;

    using ThreadSingletonProcImpl = void ( * )(
        ThreadSingletonImpl &
    );

    template< typename >
    class ThreadSingleton;

    template< typename USER_DATA_T >
    using ThreadSingletonProc = void ( * )(
        ThreadSingleton< USER_DATA_T > &
    );
}

#endif  // BROWNSUGAR_DEF_CORE_THREAD_SINGLETON_H
