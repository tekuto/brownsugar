﻿#ifndef BROWNSUGAR_DEF_CORE_COMMON_FILEDATA_H
#define BROWNSUGAR_DEF_CORE_COMMON_FILEDATA_H

#include <vector>

namespace brownsugar {
    using FileData = std::vector< char >;
}

#endif  // BROWNSUGAR_DEF_CORE_COMMON_FILEDATA_H
