﻿#ifndef BROWNSUGAR_STATEMANAGER_H
#define BROWNSUGAR_STATEMANAGER_H

#include "brownsugar/def/statemanager.h"
#include "brownsugar/core/thread/cache.h"
#include "brownsugar/core/state/state.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

namespace brownsugar {
    class StateManager : public fg::UniqueWrapper< StateManager >
    {
        ThreadCache::Unique threadCacheUnique;

        FgState::Unique stateUnique;

    public:
        StateManager(
        );

        static FG_FUNCTION_PTR(
            StateManager
            , create_(
            )
        )

        inline static Unique create(
        )
        {
            return create_();
        }

        static FG_FUNCTION_VOID(
            destroy(
                StateManager *
            )
        )

        FG_FUNCTION_REF(
            fg::State<>
            , getState(
            )
        )
    };
}

#endif  // BROWNSUGAR_STATEMANAGER_H
