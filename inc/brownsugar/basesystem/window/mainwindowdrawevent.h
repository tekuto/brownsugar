﻿#ifndef BROWNSUGAR_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H
#define BROWNSUGAR_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H

#include "fg/basesystem/window/mainwindowdrawevent.h"
#include "fg/window/window.h"

struct FgMainWindowDrawEventData
{
    fg::Window &    window;
};

#endif  // BROWNSUGAR_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H
