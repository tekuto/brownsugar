﻿#ifndef BROWNSUGAR_BASESYSTEM_BASESYSTEM_H
#define BROWNSUGAR_BASESYSTEM_BASESYSTEM_H

#include "brownsugar/def/basesystem/basesystem.h"
#include "fg/core/state/state.h"
#include "fg/core/state/eventmanager.h"
#include "fg/core/state/joiner.h"
#include "fg/basesystem/window/mainwindowdrawevent.h"
#include "fg/basesystem/controller/raw/event.h"
#include "fg/window/window.h"
#include "fg/window/eventmanagers.h"
#include "fg/window/paintevent.h"
#include "fg/window/eventprocessor.h"
#include "fg/gl/context.h"
#include "fg/gl/threadexecutor.h"
#include "fg/common/unique.h"
#include "sucrose/controller/raw/manager.h"
#include "fg/util/import.h"

namespace brownsugar {
    struct Basesystem : public fg::UniqueWrapper< Basesystem >
    {
        fg::StateEventManager< FgMainWindowDrawEventData >::Unique  mainWindowDrawEventManagerUnique;
        fg::StateJoiner::Unique                                     mainWindowDrawEventJoinerUnique;

        fg::StateEventManager< FgRawControllerEventData >::Unique   rawControllerEventManagerUnique;
        fg::StateJoiner::Unique                                     rawControllerEventJoinerUnique;

        fg::Window::Unique  windowUnique;

        fg::WindowEventManagers::Unique             windowEventManagersUnique;
        fg::WindowPaintEventRegisterManager::Unique windowPaintEventRegisterManagerUnique;

        fg::GLContext::Unique           glContextUnique;
        fg::GLThreadExecutor::Unique    glThreadExecutorUnique;

        fg::WindowEventProcessor::Unique    windowEventProcessorUnique;

        sucrose::RawControllerManager::Unique   rawControllerManagerUnique;

        Basesystem(
        );

        inline static Unique create(
        )
        {
            return new Basesystem;
        }

        void initialize(
            fg::State< Basesystem > &
        );

        FG_FUNCTION_REF(
            sucrose::RawControllerManager
            , getRawControllerManager(
            )
        )
    };
}

#endif  // BROWNSUGAR_BASESYSTEM_BASESYSTEM_H
