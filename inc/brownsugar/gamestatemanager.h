﻿#ifndef BROWNSUGAR_GAMESTATEMANAGER_H
#define BROWNSUGAR_GAMESTATEMANAGER_H

#include "brownsugar/def/gamestatemanager.h"
#include "brownsugar/basesystemstatemanager.h"
#include "brownsugar/def/basesystem/basesystem.h"
#include "brownsugar/core/module/context.h"
#include "brownsugar/core/state/game.h"
#include "fg/core/state/basesystem.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

namespace brownsugar {
    class GameStateManager : public fg::UniqueWrapper< GameStateManager >
    {
        BasesystemStateManager::Unique  basesystemStateManagerUnique;

        fg::ModuleContext::Unique   moduleContextUnique;

        FgGameState::Unique gameStateUnique;

    public:
        GameStateManager(
        );

        static FG_FUNCTION_PTR(
            GameStateManager
            , create_(
            )
        )

        inline static Unique create(
        )
        {
            return create_();
        }

        static FG_FUNCTION_VOID(
            destroy(
                GameStateManager *
            )
        )

        FG_FUNCTION_REF(
            FgGameState
            , getGameState_(
            )
        )

        template< typename STATE_DATA_T >
        fg::GameState< STATE_DATA_T > & getGameState(
        )
        {
            return reinterpret_cast< fg::GameState< STATE_DATA_T > & >( this->getGameState_() );
        }

        FG_FUNCTION_REF(
            FgBasesystemState
            , getBasesystemState_(
            )
        )

        inline fg::BasesystemState< Basesystem > & getBasesystemState(
        )
        {
            return reinterpret_cast< fg::BasesystemState< Basesystem > & >( this->getBasesystemState_() );
        }

        FG_FUNCTION_VOID(
            start(
            )
        )
    };
}

#endif  // BROWNSUGAR_GAMESTATEMANAGER_H
