﻿#ifndef BROWNSUGAR_BASESYSTEMSTATEMANAGER_H
#define BROWNSUGAR_BASESYSTEMSTATEMANAGER_H

#include "brownsugar/def/basesystemstatemanager.h"
#include "brownsugar/statemanager.h"
#include "brownsugar/core/module/context.h"
#include "brownsugar/core/state/basesystem.h"
#include "fg/util/import.h"

namespace brownsugar {
    class BasesystemStateManager : public fg::UniqueWrapper< BasesystemStateManager >
    {
        brownsugar::StateManager::Unique    stateManagerUnique;

        fg::ModuleContext::Unique   moduleContextUnique;

        FgBasesystemState::Unique   basesystemStateUnique;

    public:
        BasesystemStateManager(
        );

        static FG_FUNCTION_PTR(
            BasesystemStateManager
            , create_(
            )
        )

        inline static Unique create(
        )
        {
            return create_();
        }

        static FG_FUNCTION_VOID(
            destroy(
                BasesystemStateManager *
            )
        )

        FG_FUNCTION_REF(
            FgBasesystemState
            , getBasesystemState_(
            )
        )

        template< typename STATE_DATA_T >
        fg::BasesystemState< STATE_DATA_T > & getBasesystemState(
        )
        {
            return reinterpret_cast< fg::BasesystemState< STATE_DATA_T > & >( this->getBasesystemState_() );
        }

        FG_FUNCTION_REF(
            FgState
            , getTopState_(
            )
        )

        inline fg::State<> & getTopState(
        )
        {
            return reinterpret_cast< fg::State<> & >( this->getTopState_() );
        }

        FG_FUNCTION_VOID(
            start(
            )
        )
    };
}

#endif  // BROWNSUGAR_BASESYSTEMSTATEMANAGER_H
