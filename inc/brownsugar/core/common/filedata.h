﻿#ifndef BROWNSUGAR_CORE_COMMON_FILEDATA_H
#define BROWNSUGAR_CORE_COMMON_FILEDATA_H

#include "brownsugar/def/core/common/filedata.h"

#include <string>

namespace brownsugar {
    FileData readFile(
        const std::string &
    );

    void writeFile(
        const std::string &
        , const FileData &
    );
}

#endif  // BROWNSUGAR_CORE_COMMON_FILEDATA_H
