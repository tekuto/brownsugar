﻿#ifndef BROWNSUGAR_CORE_MODULE_CONTEXT_H
#define BROWNSUGAR_CORE_MODULE_CONTEXT_H

#include "fg/def/core/module/context.h"
#include "fg/util/import.h"

#include <string>

struct FgModuleContext
{
    const std::string   PACKAGE_PATH;

    FgModuleContext(
        const std::string &
    );

    static FG_FUNCTION_PTR(
        FgModuleContext
        , create_(
            const std::string &
        )
    )

    inline static fg::ModuleContext::Unique create(
        const std::string & _PACKAGE_PATH
    )
    {
        return create_( _PACKAGE_PATH );
    }

    const std::string & getPackagePath(
    ) const;
};

#endif  // BROWNSUGAR_CORE_MODULE_CONTEXT_H
