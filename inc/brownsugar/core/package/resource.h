﻿#ifndef BROWNSUGAR_CORE_PACKAGE_RESOURCE_H
#define BROWNSUGAR_CORE_PACKAGE_RESOURCE_H

#include "fg/def/core/package/resource.h"
#include "brownsugar/def/core/common/filedata.h"

struct FgPackageResource
{
    brownsugar::FileData    fileData;
};

#endif  // BROWNSUGAR_CORE_PACKAGE_RESOURCE_H
