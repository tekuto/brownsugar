﻿#ifndef BROWNSUGAR_CORE_STATE_GAME_H
#define BROWNSUGAR_CORE_STATE_GAME_H

#include "fg/core/state/game.h"
#include "fg/core/state/state.h"
#include "fg/core/module/context.h"
#include "fg/util/import.h"

struct FgGameState : public fg::UniqueWrapper< FgGameState >
{
    FgState &           state;
    FgModuleContext &   moduleContext;

    FgGameState(
        FgState &
        , FgModuleContext &
    );

    static FG_FUNCTION_PTR(
        FgGameState
        , create_(
            FgState &
            , FgModuleContext &
        )
    )

    inline static Unique create(
        FgState &           _state
        , FgModuleContext & _moduleContext
    )
    {
        return create_(
            _state
            , _moduleContext
        );
    }

    static FG_FUNCTION_VOID(
        destroy(
            FgGameState *
        )
    )
};

#endif  // BROWNSUGAR_CORE_STATE_GAME_H
