﻿#ifndef BROWNSUGAR_CORE_STATE_EVENTMANAGER_H
#define BROWNSUGAR_CORE_STATE_EVENTMANAGER_H

#include "fg/core/state/eventmanager.h"
#include "fg/core/state/event.h"
#include "fg/core/state/eventbackground.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"
#include "fg/common/unique.h"
#include "brownsugar/core/state/eventimpl.h"

#include <vector>
#include <memory>
#include <mutex>

struct FgStateEventManager
{
    struct RegisterInfo : public fg::UniqueWrapper< RegisterInfo >
    {
        const bool  BACKGROUND;

        FgStateEventManager &           manager;
        FgState &                       state;
        brownsugar::StateEvent::Proc    eventProcPtr;

        fg::StateJoiner::Unique joinerUnique;

        RegisterInfo(
            bool
            , FgStateEventManager &
            , FgState &
            , brownsugar::StateEvent::Proc
        );

        static Unique create(
            FgStateEventManager &
            , FgState &
            , FgStateEventProc
        );

        static Unique create(
            FgStateEventManager &
            , FgState &
            , FgStateEventBackgroundProc
        );
    };

    using RegisterInfoUniques = std::vector< RegisterInfo::Unique >;

    struct UnregisterEvent
    {
        void operator()(
            RegisterInfo *
        ) const;
    };

    using EventProcUnregisterer = std::unique_ptr<
        RegisterInfo
        , UnregisterEvent
    >;

    std::mutex  mutex;

    RegisterInfoUniques registerInfoUniques;

    EventProcUnregisterer registerEventProc(
        FgState &
        , FgStateEventProc
    );

    EventProcUnregisterer registerEventProc(
        FgState &
        , FgStateEventBackgroundProc
    );
};

#endif  // BROWNSUGAR_CORE_STATE_EVENTMANAGER_H
