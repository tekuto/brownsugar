﻿#ifndef BROWNSUGAR_CORE_STATE_REENTEREVENT_H
#define BROWNSUGAR_CORE_STATE_REENTEREVENT_H

#include "fg/core/state/reenterevent.h"
#include "fg/core/state/creating.h"

struct FgStateReenterEventData
{
    FgCreatingStateDataDestroyProc  prevDataDestroyProcPtr;

    FgStateReenterEventData(
        FgCreatingStateDataDestroyProc
    );
};

#endif  // BROWNSUGAR_CORE_STATE_REENTEREVENT_H
