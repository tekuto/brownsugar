﻿#ifndef BROWNSUGAR_CORE_STATE_EVENTIMPL_H
#define BROWNSUGAR_CORE_STATE_EVENTIMPL_H

#include "brownsugar/def/core/state/eventimpl.h"
#include "fg/core/state/state.h"

namespace brownsugar {
    class StateEvent
    {
        FgState &   state;
        void *      eventDataPtr;

    public:
        using Proc = void ( * )(
            StateEvent &
        );

        StateEvent(
            FgState &
            , void *
        );

        FgState * getState(
        );

        void * getData(
        );
    };
}

#endif  // BROWNSUGAR_CORE_STATE_EVENTIMPL_H
