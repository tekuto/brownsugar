﻿#ifndef BROWNSUGAR_CORE_STATE_JOINER_H
#define BROWNSUGAR_CORE_STATE_JOINER_H

#include "fg/core/state/joiner.h"

#include <memory>
#include <mutex>
#include <condition_variable>
#include <cstddef>

struct FgStateJoiner
{
    struct End
    {
        void operator()(
            FgStateJoiner *
        ) const;
    };

    using Ender = std::unique_ptr<
        FgStateJoiner
        , End
    >;

    struct Join
    {
        void operator()(
            FgStateJoiner *
        ) const;
    };

    using Joiner = std::unique_ptr<
        FgStateJoiner
        , Join
    >;

    std::mutex              mutex;
    std::condition_variable cond;

    std::size_t runningCount;

    Joiner  joiner;

    FgStateJoiner(
    );

    Ender start(
    );
};

#endif  // BROWNSUGAR_CORE_STATE_JOINER_H
