﻿#ifndef BROWNSUGAR_CORE_STATE_EVENTREGISTERMANAGER_H
#define BROWNSUGAR_CORE_STATE_EVENTREGISTERMANAGER_H

#include "fg/core/state/eventregistermanager.h"
#include "brownsugar/core/state/eventmanager.h"

struct FgStateEventRegisterManager
{
    FgStateEventManager::EventProcUnregisterer  unregisterer;
};

#endif  // BROWNSUGAR_CORE_STATE_EVENTREGISTERMANAGER_H
