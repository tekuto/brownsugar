﻿#ifndef BROWNSUGAR_CORE_STATE_STATE_H
#define BROWNSUGAR_CORE_STATE_STATE_H

#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/threadsingleton.h"
#include "fg/core/state/joiner.h"
#include "brownsugar/core/state/eventimpl.h"
#include "brownsugar/core/state/joiner.h"
#include "brownsugar/core/state/eventmanager.h"
#include "brownsugar/core/thread/cache.h"
#include "brownsugar/core/thread/joiner.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <memory>
#include <set>
#include <mutex>

struct FgState : public fg::UniqueWrapper< FgState >
{
    struct DestroyData
    {
        void operator()(
            FgState *
        ) const;
    };

    using DataDestroyer = std::unique_ptr<
        FgState
        , DestroyData
    >;

    struct ThreadSingletonData
    {
        FgState &                   state;
        FgStateThreadSingletonProc  threadProcPtr;
        FgStateJoiner::Ender        ender;
        void *                      userDataPtr;

        bool    reexecute;

        ThreadSingletonData(
            FgState &
            , FgStateThreadSingletonProc
            , FgStateJoiner::Ender &&
            , void *
        );
    };

    struct LessThreadSingletonData
    {
        bool operator()(
            const ThreadSingletonData &
            , const ThreadSingletonData &
        ) const;
    };

    using ThreadSingletonDataSet = std::set<
        ThreadSingletonData
        , LessThreadSingletonData
    >;

    struct EndState
    {
        void operator()(
            FgState *
        ) const;
    };

    using Ender = std::unique_ptr<
        FgState
        , EndState
    >;

    brownsugar::ThreadCache &   threadCache;

    FgState *   prevStatePtr;

    std::mutex  disabledMutex;

    bool    disabled;

    void *                          dataPtr;
    FgCreatingStateDataDestroyProc  dataDestroyProcPtr;
    DataDestroyer                   dataDestroyer;

    bool                            existsEnterEvent;
    brownsugar::StateEvent::Proc    enterEventProcPtr;
    bool                            existsExitEvent;
    brownsugar::StateEvent::Proc    exitEventProcPtr;
    bool                            existsReenterEvent;
    brownsugar::StateEvent::Proc    reenterEventProcPtr;
    bool                            existsLeaveEvent;
    brownsugar::StateEvent::Proc    leaveEventProcPtr;

    std::mutex  threadSingletonMutex;

    ThreadSingletonDataSet  threadSingletonDataSet;

    brownsugar::ThreadJoiner::Unique    threadJoinerBackgroundUnique;
    brownsugar::ThreadJoiner::Unique    threadJoinerUnique;

    Ender   ender;

    FgState::Unique nextStateUnique;

    FgState(
        brownsugar::ThreadCache &
        , FgState *
        , bool
        , void *
        , FgCreatingStateDataDestroyProc
    );

    static FG_FUNCTION_PTR(
        FgState
        , create_(
            brownsugar::ThreadCache &
            , FgState *
            , bool
            , void *
            , FgCreatingStateDataDestroyProc
        )
    )

    inline static Unique create(
        brownsugar::ThreadCache &           _threadCache
        , FgState *                         _prevStatePtr
        , bool                              _disabled
        , void *                            _dataPtr
        , FgCreatingStateDataDestroyProc    _dataDestroyProcPtr
    )
    {
        return create_(
            _threadCache
            , _prevStatePtr
            , _disabled
            , _dataPtr
            , _dataDestroyProcPtr
        );
    }

    static FG_FUNCTION_VOID(
        destroy(
            FgState *
        )
    )

    void setData(
        void *
        , FgCreatingStateDataDestroyProc
    );

    void setEnterEventProc(
        brownsugar::StateEvent::Proc
    );

    void setExitEventProc(
        brownsugar::StateEvent::Proc
    );

    void setReenterEventProc(
        brownsugar::StateEvent::Proc
    );

    void setLeaveEventProc(
        brownsugar::StateEvent::Proc
    );

    FG_FUNCTION_REF(
        FgState
        , getNextState(
        )
    )

    FG_FUNCTION_VOID(
        setNextState(
            FgState::Unique &&
        )
    )

    FgState & getRootState(
    );

    FG_FUNCTION_VOID(
        executeEnterThread(
        )
    )

    void executeEvent(
        brownsugar::StateEvent::Proc
        , fg::StateJoiner &
        , FgStateJoiner &
        , void *
    );

    void executeEventBackground(
        brownsugar::StateEvent::Proc
        , fg::StateJoiner &
        , FgStateJoiner &
        , void *
    );
};

#endif  // BROWNSUGAR_CORE_STATE_STATE_H
