﻿#ifndef BROWNSUGAR_CORE_STATE_BASESYSTEM_H
#define BROWNSUGAR_CORE_STATE_BASESYSTEM_H

#include "fg/core/state/basesystem.h"
#include "fg/core/state/state.h"
#include "fg/core/module/context.h"
#include "fg/util/import.h"

struct FgBasesystemState : public fg::UniqueWrapper< FgBasesystemState >
{
    FgState &           state;
    FgModuleContext &   moduleContext;

    bool    existsWaitEndProc;

    FgBasesystemState(
        FgState &
        , FgModuleContext &
    );

    static FG_FUNCTION_PTR(
        FgBasesystemState
        , create_(
            FgState &
            , FgModuleContext &
        )
    )

    inline static Unique create(
        FgState &           _state
        , FgModuleContext & _moduleContext
    )
    {
        return create_(
            _state
            , _moduleContext
        );
    }

    static FG_FUNCTION_VOID(
        destroy(
            FgBasesystemState *
        )
    )

    FG_FUNCTION_BOOL(
        isExistsWaitEndProc(
        ) const
    )
};

#endif  // BROWNSUGAR_CORE_STATE_BASESYSTEM_H
