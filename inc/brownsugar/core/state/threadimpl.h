﻿#ifndef BROWNSUGAR_CORE_STATE_THREADIMPL_H
#define BROWNSUGAR_CORE_STATE_THREADIMPL_H

#include "brownsugar/def/core/state/threadimpl.h"
#include "fg/core/state/state.h"

namespace brownsugar {
    class StateThread
    {
        FgState &   state;
        void *      userDataPtr;

    public:
        StateThread(
            FgState &
            , void *
        );

        FgState * getState(
        );

        void * getData(
        );
    };
}

#endif  // BROWNSUGAR_CORE_STATE_THREADIMPL_H
