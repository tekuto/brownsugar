﻿#ifndef BROWNSUGAR_CORE_THREAD_CACHE_H
#define BROWNSUGAR_CORE_THREAD_CACHE_H

#include "brownsugar/def/core/thread/cache.h"
#include "brownsugar/def/core/thread/joiner.h"
#include "brownsugar/core/thread/thread.h"
#include "brownsugar/core/thread/singleton.h"
#include "fg/common/unique.h"

#include <vector>
#include <mutex>

namespace brownsugar {
    class ThreadCache : public fg::UniqueWrapper< ThreadCache >
    {
        using ThreadUniques = std::vector< ThreadImpl::Unique >;

        using ThreadSingletonUniques = std::vector< ThreadSingletonImpl::Unique >;

        std::mutex  threadUniquesMutex;

        ThreadUniques   threadUniques;

        std::mutex  singletonThreadUniquesMutex;

        ThreadSingletonUniques  singletonThreadUniques;

    public:
        ThreadCache(
        );

        static Unique create(
        );

        static void destroy(
            ThreadCache *
        );

        void execute(
            ThreadProcImpl
            , void *
            , ThreadJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            ThreadProc< USER_DATA_T >   _procPtr
            , USER_DATA_T &             _userData
            , ThreadJoiner &            _joiner
        )
        {
            this->execute(
                reinterpret_cast< ThreadProcImpl >( _procPtr )
                , &_userData
                , _joiner
            );
        }

        void execute(
            ThreadSingletonProcImpl
            , void *
            , ThreadJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            ThreadSingletonProc< USER_DATA_T >  _procPtr
            , USER_DATA_T &                     _userData
            , ThreadJoiner &                    _joiner
        )
        {
            this->execute(
                reinterpret_cast< ThreadSingletonProcImpl >( _procPtr )
                , &_userData
                , _joiner
            );
        }
    };
}

#endif  // BROWNSUGAR_CORE_THREAD_CACHE_H
