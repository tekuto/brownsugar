﻿#ifndef BROWNSUGAR_CORE_THREAD_SINGLETON_H
#define BROWNSUGAR_CORE_THREAD_SINGLETON_H

#include "brownsugar/def/core/thread/singleton.h"
#include "brownsugar/core/thread/joiner.h"
#include "fg/common/unique.h"

#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>

namespace brownsugar {
    class ThreadSingletonImpl : public fg::UniqueWrapper< ThreadSingletonImpl >
    {
        struct JoinThread
        {
            void operator()(
                std::thread *
            ) const;
        };

        using Joiner = std::unique_ptr<
            std::thread
            , JoinThread
        >;

        struct EndThread
        {
            void operator()(
                ThreadSingletonImpl *
            ) const;
        };

        using Ender = std::unique_ptr<
            ThreadSingletonImpl
            , EndThread
        >;

        std::mutex              mutex;
        std::condition_variable cond;

        bool    ended;
        bool    enabled;
        bool    reexecute;

        ThreadSingletonProcImpl procPtr;
        void *                  userDataPtr;
        ThreadJoiner::Ender     procEnder;

        std::thread thread;

        Joiner  joiner;
        Ender   ender;

        bool waitForEnabled(
            std::unique_lock< std::mutex > &
        );

        void threadProc(
        );

    public:
        ThreadSingletonImpl(
            ThreadSingletonProcImpl
            , void *
            , ThreadJoiner &
        );

        static Unique create(
            ThreadSingletonProcImpl _procPtr
            , void *                _userDataPtr
            , ThreadJoiner &        _joiner
        )
        {
            return new ThreadSingletonImpl(
                _procPtr
                , _userDataPtr
                , _joiner
            );
        }

        bool setReexecute(
            ThreadSingletonProcImpl
            , void *
            , ThreadJoiner &
        );

        bool reset(
            ThreadSingletonProcImpl
            , void *
            , ThreadJoiner &
        );

        void * getData(
        );
    };

    template< typename USER_DATA_T >
    class ThreadSingleton
    {
    public:
        USER_DATA_T & getData(
        )
        {
            return *static_cast< USER_DATA_T * >( reinterpret_cast< ThreadSingletonImpl * >( this )->getData() );
        }
    };
}

#endif  // BROWNSUGAR_CORE_THREAD_SINGLETON_H
