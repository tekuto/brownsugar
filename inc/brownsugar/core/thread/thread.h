﻿#ifndef BROWNSUGAR_CORE_THREAD_THREAD_H
#define BROWNSUGAR_CORE_THREAD_THREAD_H

#include "brownsugar/def/core/thread/thread.h"
#include "brownsugar/core/thread/joiner.h"
#include "fg/common/unique.h"

#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>

namespace brownsugar {
    class ThreadImpl : public fg::UniqueWrapper< ThreadImpl >
    {
        struct JoinThread
        {
            void operator()(
                std::thread *
            ) const;
        };

        using Joiner = std::unique_ptr<
            std::thread
            , JoinThread
        >;

        struct EndThread
        {
            void operator()(
                ThreadImpl *
            ) const;
        };

        using Ender = std::unique_ptr<
            ThreadImpl
            , EndThread
        >;

        std::mutex              mutex;
        std::condition_variable cond;

        bool    ended;
        bool    enabled;

        ThreadProcImpl      procPtr;
        void *              userDataPtr;
        ThreadJoiner::Ender procEnder;

        std::thread thread;

        Joiner  joiner;
        Ender   ender;

        bool waitForEnabled(
            std::unique_lock< std::mutex > &
        );

        void threadProc(
        );

    public:
        ThreadImpl(
            ThreadProcImpl
            , void *
            , ThreadJoiner &
        );

        static Unique create(
            ThreadProcImpl      _procPtr
            , void *            _userDataPtr
            , ThreadJoiner &    _joiner
        )
        {
            return new ThreadImpl(
                _procPtr
                , _userDataPtr
                , _joiner
            );
        }

        bool reset(
            ThreadProcImpl
            , void *
            , ThreadJoiner &
        );

        void * getData(
        );
    };

    template< typename USER_DATA_T >
    class Thread
    {
    public:
        USER_DATA_T & getData(
        )
        {
            return *static_cast< USER_DATA_T * >( reinterpret_cast< ThreadImpl * >( this )->getData() );
        }
    };
}

#endif  // BROWNSUGAR_CORE_THREAD_THREAD_H
