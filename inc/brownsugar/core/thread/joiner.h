﻿#ifndef BROWNSUGAR_CORE_THREAD_JOINER_H
#define BROWNSUGAR_CORE_THREAD_JOINER_H

#include "brownsugar/def/core/thread/joiner.h"
#include "brownsugar/def/core/thread/thread.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <memory>
#include <mutex>
#include <condition_variable>
#include <cstddef>

namespace brownsugar {
    class ThreadJoiner : public fg::UniqueWrapper< ThreadJoiner >
    {
        struct End
        {
            void operator()(
                ThreadJoiner *
            ) const;
        };

        struct Join
        {
            void operator()(
                ThreadJoiner *
            ) const;
        };

        using Joiner = std::unique_ptr<
            ThreadJoiner
            , Join
        >;

        std::mutex              mutex;
        std::condition_variable cond;

        std::size_t runningThreadCount;

        Joiner  joiner;

    public:
        using Ender = std::unique_ptr<
            ThreadJoiner
            , End
        >;

        ThreadJoiner(
        );

        static Unique create(
        );

        static void destroy(
            ThreadJoiner *
        );

        Ender startThread(
        );

        FG_FUNCTION_VOID(
            join(
            )
        )
    };
}

#endif  // BROWNSUGAR_CORE_THREAD_JOINER_H
